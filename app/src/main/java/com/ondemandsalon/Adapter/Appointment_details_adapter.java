package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.ArrayList;

public class Appointment_details_adapter extends RecyclerView.Adapter<Appointment_details_adapter.MyViewHolder> {

    Context context;
    ArrayList<Service_data> sdatalist;

    public Appointment_details_adapter(Context context, ArrayList<Service_data> sdatalist) {
        this.context = context;
        this.sdatalist = sdatalist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_details_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (position % 2 == 0) {
            holder.item_back.setBackgroundColor(context.getResources().getColor(R.color.smothgrey));
        } else {
            holder.item_back.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        Service_data d = sdatalist.get(position);

     /*   holder.snam.setText(d.getDuration());
        holder.pnam.setText(d.getPrice());*/
    }

    @Override
    public int getItemCount() {
        return sdatalist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout item_back;
        CustomTextviewRegular snam, pnam;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_back = itemView.findViewById(R.id.item_back);
            snam = itemView.findViewById(R.id.snam);
            pnam = itemView.findViewById(R.id.pnam);
        }
    }
}
