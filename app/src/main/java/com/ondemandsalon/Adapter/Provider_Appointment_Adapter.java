package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.fragments.Appointment_details;
import com.ondemandsalon.fragments.Provider;
import com.ondemandsalon.fragments.Ratreviews_details;
import com.ondemandsalon.model.Provider_Appo_data;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.ArrayList;

public class Provider_Appointment_Adapter extends RecyclerView.Adapter<Provider_Appointment_Adapter.MyViewHolder> {

    Context context;
    HomeNew p;
    ArrayList<Provider_Appo_data> apdata;

    public Provider_Appointment_Adapter(Context context, HomeNew p, ArrayList<Provider_Appo_data> apdata) {
        this.context = context;
        this.p = p;
        this.apdata = apdata;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.provider_appointment_row_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final Provider_Appo_data data = apdata.get(position);

        holder.apnm.setText(data.getSana_saikh());
        holder.snm.setText(data.getColor());
        holder.aptm.setText(data.getS());
        holder.date_txt.setText(data.getToday());

        if (data.getType().equals("U")) {
            holder.btns.setVisibility(View.VISIBLE);
            holder.cardRate.setVisibility(View.GONE);
        } else {
            holder.btns.setVisibility(View.GONE);
            holder.cardRate.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.changeFragment(new Appointment_details(context, p, data), 1, "Appointment Details","APPO_DETAILS","provider");
                HomeNew.APPOINTMENT_LEVEL_1 = "Appointment Details";
            }
        });

        holder.cardRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                p.changeFragment(new Ratreviews_details(data.getSana_saikh(), data.getS(), data.getColor()), 3, "Detailed Review");
            }
        });
    }

    @Override
    public int getItemCount() {
        return apdata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CustomTextviewRegular apnm, snm, aptm, date_txt;
        CardView cardRate;
        LinearLayout btns;

        public MyViewHolder(View itemView) {
            super(itemView);
            apnm = itemView.findViewById(R.id.apnm);
            snm = itemView.findViewById(R.id.snm);
            aptm = itemView.findViewById(R.id.aptm);
            date_txt = itemView.findViewById(R.id.date_txt);
            cardRate = itemView.findViewById(R.id.cardRate);
            btns = itemView.findViewById(R.id.btns);
        }
    }
}
