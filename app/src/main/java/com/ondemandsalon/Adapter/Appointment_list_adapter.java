package com.ondemandsalon.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ondemandsalon.R;
import com.ondemandsalon.model.User_Appointment_data;
import com.ondemandsalon.utils.CustomTextviewBold;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.ArrayList;

public class Appointment_list_adapter extends RecyclerView.Adapter<Appointment_list_adapter.Myviewholder> {

    ArrayList<User_Appointment_data> shopsList;

    public Appointment_list_adapter(ArrayList<User_Appointment_data> shopsList) {
        this.shopsList = shopsList;
    }

    @NonNull
    @Override
    public Myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointmentitem, parent, false);
        return new Myviewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Myviewholder holder, int position) {
        User_Appointment_data data = shopsList.get(position);

        if (data.getApp_status().equals("1")) {
            holder.status.setText("Accepted");
        } else if(data.getApp_status().equals("1")) {
            holder.status.setText("Cancel");
        }else {
            holder.status.setText("Pending");
        }
        holder.nm.setText(data.getApp_salon_name());
        holder.mono.setText(data.getApp_salon_phone());
        holder.serviceTxt.setText(data.getServices());
        holder.tmTxt.setText(data.getApp_service_date() + " " + data.getApp_service_time());
    }

    @Override
    public int getItemCount() {
        return shopsList.size();
    }

    public class Myviewholder extends RecyclerView.ViewHolder {

        CustomTextviewBold nm;
        CustomTextviewRegular mono, serviceTxt, tmTxt, status;


        public Myviewholder(View v) {
            super(v);
            nm = v.findViewById(R.id.salonName);
            mono = v.findViewById(R.id.mono);
            serviceTxt = v.findViewById(R.id.services);
            tmTxt = v.findViewById(R.id.time);
            status = v.findViewById(R.id.status);
        }
    }
}
