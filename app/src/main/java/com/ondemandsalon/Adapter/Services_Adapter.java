package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.UtilDialog;

import java.util.ArrayList;

public class Services_Adapter extends RecyclerView.Adapter<Services_Adapter.MyViewHolder> {

    Context context;
    ArrayList<Service_data> servicelst;
    UtilDialog dialog;
    addremove addremove;

    public Services_Adapter(Context context, ArrayList<Service_data> servicelst) {
        this.context = context;
        this.servicelst = servicelst;
        dialog = new UtilDialog(context);
        addremove = (addremove) context;
    }

    public interface addremove {
        void AddRemove(String time, String service_id, String s);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final Service_data sdata = servicelst.get(position);

        holder.snm.setText(sdata.getService_name());
        holder.stm.setText(sdata.getService_time());
        holder.spr.setText(sdata.getService_price());

        holder.stm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                addremove.AddRemove("TIME", sdata.getService_id(), editable.toString());
            }
        });

        holder.spr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                addremove.AddRemove("PRICE", sdata.getService_id(), editable.toString());
            }
        });

        holder.stm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.SelectTimeDialog("Select Time", holder.stm);
            }
        });

        holder.redclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addremove.AddRemove("REMOVE", sdata.getService_id(), "");
            }
        });

        if (position % 2 == 0) {
            holder.item_back.setBackgroundColor(context.getResources().getColor(R.color.smothgrey));
        } else {
            holder.item_back.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return servicelst.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout item_back;
        CustomTextviewRegular snm, stm;
        CustomEdittextRegular spr;
        ImageView redclose;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_back = itemView.findViewById(R.id.item_back);
            snm = itemView.findViewById(R.id.snm);
            stm = itemView.findViewById(R.id.stm);
            spr = itemView.findViewById(R.id.spr);
            redclose = itemView.findViewById(R.id.redClose);
        }
    }
}
