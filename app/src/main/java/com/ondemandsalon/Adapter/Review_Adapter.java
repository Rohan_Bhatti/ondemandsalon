package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.ondemandsalon.R;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Review_Adapter extends RecyclerView.Adapter<Review_Adapter.MyViewHolder> {

    ArrayList<Sub_cat> categoryList;
    Context context;

    public Review_Adapter(ArrayList<Sub_cat> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviewitem, parent, false);
        return new MyViewHolder(v);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.ratingBar.setEnabled(false);
        Sub_cat sub_cat = categoryList.get(position);
        holder.nm.setText(sub_cat.getUser_name());
        Glide.with(context).load(sub_cat.getUser_profile_image()).into(holder.item_Img);
        holder.nm3.setText(sub_cat.getRating_time());
        holder.ratingBar.setRating(Float.parseFloat(sub_cat.getUser_rating()));
        holder.rating_description.setText(sub_cat.getUser_description());
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView item_Img;
        CustomTextviewRegular nm, nm3, rating_description;
        RatingBar ratingBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_Img = itemView.findViewById(R.id.item_img);
            nm = itemView.findViewById(R.id.nm);
            nm3 = itemView.findViewById(R.id.nm3);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            rating_description = itemView.findViewById(R.id.rating_description);
        }
    }
}
