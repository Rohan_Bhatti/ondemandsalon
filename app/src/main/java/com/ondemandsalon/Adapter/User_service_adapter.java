package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ondemandsalon.R;
import com.ondemandsalon.fragments.Select_Appointment_Time;
import com.ondemandsalon.model.Sub_cat;

import java.util.ArrayList;

public class User_service_adapter extends RecyclerView.Adapter<User_service_adapter.MyViewHolder> {

    ArrayList<Sub_cat> categoryList;
    Context context;
    int selecteditem;

    public User_service_adapter(ArrayList<Sub_cat> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_user, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final Sub_cat sub_cat = categoryList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*holder.username.setTextColor(context.getResources().getColor(R.color.blue));
                holder.userimage.setImageResource(R.drawable.blueuser);*/
                Select_Appointment_Time.Selemp = sub_cat.getEmp_id();
                selecteditem = position;
                notifyDataSetChanged();
            }
        });

        holder.username.setText(sub_cat.getEmp_fname());
        if (selecteditem == position) {
            holder.username.setTextColor(context.getResources().getColor(R.color.blue));
            holder.userimage.setImageResource(R.drawable.blueuser);
        } else {
            holder.username.setTextColor(context.getResources().getColor(R.color.grey));
            holder.userimage.setImageResource(R.drawable.grayuser);
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView userimage;
        TextView username;

        public MyViewHolder(View itemView) {
            super(itemView);
            userimage = itemView.findViewById(R.id.item_img);
            username = itemView.findViewById(R.id.item_name);
        }
    }
}
