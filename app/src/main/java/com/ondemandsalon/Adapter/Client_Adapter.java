package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.fragments.Add_clients;
import com.ondemandsalon.fragments.Add_employee;
import com.ondemandsalon.fragments.Provider;
import com.ondemandsalon.model.Provider_Client;
import com.ondemandsalon.utils.CustomTextviewLight;

import java.util.ArrayList;

public class Client_Adapter extends RecyclerView.Adapter<Client_Adapter.MyViewHolder> {

    Context context;
    ArrayList<Provider_Client> provider_Clients;
    String frm;
    HomeNew p;

    public Client_Adapter(Context context, ArrayList<Provider_Client> provider_Clients, String from, HomeNew p) {
        this.context = context;
        this.provider_Clients = provider_Clients;
        this.frm = from;
        this.p = p;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.client_row_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Provider_Client provider_client = provider_Clients.get(position);
        holder.name.setText(provider_client.getName());
        holder.mono.setText(provider_client.getMobileno());
        holder.location.setText(provider_client.getLocation());

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frm.equals("Employee")) {
                    p.changeFragment(new Add_employee("edit", provider_client), 3, "Edit Employee","E_EMPLOYEE","provider");
                } else {
                    p.changeFragment(new Add_clients(context,"edit", provider_client), 2, "Edit Client","E_CLIENT","provider");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return provider_Clients.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CustomTextviewLight name, mono, location;
        ImageView edit;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.client_name);
            mono = itemView.findViewById(R.id.client_mo_num);
            location = itemView.findViewById(R.id.client_location);
            edit = itemView.findViewById(R.id.editIcon);
        }
    }
}
