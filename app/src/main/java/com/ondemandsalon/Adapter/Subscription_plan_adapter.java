package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ondemandsalon.R;

public class Subscription_plan_adapter extends RecyclerView.Adapter<Subscription_plan_adapter.MyViewHolder> {

    Context context;

    public Subscription_plan_adapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.subscription_plan_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (position % 2 == 0) {
            holder.item_back.setBackgroundColor(context.getResources().getColor(R.color.smothgrey));
        } else {
            holder.item_back.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout item_back;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_back = itemView.findViewById(R.id.item_back);
        }
    }
}
