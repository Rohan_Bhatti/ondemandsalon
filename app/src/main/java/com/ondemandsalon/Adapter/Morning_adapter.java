package com.ondemandsalon.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.fragments.Select_Appointment_Time;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.ArrayList;

public class Morning_adapter extends BaseAdapter {
    Context context;
    ArrayList<Sub_cat> item_names;
    String frm;

    int pos;

    public Morning_adapter(Context context, ArrayList<Sub_cat> item_names, String frm) {
        this.context = context;
        this.item_names = item_names;
        this.frm = frm;
    }

    @Override
    public int getCount() {
        return item_names.size();
    }

    @Override
    public Object getItem(int i) {
        return item_names.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        view = LayoutInflater.from(context).inflate(R.layout.time_item, viewGroup, false);

        Sub_cat ct = item_names.get(i);

        CustomTextviewRegular txt = view.findViewById(R.id.txt);
        RelativeLayout rel = view.findViewById(R.id.rel);

        if (ct.getSub_cat_id().equals("1")) {
            rel.setBackgroundColor(context.getResources().getColor(R.color.blue));
            txt.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            rel.setBackground(context.getResources().getDrawable(R.drawable.grey_line_back));
            txt.setTextColor(context.getResources().getColor(R.color.blue));
        }
        txt.setText(ct.getSub_cat_name());


        return view;
    }
}
