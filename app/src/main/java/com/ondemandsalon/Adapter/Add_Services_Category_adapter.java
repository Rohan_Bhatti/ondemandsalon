package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.android.volley.Response;
import com.ondemandsalon.R;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.utils.CustomTextviewRegular;

import org.json.JSONObject;

import java.util.ArrayList;

public class Add_Services_Category_adapter extends RecyclerView.Adapter<Add_Services_Category_adapter.MyViewHolder> {

    Context context;
    ArrayList<Sub_cat> categoryLst;
    Maintain_categories maintain_categories;


    public Add_Services_Category_adapter(Context context, ArrayList<Sub_cat> categoryLst) {
        this.context = context;
        this.categoryLst = categoryLst;
        maintain_categories = (Maintain_categories) context;
    }

    public interface Maintain_categories {
        void add_remove_category(String sub_cat_id, String sub_cat_name, String add);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_services_category_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        Sub_cat sub_cat = categoryLst.get(position);
        holder.ctnm.setText(sub_cat.getSub_cat_name());

        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Sub_cat sub_cat = categoryLst.get(position);
                if (b) {
                    holder.img.setVisibility(View.VISIBLE);
                    maintain_categories.add_remove_category(sub_cat.getSub_cat_id(), sub_cat.getSub_cat_name(), "ADD");
                } else {
                    holder.img.setVisibility(View.VISIBLE);
                    maintain_categories.add_remove_category(sub_cat.getSub_cat_id(), sub_cat.getSub_cat_name(), "REMOVE");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryLst.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CustomTextviewRegular ctnm;
        CheckBox check;
        ImageView img;

        public MyViewHolder(View itemView) {
            super(itemView);
            ctnm = itemView.findViewById(R.id.ctnm);
            check = itemView.findViewById(R.id.checkbox);
            img = itemView.findViewById(R.id.img);
        }
    }
}
