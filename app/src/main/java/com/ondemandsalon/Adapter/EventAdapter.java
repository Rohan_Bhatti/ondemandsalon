package com.ondemandsalon.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.ForgotPasswordActivity;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.fragments.Service_Details;
import com.ondemandsalon.fragments.User;
import com.ondemandsalon.helper.OnResponseListener;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;
import com.ondemandsalon.utils.LoginDialog;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.ondemandsalon.fragments.User.disableLogin;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {

    ArrayList<Service_data> categoryList;
    Context context;
    //    gotoNext n;
    Preferences preferences;
    book_service_cart book_service_cart;
    ConnectionDetector connectionDetector;
    //    Dialog dialog;
    HomeNew h;
    ProgDialog progDialog;
    LoginDialog dialog;


    public EventAdapter(ArrayList<Service_data> categoryList, Context context, Service_Details service_details, HomeNew h) {
        this.categoryList = categoryList;
        this.context = context;
        this.book_service_cart = service_details;
        preferences = new Preferences(context);
        progDialog = new ProgDialog(context);
        this.h = h;
        dialog = new LoginDialog(context);
        connectionDetector = new ConnectionDetector(context);
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.eventitem, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final Service_data data = categoryList.get(position);

        holder.priceTxt.setText(data.getSub_cat_price() + "AED");
        holder.nm.setText(data.getSub_cat_name());
        Glide.with(context).load(data.getSub_cat_image()).into(holder.srIc);

        if (data.getStatus() == 0) {
            holder.book_btn.setBackground(context.getResources().getDrawable(R.drawable.newrecblue));
            holder.book_txt.setText("Book");
            holder.book_txt.setTextColor(context.getResources().getColor(R.color.blue));
            holder.book_Img.setImageResource(R.drawable.plus);
        } else {
            holder.book_btn.setBackgroundColor(context.getResources().getColor(android.R.color.darker_gray));
            holder.book_txt.setText("Cancel");
            holder.book_txt.setTextColor(context.getResources().getColor(R.color.white));
            holder.book_Img.setImageResource(R.drawable.booked_green_sign);
        }

        holder.book_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.book_txt.getText().equals("Book")) {
                    if (preferences.getUserLogin()) {
                        data.setStatus(1);
                        notifyDataSetChanged();
                        book_service_cart.add_service_to_cart("ADD", data);
                    } else
                        openDialog(position,  "book");
                } else {
                    book_service_cart.add_service_to_cart("REMOVE", data);
                    data.setStatus(0);
                    notifyDataSetChanged();
                }
            }
        });

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preferences.getUserLogin()) {
                    callIntent(data.getSalon_contct_no());
                } else
                    openDialog(position, "call");
            }
        });

    }

    private void callIntent(String salon_contct_no) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + salon_contct_no));
        context.startActivity(intent);

    }

    private void openDialog(final int position,final String action) {

        dialog.openLogin("user", new OnResponseListener() {
            @Override
            public void onResponce(int res) {
                switch (res) {
                    case 1:
                        h.disableLogin("User NewLogin");
                        if (action.equals("book")) {
                            Service_data service_data = categoryList.get(position);
                            service_data.setStatus(1);
                            notifyDataSetChanged();
                            book_service_cart.add_service_to_cart("ADD", service_data);
                        } else {
                            Service_data service_data = categoryList.get(position);
                            callIntent(service_data.getSalon_contct_no());
                        }
                        break;
                    case 3:
                        if (action.equals("book")) {
                            h.gotoN(position, "book");
                        } else
                            h.gotoN(position, "call");
                        break;
                }
            }
        });

        /*dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.login_dialog);

        ImageView closeImage = dialog.findViewById(R.id.closeImage);
        RelativeLayout signIn = dialog.findViewById(R.id.signInRel);
        CustomTextviewRegular signupTxt = dialog.findViewById(R.id.signupTxt);

        final CustomEdittextRegular emailEdt, passswordEdt;

        emailEdt = dialog.findViewById(R.id.emailEdt);
        passswordEdt = dialog.findViewById(R.id.passwordEdt);
        CustomTextviewRegular forgotpassword;
        forgotpassword = dialog.findViewById(R.id.forgotTxt);
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                context.startActivity(new Intent(context, ForgotPasswordActivity.class));
            }
        });
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getUserLogin()) {
                    if (action.equals("book")) {
                        Service_data service_data = categoryList.get(position);
                        service_data.setStatus(1);
                        notifyDataSetChanged();
                        book_service_cart.add_service_to_cart("ADD", service_data);
                    } else {
                        Service_data service_data = categoryList.get(position);
                        callIntent(service_data.getSalon_contct_no());
                    }
                } else
                    validate(emailEdt, passswordEdt,position);

//                n.gotoN(position, "LogIn");
            }
        });

        signupTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                if (action.equals("book")) {
//                    n.gotoN(position, "SignUp", "book");
//                } else
//                    n.gotoN(position, "SignUp", "call");

            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
*/
    }

   /* private void validate(CustomEdittextRegular emailEdt, CustomEdittextRegular passswordEdt, int pos) {

        if (!validateEmail(emailEdt)) {
            return;
        }
        if (!validatePassword(passswordEdt)) {
            return;
        }
        if (connectionDetector.isNetworkAvailable()) {
            login(emailEdt, passswordEdt, pos);
        } else
            connectionDetector.error_Dialog();
    }

    private void login(final CustomEdittextRegular emailEdt, final CustomEdittextRegular passswordEdt, final int pos) {
        progDialog.ProgressDialogStart();
        JSONObject params = new JSONObject();
        try {
            final String eml = emailEdt.getText().toString();
            final String psd = passswordEdt.getText().toString();
            params.accumulate("user_email", eml);
            params.accumulate("user_password", psd);
            params.accumulate("role", "2");
            params.accumulate("device_type", "1");
            params.accumulate("device_token", "123");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        json j = new json(API.LOGIN, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
                System.out.println(response.toString());
                try {
                    if (response.getString("status").equals("1")) {
                        JSONObject jsonObject = response.getJSONObject("user_details");
                        preferences.setClientID(jsonObject.getString("id"));
                        preferences.setLogin(true);
                        preferences.setUserLogin(true);
                        preferences.lastLogin("user");
                        disableLogin();
                        Service_data service_data = categoryList.get(pos);
                        service_data.setStatus(1);
                        notifyDataSetChanged();
                        book_service_cart.add_service_to_cart("ADD", service_data);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        User.disableLogin();
                    } else {
                        Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                Toast.makeText(context, "Somthing wents wrong", Toast.LENGTH_SHORT).show();
                System.out.println(error.toString());
            }
        });
        AppController.getInstance().addToRequestQueue(j);
    }

    private boolean validateEmail(CustomEdittextRegular emailEdt) {
        String e = emailEdt.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        } else if (!isValidEmail(e)) {
            Toast.makeText(context, "Please enter valid email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        }
        return true;
    }

    private boolean validatePassword(CustomEdittextRegular passswordEdt) {
        if (passswordEdt.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter your password", Toast.LENGTH_SHORT).show();
            requestFocus(passswordEdt);
            return false;
        } else if (passswordEdt.getText().toString().trim().length() < 4) {
            Toast.makeText(context, "Please enter valid password", Toast.LENGTH_SHORT).show();
            requestFocus(passswordEdt);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }*/

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public interface book_service_cart {
        void add_service_to_cart(String add, Service_data data);
    }

    public interface gotoNext {
        public void gotoN(int position, String type, String fro);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout book_btn;
        CustomTextviewSemiBold book_txt;
        ImageView book_Img, call, srIc;
        CustomTextviewRegular priceTxt, nm;

        public MyViewHolder(View v) {
            super(v);

            book_btn = v.findViewById(R.id.book_btn);
            priceTxt = v.findViewById(R.id.priceTxt);
            book_txt = v.findViewById(R.id.book_txt);
            book_Img = v.findViewById(R.id.book_img);
            srIc = v.findViewById(R.id.srIc);
            call = v.findViewById(R.id.call);
            nm = v.findViewById(R.id.nm);
        }
    }
}
