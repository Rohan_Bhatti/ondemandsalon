package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.fragments.Provider;
import com.ondemandsalon.fragments.User;
import com.ondemandsalon.fragments.Ratreviews_details;
import com.ondemandsalon.model.Ratings_Data;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;

import java.util.ArrayList;

public class Ratings_Reviews_Adapter extends RecyclerView.Adapter<Ratings_Reviews_Adapter.MyviewHolder> {


    Context context;
    ArrayList<Ratings_Data> ratingsList;
    HomeNew p;
    String frm;

    public Ratings_Reviews_Adapter(Context context, ArrayList<Ratings_Data> ratingsList, HomeNew p,String frm) {
        this.context = context;
        this.ratingsList = ratingsList;
        this.p = p;
        this.frm = frm;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.rating_item, parent, false);
        return new MyviewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        final Ratings_Data rdata = ratingsList.get(position);
        holder.name.setText(rdata.getSalon_name());
        holder.date.setText(rdata.getRating_date());
        holder.comment.setText(rdata.getRating_description());
        holder.ratingBar.setRating(Float.parseFloat(rdata.getAverage_rating()));
        holder.ratingBar.setEnabled(false);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frm.equals("user"))
                    p.changeFragment(new Ratreviews_details(rdata, "USER"), 3, "Detailed Review", "U_D_REVIEW","user");
                else
                    p.changeFragment(new Ratreviews_details(rdata.getSalon_name(), rdata.getRating_date(), rdata.getRating_description(),"PROVIDER"), 3, "Detailed Review","P_D_REVIEW","provider");
            }
        });


    }

    @Override
    public int getItemCount() {
        return ratingsList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {

        CustomTextviewSemiBold name;
        CustomTextviewRegular date, comment;
        RatingBar ratingBar;

        public MyviewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            date = itemView.findViewById(R.id.date);
            comment = itemView.findViewById(R.id.cmt);
            ratingBar = itemView.findViewById(R.id.ratingBar);

        }
    }
}
