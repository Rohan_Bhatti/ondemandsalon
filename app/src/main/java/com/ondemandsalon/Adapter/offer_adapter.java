package com.ondemandsalon.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.fragments.Add_Offer;
import com.ondemandsalon.fragments.OfferDescription;
import com.ondemandsalon.fragments.Provider;
import com.ondemandsalon.fragments.User;
import com.ondemandsalon.model.Shops;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;

import java.util.ArrayList;

import static com.ondemandsalon.helper.CONSTANTS.SERVICES;

public class offer_adapter extends RecyclerView.Adapter<offer_adapter.MyviewHolder> {

    HomeNew u;
    ArrayList<Shops> list;
    String frm;

    public offer_adapter() {
    }

    public offer_adapter(HomeNew u, ArrayList<Shops> list,String frm) {
        this.u = u;
        this.list = list;
        this.frm = frm;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyviewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {

        if (frm.equals("user")) {
            holder.editicon.setVisibility(View.GONE);
            holder.titleTxt.setVisibility(View.VISIBLE);

            final Shops s = list.get(position);
            holder.salonName.setText(s.getSalon_name());
            holder.desc.setText(s.getOffer_description());
            holder.offer_date.setText(s.getDate());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    u.changeFragment(new OfferDescription(s), 0, "Offer Description", "OFFER_DESC","user");
                    HomeNew.HOME_LEVEL_2 = "Offer Description";
                }
            });
        } else {
            holder.editicon.setVisibility(View.VISIBLE);
            holder.titleTxt.setVisibility(View.GONE);
        }

        holder.editicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u.changeFragment(new Add_Offer("edit"), 3, "Edit Offer","E_OFFER","provider");
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {

        ImageView editicon;
        CustomTextviewRegular titleTxt, desc, offer_date;
        CustomTextviewSemiBold salonName;

        public MyviewHolder(View itemView) {
            super(itemView);

            editicon = itemView.findViewById(R.id.editIcon);
            titleTxt = itemView.findViewById(R.id.titleTxt);
            salonName = itemView.findViewById(R.id.client_name);
            desc = itemView.findViewById(R.id.client_location);
            offer_date = itemView.findViewById(R.id.client_mo_num);
        }
    }
}
