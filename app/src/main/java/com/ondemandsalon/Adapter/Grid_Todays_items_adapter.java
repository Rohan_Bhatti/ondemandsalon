package com.ondemandsalon.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ondemandsalon.R;
import com.ondemandsalon.model.User_Home_Category;

import java.util.ArrayList;

public class Grid_Todays_items_adapter extends BaseAdapter {
    Context context;
    ArrayList<User_Home_Category> item_names;
    ImageView imageView;
    TextView textView;

    public Grid_Todays_items_adapter(Context context, ArrayList<User_Home_Category> item_names) {
        this.context = context;
        this.item_names = item_names;
    }

    @Override
    public int getCount() {
        return item_names.size();
    }

    @Override
    public Object getItem(int i) {
        return item_names.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.today_item, viewGroup, false);
        imageView = view.findViewById(R.id.item_img);
        textView = view.findViewById(R.id.item_name);

        User_Home_Category user_home_category = item_names.get(i);

        Glide.with(context).load(user_home_category.getImg()).into(imageView);
//        imageView.setImageResource(R.drawable.brows);
        textView.setText(user_home_category.getNm());
        return view;
    }
}
