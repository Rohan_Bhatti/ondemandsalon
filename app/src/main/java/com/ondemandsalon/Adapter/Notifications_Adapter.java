package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ondemandsalon.R;
import com.ondemandsalon.model.Notifications_Data;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Notifications_Adapter extends RecyclerView.Adapter<Notifications_Adapter.Myviewholder> {

    Context c;
    ArrayList<Notifications_Data> list;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dateFormatD = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat dateFormatT = new SimpleDateFormat("hh:mm a");

    public Notifications_Adapter(Context c, ArrayList<Notifications_Data> list) {
        this.c = c;
        this.list = list;
    }

    @NonNull
    @Override
    public Myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Myviewholder(LayoutInflater.from(c).inflate(R.layout.notitem, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Myviewholder holder, int position) {

        Notifications_Data data = list.get(position);

        try {
            Date d = dateFormat.parse(data.getCreated_date());

            holder.date.setText(dateFormatD.format(d));
            holder.time.setText(dateFormatT.format(d));

            holder.salonnm.setText(data.getBusiness_name());
            holder.nottxt.setText(data.getNotification_text());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Myviewholder extends RecyclerView.ViewHolder {

        CustomTextviewSemiBold date, time, salonnm;
        CustomTextviewRegular nottxt;

        public Myviewholder(View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.dateTxt);
            time = itemView.findViewById(R.id.timeTxt);
            salonnm = itemView.findViewById(R.id.salonnm);
            nottxt = itemView.findViewById(R.id.notTxt);
        }
    }
}
