package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.fragments.Search_All_Services;
import com.ondemandsalon.fragments.Service_Details;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    ArrayList<Sub_cat> sub_categoryList;
    Context c;
    int pos;

    filter_data filter_data;

    public interface filter_data {
        void filter(String sub_cat_name, String sub_cat_id);
    }

    public CategoryAdapter(Context c, ArrayList<Sub_cat> categoryList, Search_All_Services f) {
        this.sub_categoryList = categoryList;
        this.filter_data = f;
        this.c = c;
    }

    public CategoryAdapter(Context context, ArrayList<Sub_cat> categoryList, Service_Details service_details) {
        this.sub_categoryList = categoryList;
        this.c = context;
        this.filter_data = (filter_data) service_details;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_row, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Sub_cat sub_cat = sub_categoryList.get(position);
        holder.txt.setText(sub_cat.getSub_cat_name());

        if (pos != position)
            holder.categoryRel.setBackgroundColor(c.getResources().getColor(R.color.blue));
        else
            holder.categoryRel.setBackground(c.getResources().getDrawable(R.drawable.service_list_back_blue));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                filter_data.filter(sub_cat.getSub_cat_name(), sub_cat.getSub_cat_id());
//                r.refreshList();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return sub_categoryList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        CustomTextviewRegular txt;
        RelativeLayout categoryRel;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt = itemView.findViewById(R.id.category_txt);
            categoryRel = itemView.findViewById(R.id.categoryRel);
        }
    }
}
