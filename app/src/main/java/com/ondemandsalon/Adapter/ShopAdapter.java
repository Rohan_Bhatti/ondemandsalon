package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.fragments.User;
import com.ondemandsalon.fragments.Service_Details;
import com.ondemandsalon.model.Shops;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;

import java.util.ArrayList;

import static com.ondemandsalon.helper.CONSTANTS.SERVICES;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.Myviewholder> {

    ArrayList<Shops> shopsList;
    HomeNew h;
    String frm;
    Context context;
    String use_for;

    public ShopAdapter(ArrayList<Shops> shopsList, HomeNew h, String frm, Context context, String near_by) {
        this.shopsList = shopsList;
        this.h = h;
        this.frm = frm;
        this.context = context;
        this.use_for = near_by;
    }

    @NonNull
    @Override
    public Myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_item_row, parent, false);
        return new Myviewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Myviewholder holder, int position) {
        final Shops sh = shopsList.get(position);

        Glide.with(context).load(sh.getSalon_image()).into(holder.shopImage);
        holder.nm.setText(sh.getSalon_name());
        holder.nm1.setText(sh.getSalon_location());
        holder.offretxt.setVisibility(View.GONE);
        holder.ratingBar.setEnabled(false);
        holder.ratingBar.setRating(Float.parseFloat(sh.getSalon_rating()));

        if (frm.equals("shops")) {
            holder.nm2.setText(sh.getSalon_total_reviwes() + " Reviews");
            holder.nm3.setText(sh.getSalon_happy_customers() + "% Happy Customers");
        } else {
            holder.offretxt.setVisibility(View.VISIBLE);
            holder.offretxt.setText(sh.getOffer_title());
            holder.nm3.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frm.equals("shops")) {
                    switch (use_for) {
                        case "near_by":
                            h.changeFragment(new Service_Details(h, sh.getSalon_id(),use_for), 0, SERVICES, "SERVICE_D","user");
                            HomeNew.HOME_LEVEL_2 = SERVICES;
                            break;
                        case "search_all_service":
                            h.changeFragment(new Service_Details(h, sh.getSalon_id(),use_for), 0, SERVICES, "SERVICE_D","user");
                            HomeNew.HOME_LEVEL_2 = SERVICES;
                            break;
                        case "user_search":
                            h.changeFragment(new Service_Details(h, sh.getSalon_id(),use_for), 1, SERVICES, "SERVICE_D","user");
                            HomeNew.SEARCH_LEVEL_1 = SERVICES;
                            break;
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return shopsList.size();
    }

    public interface changeFrag {
        public void chnge();
    }

    public class Myviewholder extends RecyclerView.ViewHolder {

        RatingBar ratingBar;
        CustomTextviewRegular nm, nm1, nm2, nm3;
        ImageView shopImage;
        CustomTextviewSemiBold offretxt;

        public Myviewholder(View itemView) {
            super(itemView);
            nm = itemView.findViewById(R.id.nm);
            nm1 = itemView.findViewById(R.id.nm1);
            nm2 = itemView.findViewById(R.id.nm3);
            nm3 = itemView.findViewById(R.id.nm4);
            offretxt = itemView.findViewById(R.id.offerTxt);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            shopImage = itemView.findViewById(R.id.shopImage);
        }
    }
}
