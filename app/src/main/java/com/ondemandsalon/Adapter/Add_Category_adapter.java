package com.ondemandsalon.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.ArrayList;

public class Add_Category_adapter extends RecyclerView.Adapter<Add_Category_adapter.MyViewHolder> {

    Context context;
    ArrayList<Service_data> slist;

    public Add_Category_adapter(Context context, ArrayList<Service_data> slist) {
        this.context = context;
        this.slist = slist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_service_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (position % 2 == 0) {
            holder.item_back.setBackgroundColor(context.getResources().getColor(R.color.smothgrey));
        } else {
            holder.item_back.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        Service_data d = slist.get(position);

 /*       holder.time.setText(d.getDuration());
        holder.pr.setText(d.getPrice());*/

    }

    @Override
    public int getItemCount() {
        return slist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout item_back;
        CustomTextviewRegular time, pr;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_back = itemView.findViewById(R.id.item_back);
            time = itemView.findViewById(R.id.time);
            pr = itemView.findViewById(R.id.pr);
        }
    }
}
