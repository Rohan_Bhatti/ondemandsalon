package com.ondemandsalon.helper;

import android.content.Context;
import android.content.SharedPreferences;


public class Preferences {
    public static final String PREF_NAME = "on_demand_saloon";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String USER_ID = "user_id";
    private static final String USER_NAME = "user_name";
    private static final String USER_PHONE = "user_phone";
    private static final String USER_LOCATION = "user_location";
    private static final String CATEGORY_SERVICE = "category_service";
    private static final String WORKING_HOURS = "working_hours";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_AVATAR = "user_avatar";
    private static final String LAT = "lat";
    private static final String LONG = "long";
    private static final String ROLE = "role";
    private static final String STATUS = "status";
    private static final String LOGIN = "login";
    private static final String USER_LOGIN = "user_login";
    private static final String PROVIDER_LOGIN = "provider_login";
    private static final String LAST_LOGIN = "last_login";
    private static final String CLIENT_ID = "user_id_api";
    private static final String PROVIDER_ID = "provider_id_api";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    public Preferences(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public boolean isFirstTimeLaunch() {
        return preferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public void logout() {

        editor.clear();
        editor.commit();

    }

    public void setUserData(String user_id, String user_name, String user_phone, String user_location, String category_service, String working_hours, String user_email, String user_avatar, String latitude, String longitude, String role, String status) {

        editor.putString(USER_ID, user_id);
        editor.putString(USER_NAME, user_name);
        editor.putString(USER_PHONE, user_phone);
        editor.putString(USER_LOCATION, user_location);
        editor.putString(CATEGORY_SERVICE, category_service);
        editor.putString(WORKING_HOURS, working_hours);
        editor.putString(USER_EMAIL, user_email);
        editor.putString(USER_AVATAR, user_avatar);
        editor.putString(LAT, latitude);
        editor.putString(LONG, longitude);
        editor.putString(ROLE, role);
        editor.putString(STATUS, status);
        editor.commit();

    }

    public boolean getLogin() {
        return preferences.getBoolean(LOGIN, false);
    }

    public void setLogin(boolean b) {
        editor.putBoolean(LOGIN, b);
        editor.commit();

    }

    public boolean getUserLogin() {
        return preferences.getBoolean(USER_LOGIN, false);
    }

    public void setUserLogin(boolean b) {
        editor.putBoolean(USER_LOGIN, b);
        editor.commit();
    }

    public boolean getProviderLogin() {
        return preferences.getBoolean(PROVIDER_LOGIN, false);
    }

    public void setProviderLogin(boolean b) {
        editor.putBoolean(PROVIDER_LOGIN, b);
        editor.commit();
    }

    public void lastLogin(String type) {
        editor.putString(LAST_LOGIN, type);
        editor.commit();
    }

    public String getlastLogin() {
        return preferences.getString(LAST_LOGIN, "user");
    }

    public void setData(String nm, String email, String num) {
        editor.putString(USER_NAME, nm);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_PHONE, num);
        editor.commit();
    }

    public String getName() {
        return preferences.getString(USER_NAME, "Laura Michelle");
    }

    public String getMail() {
        return preferences.getString(USER_EMAIL, "laura@gmail.com");
    }

    public String getNumber() {
        return preferences.getString(USER_PHONE, "+919865326598");
    }

    public void logoutUser() {
        editor.putBoolean(USER_LOGIN, false);
        editor.commit();
    }

    public void logoutProvider() {
        editor.putBoolean(PROVIDER_LOGIN, false);
        editor.commit();
    }

    public void setClientID(String id) {
        editor.putString(CLIENT_ID, id);
        editor.commit();
    }

    public String getClientID() {
        return preferences.getString(CLIENT_ID, "");
    }

    public void setProviderId(String id) {
        editor.putString(PROVIDER_ID, id);
        editor.commit();
    }

    public String getProviderId() {
        return preferences.getString(PROVIDER_ID, "");
    }
}
