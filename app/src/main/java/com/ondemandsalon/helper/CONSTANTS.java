package com.ondemandsalon.helper;

public class CONSTANTS {

    public static final String YOUR_LOCATION = "Your Location v";

    public static final String SEARCH_SERVICES = "Search Services";

    public static final String MY_APPOINTMENTS = "My Appointments";

    public static final String PROFILE = "Profile";

    public static final String DASHBOARD = "Dashboard";

    public static final String ALL_SERVICES = "All Services";

    public static final String SERVICES = "Services";

    public static final String ACCOUNT = "Account";

    public static final String RATING_AND_REVIEWS = "Rating & Reviews";

    public static final String CLIENT = "Clients List";

    public static final String APPOINTMENT = "Appointments List";

    public static final String EMPLOYEE_LIST = "Employee List";

}
