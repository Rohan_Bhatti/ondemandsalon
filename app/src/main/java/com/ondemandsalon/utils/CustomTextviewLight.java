package com.ondemandsalon.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class CustomTextviewLight extends AppCompatTextView {

    public static final String fontname = "font/titillium_light.otf";


    public CustomTextviewLight(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    public CustomTextviewLight(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    public CustomTextviewLight(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
    }
}
