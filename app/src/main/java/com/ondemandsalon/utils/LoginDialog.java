package com.ondemandsalon.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.ForgotPasswordActivity;
import com.ondemandsalon.helper.OnResponseListener;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginDialog {

    Context context;
    Dialog dialog;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;

    public LoginDialog(Context context) {
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
        preferences = new Preferences(context);
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void openLogin(final String type, final OnResponseListener onResponseListener) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.login_dialog);

        CustomTextviewRegular forgotpassword;
        forgotpassword = dialog.findViewById(R.id.forgotTxt);
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                context.startActivity(new Intent(context, ForgotPasswordActivity.class));
            }
        });
        ImageView closeImage = dialog.findViewById(R.id.closeImage);
        RelativeLayout signIn = dialog.findViewById(R.id.signInRel);
        CustomTextviewRegular signupTxt = dialog.findViewById(R.id.signupTxt);

        final CustomEdittextRegular emailEdt, passswordEdt;
        emailEdt = dialog.findViewById(R.id.emailEdt);
        passswordEdt = dialog.findViewById(R.id.passwordEdt);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onResponseListener.onResponce(0);
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(emailEdt, passswordEdt, type, onResponseListener);
            }
        });

        signupTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (type.equals("user"))
                    onResponseListener.onResponce(3);
                else
                    onResponseListener.onResponce(4);
            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void validate(CustomEdittextRegular emailEdt, CustomEdittextRegular passswordEdt, String type, OnResponseListener onResponseListener) {

        if (!validateEmail(emailEdt)) {
            return;
        }

        if (!validatePassword(passswordEdt)) {
            return;
        }

        if (connectionDetector.isNetworkAvailable()) {
            login(emailEdt, passswordEdt, type, onResponseListener);
        } else
            connectionDetector.error_Dialog();

    }

    private void login(CustomEdittextRegular emailEdt, CustomEdittextRegular passswordEdt, final String type, final OnResponseListener onResponseListener) {

        progDialog.ProgressDialogStart();
        JSONObject params = new JSONObject();
        final String eml = emailEdt.getText().toString();
        final String psd = passswordEdt.getText().toString();

        try {
            params.accumulate("user_email", eml);
            params.accumulate("user_password", psd);
            params.accumulate("role", "2");
            params.accumulate("device_type", "1");
            params.accumulate("device_token", "123");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        json j = new json(API.LOGIN, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
                System.out.println(response.toString());
                try {
                    if (response.getString("status").equals("1")) {

                        if (type.equals("user")) {

                            JSONObject jsonObject = response.getJSONObject("user_details");
                            preferences.setClientID(jsonObject.getString("id"));
                            preferences.setLogin(true);
                            preferences.setUserLogin(true);
                            preferences.lastLogin("user");
                            if (dialog.isShowing())
                                dialog.dismiss();
                        } else {
                            JSONObject jsonObject = response.getJSONObject("provider_details");
                            preferences.setProviderId(jsonObject.getString("id"));
                            preferences.setLogin(true);
                            preferences.setProviderLogin(true);
                            preferences.lastLogin("provider");
                            if (dialog.isShowing())
                                dialog.dismiss();
                        }

                        onResponseListener.onResponce(1);
                    } else {
                        onResponseListener.onResponce(2);
                        Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                onResponseListener.onResponce(2);
                Toast.makeText(context, "Something went's wrong", Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(j);


    }

    private boolean validateEmail(CustomEdittextRegular emailEdt) {
        String e = emailEdt.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        } else if (!isValidEmail(e)) {
            Toast.makeText(context, "Please enter valid email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        }
        return true;
    }

    private boolean validatePassword(CustomEdittextRegular passswordEdt) {
        if (passswordEdt.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter your password", Toast.LENGTH_SHORT).show();
            requestFocus(passswordEdt);
            return false;
        } else if (passswordEdt.getText().toString().trim().length() < 4) {
            Toast.makeText(context, "Please enter valid password", Toast.LENGTH_SHORT).show();
            requestFocus(passswordEdt);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
