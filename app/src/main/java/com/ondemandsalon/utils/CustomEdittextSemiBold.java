package com.ondemandsalon.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class CustomEdittextSemiBold extends AppCompatEditText {

    public static final String fontname = "font/titillium_semibold.otf";

    public CustomEdittextSemiBold(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    public CustomEdittextSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    public CustomEdittextSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }
}
