package com.ondemandsalon.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class CustomEdittextLight extends AppCompatEditText {

    public static final String fontname = "font/titillium_light.otf";

    public CustomEdittextLight(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    public CustomEdittextLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    public CustomEdittextLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
