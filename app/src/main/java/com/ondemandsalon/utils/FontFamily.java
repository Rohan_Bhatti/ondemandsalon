package com.ondemandsalon.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by __urvish on 24-07-2018.
 */

public class FontFamily {

    Context context;
    public Typeface titillium_regular;
    public FontFamily(Context context)
    {
        this.context = context;

        titillium_regular = Typeface.createFromAsset(context.getApplicationContext().getAssets(),  "font/titillium_regular.otf");
    }

}
