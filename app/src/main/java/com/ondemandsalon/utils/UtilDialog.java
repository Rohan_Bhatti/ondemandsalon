package com.ondemandsalon.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ondemandsalon.R;
import com.ondemandsalon.helper.ClickListener;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.List;

public class UtilDialog {

    Context c;
    AlertDialog.Builder builder;
    NumberPicker numbers, hoursMin;
    CardView done;
    FontFamily fontFamily;
    BottomSheetDialog dialog;
    View view;
    ListView lsview;
    ArrayAdapter<String> listAdapter;
    TextView txt;


    public UtilDialog(Context context) {
        c = context;
        builder = new AlertDialog.Builder(context);
        fontFamily = new FontFamily(c);
        dialog = new BottomSheetDialog(context);
    }

    public void openListDialog(String title, final String[] items, final TextView txt) {
        builder.setTitle(title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                txt.setText(items[which]);
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void openListWithoutDialog(final String[] items, final TextView txt) {
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                txt.setText(items[which]);
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void SelectTimeDialog(String title, final TextView txt) {
//        View v = LayoutInflater.from(c).inflate(R.layout.time_picker, c, false);

        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.time_picker, null);
        builder.setView(dialogView);
        builder.setCancelable(false);

        final AlertDialog dialog = builder.create();
        numbers = dialogView.findViewById(R.id.numbers);
        hoursMin = dialogView.findViewById(R.id.hoursMin);
        done = dialogView.findViewById(R.id.done);

        numbers.setTypeface(fontFamily.titillium_regular);
        numbers.setTextSize(45f);
        numbers.setSelectedTextSize(60f);
        numbers.setWrapSelectorWheel(true);
        numbers.setMinValue(1);
        hoursMin.setMinValue(1);

        hoursMin.setTextSize(45f);
        hoursMin.setSelectedTextSize(60f);

        final String[] minutesArray = new String[12];
        final String[] hoursrray = new String[24];

        int m = 0;
        for (int i = 0; i < 12; i++) {
            minutesArray[i] = String.valueOf(m) + "m";
            m = m + 5;
        }

        for (int i = 0; i < 24; i++) {
            hoursrray[i] = String.valueOf(i) + "h";
        }

        numbers.setMaxValue(hoursrray.length);
        hoursMin.setMaxValue(minutesArray.length);
        hoursMin.setTypeface(fontFamily.titillium_regular);
        hoursMin.setDisplayedValues(minutesArray);
        numbers.setDisplayedValues(hoursrray);
        hoursMin.setWrapSelectorWheel(true);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*    String str;
                if (hoursMin.getValue() == 1) {
                    str = "H";
                } else
                    str = "Min";*/
                txt.setText(hoursrray[numbers.getValue() - 1] + ":" + minutesArray[hoursMin.getValue() - 1]);
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    public void showBottomDialog(View view) {
        dialog.setContentView(view);
        if (!dialog.isShowing())
            dialog.show();
    }

    public void dismissBottomDialog() {
        if (dialog.isShowing())
            dialog.dismiss();
    }


    public void listBottomDialog(TextView textView, final List<String> strings, final ClickListener clickListener) {
        txt = textView;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = LayoutInflater.from(c).inflate(R.layout.recycler_dialog, null);

        lsview = view.findViewById(R.id.lsview);
        listAdapter = new ArrayAdapter<>(c, R.layout.bottomlist_row_item, strings);
        lsview.setAdapter(listAdapter);

        lsview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt.setText(strings.get(position));
                dialog.dismiss();
                clickListener.onSelect(strings.get(position));
            }
        });

        dialog.setContentView(view);
        if (!dialog.isShowing())
            dialog.show();
    }


}
