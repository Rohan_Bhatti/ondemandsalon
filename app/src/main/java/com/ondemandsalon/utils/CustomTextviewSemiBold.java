package com.ondemandsalon.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class CustomTextviewSemiBold extends AppCompatTextView {


    public static final String fontname = "font/titillium_semibold.otf";

    public CustomTextviewSemiBold(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    public CustomTextviewSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }

    public CustomTextviewSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontname);
        this.setTypeface(face);
    }
}
