package com.ondemandsalon.model;

public class User_Setting_Model {

    String set_ans_id, set_answer_list, set_answer_que_id;

    public User_Setting_Model(String set_ans_id, String set_answer_list, String set_answer_que_id) {
        this.set_ans_id = set_ans_id;
        this.set_answer_list = set_answer_list;
        this.set_answer_que_id = set_answer_que_id;
    }

    public String getSet_ans_id() {
        return set_ans_id;
    }

    public void setSet_ans_id(String set_ans_id) {
        this.set_ans_id = set_ans_id;
    }

    public String getSet_answer_list() {
        return set_answer_list;
    }

    public void setSet_answer_list(String set_answer_list) {
        this.set_answer_list = set_answer_list;
    }

    public String getSet_answer_que_id() {
        return set_answer_que_id;
    }

    public void setSet_answer_que_id(String set_answer_que_id) {
        this.set_answer_que_id = set_answer_que_id;
    }
}
