package com.ondemandsalon.model;


public class Provider_Client {

    String name, mobileno, location;

    public Provider_Client(String name, String mobileno, String location) {
        this.location = location;
        this.name = name;
        this.mobileno = mobileno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
