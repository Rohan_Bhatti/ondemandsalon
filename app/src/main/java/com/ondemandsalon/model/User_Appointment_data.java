package com.ondemandsalon.model;

public class User_Appointment_data {


    String app_status, app_salon_name, app_salon_phone, app_service_time, app_service_date, services;

    public User_Appointment_data(String app_status, String app_salon_name, String app_salon_phone, String app_service_time, String app_service_date, String services) {
        this.app_status = app_status;
        this.app_salon_name = app_salon_name;
        this.app_salon_phone = app_salon_phone;
        this.app_service_time = app_service_time;
        this.app_service_date = app_service_date;
        this.services = services;
    }

    public String getApp_status() {
        return app_status;
    }

    public void setApp_status(String app_status) {
        this.app_status = app_status;
    }

    public String getApp_salon_name() {
        return app_salon_name;
    }

    public void setApp_salon_name(String app_salon_name) {
        this.app_salon_name = app_salon_name;
    }

    public String getApp_salon_phone() {
        return app_salon_phone;
    }

    public void setApp_salon_phone(String app_salon_phone) {
        this.app_salon_phone = app_salon_phone;
    }

    public String getApp_service_time() {
        return app_service_time;
    }

    public void setApp_service_time(String app_service_time) {
        this.app_service_time = app_service_time;
    }

    public String getApp_service_date() {
        return app_service_date;
    }

    public void setApp_service_date(String app_service_date) {
        this.app_service_date = app_service_date;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }
}
