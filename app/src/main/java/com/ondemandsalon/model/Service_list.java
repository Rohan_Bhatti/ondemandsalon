package com.ondemandsalon.model;

public class Service_list {

    String cat_name, ser_cat_parent, ser_cat_price, ser_cat_time;

    public Service_list(String cat_name, String ser_cat_parent, String ser_cat_price, String ser_cat_time) {
        this.cat_name = cat_name;
        this.ser_cat_parent = ser_cat_parent;
        this.ser_cat_price = ser_cat_price;
        this.ser_cat_time = ser_cat_time;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getSer_cat_parent() {
        return ser_cat_parent;
    }

    public void setSer_cat_parent(String ser_cat_parent) {
        this.ser_cat_parent = ser_cat_parent;
    }

    public String getSer_cat_price() {
        return ser_cat_price;
    }

    public void setSer_cat_price(String ser_cat_price) {
        this.ser_cat_price = ser_cat_price;
    }

    public String getSer_cat_time() {
        return ser_cat_time;
    }

    public void setSer_cat_time(String ser_cat_time) {
        this.ser_cat_time = ser_cat_time;
    }
}
