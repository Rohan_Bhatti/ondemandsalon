package com.ondemandsalon.model;

public class Ratings_Data {

    String salon_name, rating_date, average_rating, rating_description, rat_time, rat_quality, rat_safety, rat_result, rat_proffesnalism, rat_rewarding;

    public Ratings_Data(String salon_name, String rating_date, String average_rating, String rating_description, String rat_time, String rat_quality, String rat_safety, String rat_result, String rat_proffesnalism, String rat_rewarding) {
        this.salon_name = salon_name;
        this.rating_date = rating_date;
        this.average_rating = average_rating;
        this.rating_description = rating_description;
        this.rat_time = rat_time;
        this.rat_quality = rat_quality;
        this.rat_safety = rat_safety;
        this.rat_proffesnalism = rat_proffesnalism;
        this.rat_result = rat_result;
        this.rat_rewarding = rat_rewarding;
    }

    public Ratings_Data(String salon_name, String rating_date, String average_rating, String rating_description) {
        this.salon_name = salon_name;
        this.rating_date = rating_date;
        this.average_rating = average_rating;
        this.rating_description = rating_description;
    }

    public String getSalon_name() {
        return salon_name;
    }

    public void setSalon_name(String salon_name) {
        this.salon_name = salon_name;
    }

    public String getRating_date() {
        return rating_date;
    }

    public void setRating_date(String rating_date) {
        this.rating_date = rating_date;
    }

    public String getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(String average_rating) {
        this.average_rating = average_rating;
    }

    public String getRating_description() {
        return rating_description;
    }

    public void setRating_description(String rating_description) {
        this.rating_description = rating_description;
    }

    public String getRat_time() {
        return rat_time;
    }

    public void setRat_time(String rat_time) {
        this.rat_time = rat_time;
    }

    public String getRat_quality() {
        return rat_quality;
    }

    public void setRat_quality(String rat_quality) {
        this.rat_quality = rat_quality;
    }

    public String getRat_safety() {
        return rat_safety;
    }

    public void setRat_safety(String rat_safety) {
        this.rat_safety = rat_safety;
    }

    public String getRat_result() {
        return rat_result;
    }

    public void setRat_result(String rat_result) {
        this.rat_result = rat_result;
    }

    public String getRat_proffesnalism() {
        return rat_proffesnalism;
    }

    public void setRat_proffesnalism(String rat_proffesnalism) {
        this.rat_proffesnalism = rat_proffesnalism;
    }

    public String getRat_rewarding() {
        return rat_rewarding;
    }

    public void setRat_rewarding(String rat_rewarding) {
        this.rat_rewarding = rat_rewarding;
    }
}
