package com.ondemandsalon.model;

public class Sub_cat {
    String sub_cat_name, sub_cat_id, parent_cat_id, user_id, user_name, user_rating, user_description, user_profile_image, rating_time;
    String emp_id, emp_fname, emp_lname, emp_image, sub_cat_price;

    public Sub_cat(String sub_cat_name, String sub_cat_id, String parent_cat_id) {
        this.sub_cat_id = sub_cat_id;
        this.sub_cat_name = sub_cat_name;
        this.parent_cat_id = parent_cat_id;
    }

    public Sub_cat(String sub_cat_name, String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
        this.sub_cat_name = sub_cat_name;
    }

    public Sub_cat(String user_name, String user_rating, String user_description, String user_profile_image, String rating_time, String user_id) {
        this.user_name = user_name;
        this.user_rating = user_rating;
        this.user_description = user_description;
        this.user_profile_image = user_profile_image;
        this.rating_time = rating_time;
        this.user_id = user_id;
    }

    public Sub_cat(String emp_id, String emp_fname, String emp_lname, String emp_image) {
        this.emp_id = emp_id;
        this.emp_fname = emp_fname;
        this.emp_image = emp_image;
        this.emp_lname = emp_lname;
    }

    public Sub_cat(String sub_cat_name, String sub_cat_id, String sub_cat_price, String s, String s1) {
        this.sub_cat_id = sub_cat_id;
        this.sub_cat_name = sub_cat_name;
        this.sub_cat_price = sub_cat_price;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getParent_cat_id() {
        return parent_cat_id;
    }

    public void setParent_cat_id(String parent_cat_id) {
        this.parent_cat_id = parent_cat_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_rating() {
        return user_rating;
    }

    public void setUser_rating(String user_rating) {
        this.user_rating = user_rating;
    }

    public String getUser_description() {
        return user_description;
    }

    public void setUser_description(String user_description) {
        this.user_description = user_description;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getRating_time() {
        return rating_time;
    }

    public void setRating_time(String rating_time) {
        this.rating_time = rating_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_fname() {
        return emp_fname;
    }

    public void setEmp_fname(String emp_fname) {
        this.emp_fname = emp_fname;
    }

    public String getEmp_lname() {
        return emp_lname;
    }

    public void setEmp_lname(String emp_lname) {
        this.emp_lname = emp_lname;
    }

    public String getEmp_image() {
        return emp_image;
    }

    public void setEmp_image(String emp_image) {
        this.emp_image = emp_image;
    }

    public String getSub_cat_price() {
        return sub_cat_price;
    }

    public void setSub_cat_price(String sub_cat_price) {
        this.sub_cat_price = sub_cat_price;
    }
}
