package com.ondemandsalon.model;

import java.util.ArrayList;

public class Categeory_Filter {

    String sub_cat_parent_id;
    ArrayList<Service_data> booked_sub_categories;

    public String getSub_cat_parent_id() {
        return sub_cat_parent_id;
    }

    public void setSub_cat_parent_id(String sub_cat_parent_id) {
        this.sub_cat_parent_id = sub_cat_parent_id;
    }

    public ArrayList<Service_data> getBooked_sub_categories() {
        return booked_sub_categories;
    }

    public void setBooked_sub_categories(ArrayList<Service_data> booked_sub_categories) {
        this.booked_sub_categories = booked_sub_categories;
    }

    public Categeory_Filter(String sub_cat_parent_id, ArrayList<Service_data> booked_sub_categories) {
        this.sub_cat_parent_id = sub_cat_parent_id;
        this.booked_sub_categories = booked_sub_categories;

    }
}
