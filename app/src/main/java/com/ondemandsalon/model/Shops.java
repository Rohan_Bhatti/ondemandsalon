package com.ondemandsalon.model;

import java.util.ArrayList;

public class Shops {

    String offer_title, date, offer_description, salon_image, salon_name, salon_location, salon_rating, from_date, to_date, salon_total_reviwes, salon_happy_customers, salon_id;
    ArrayList<Sub_cat> salon_sub_cat, booked_sub_categories;
    String cat_id, cat_name, booked_main_category_id;

    String salon_city, salon_min_price, salon_max_price, salon_country;

    public Shops(String offer_title, String date, String offer_description, String salon_image, String salon_name, String salon_location, String salon_rating, String from_date, String to_date, String salon_id) {
        this.offer_title = offer_title;
        this.date = date;
        this.offer_description = offer_description;
        this.salon_image = salon_image;
        this.salon_name = salon_name;
        this.salon_location = salon_location;
        this.salon_rating = salon_rating;
        this.from_date = from_date;
        this.to_date = to_date;
        this.salon_id = salon_id;
    }

    public Shops(String salon_image, String salon_name, String salon_location, String salon_rating, String salon_total_reviews, String salon_happy_customers, String salon_id) {
        this.salon_image = salon_image;
        this.salon_name = salon_name;
        this.salon_location = salon_location;
        this.salon_rating = salon_rating;
        this.salon_total_reviwes = salon_total_reviews;
        this.salon_happy_customers = salon_happy_customers;
        this.salon_id = salon_id;
    }

    public Shops(String salon_id, String salon_image, String salon_name, String salon_location, String salon_city, String salon_rating, String salon_total_reviews, String salon_happy_customers, String salon_min_price, String salon_max_price, String salon_country, ArrayList<Sub_cat> salon_sub_cat) {
        this.salon_id = salon_id;
        this.salon_image = salon_image;
        this.salon_name = salon_name;
        this.salon_location = salon_location;
        this.salon_city = salon_city;
        this.salon_rating = salon_rating;
        this.salon_total_reviwes = salon_total_reviews;
        this.salon_happy_customers = salon_happy_customers;
        this.salon_min_price = salon_min_price;
        this.salon_max_price = salon_max_price;
        this.salon_country = salon_country;
        this.salon_sub_cat = salon_sub_cat;
    }

    public Shops(String cat_id, String cat_name, ArrayList<Sub_cat> main_sub_categories) {
        this.cat_name = cat_name;
        this.cat_id = cat_id;
        this.salon_sub_cat = main_sub_categories;
    }

    public Shops(String salon_id, String salon_image, String salon_name, String salon_location, String salon_city, String salon_rating, String salon_total_reviews, String salon_happy_customers) {
        this.salon_id = salon_id;
        this.salon_image = salon_image;
        this.salon_name = salon_name;
        this.salon_location = salon_location;
        this.salon_city = salon_city;
        this.salon_rating = salon_rating;
        this.salon_total_reviwes = salon_total_reviews;
        this.salon_happy_customers = salon_happy_customers;
    }

    public Shops(String sub_cat_parent_id, ArrayList<Sub_cat> sub_categories) {
        this.booked_main_category_id = sub_cat_parent_id;
        this.booked_sub_categories = sub_categories;
    }

    public String getOffer_title() {
        return offer_title;
    }

    public void setOffer_title(String offer_title) {
        this.offer_title = offer_title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOffer_description() {
        return offer_description;
    }

    public void setOffer_description(String offer_description) {
        this.offer_description = offer_description;
    }

    public String getSalon_image() {
        return salon_image;
    }

    public void setSalon_image(String salon_image) {
        this.salon_image = salon_image;
    }

    public String getSalon_name() {
        return salon_name;
    }

    public void setSalon_name(String salon_name) {
        this.salon_name = salon_name;
    }

    public String getSalon_location() {
        return salon_location;
    }

    public void setSalon_location(String salon_location) {
        this.salon_location = salon_location;
    }

    public String getSalon_rating() {
        return salon_rating;
    }

    public void setSalon_rating(String salon_rating) {
        this.salon_rating = salon_rating;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getSalon_total_reviwes() {
        return salon_total_reviwes;
    }

    public void setSalon_total_reviwes(String salon_total_reviwes) {
        this.salon_total_reviwes = salon_total_reviwes;
    }

    public String getSalon_happy_customers() {
        return salon_happy_customers;
    }

    public void setSalon_happy_customers(String salon_happy_customers) {
        this.salon_happy_customers = salon_happy_customers;
    }

    public String getSalon_id() {
        return salon_id;
    }

    public void setSalon_id(String salon_id) {
        this.salon_id = salon_id;
    }

    public ArrayList<Sub_cat> getSalon_sub_cat() {
        return salon_sub_cat;
    }

    public void setSalon_sub_cat(ArrayList<Sub_cat> salon_sub_cat) {
        this.salon_sub_cat = salon_sub_cat;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getSalon_city() {
        return salon_city;
    }

    public void setSalon_city(String salon_city) {
        this.salon_city = salon_city;
    }

    public String getSalon_min_price() {
        return salon_min_price;
    }

    public void setSalon_min_price(String salon_min_price) {
        this.salon_min_price = salon_min_price;
    }

    public String getSalon_max_price() {
        return salon_max_price;
    }

    public void setSalon_max_price(String salon_max_price) {
        this.salon_max_price = salon_max_price;
    }

    public String getSalon_country() {
        return salon_country;
    }

    public void setSalon_country(String salon_country) {
        this.salon_country = salon_country;
    }

    public ArrayList<Sub_cat> getBooked_sub_categories() {
        return booked_sub_categories;
    }

    public void setBooked_sub_categories(ArrayList<Sub_cat> booked_sub_categories) {
        this.booked_sub_categories = booked_sub_categories;
    }

    public String getBooked_main_category_id() {
        return booked_main_category_id;
    }

    public void setBooked_main_category_id(String booked_main_category_id) {
        this.booked_main_category_id = booked_main_category_id;
    }
}
