package com.ondemandsalon.model;

public class User_Home_Category {

    String nm;
    String img;
    String id;


    public User_Home_Category(String nm, String img, String service_id) {
        this.nm = nm;
        this.img = img;
        this.id = service_id;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
