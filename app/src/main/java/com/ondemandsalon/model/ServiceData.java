package com.ondemandsalon.model;

public class ServiceData {

    String ser_id, ser_name, ser_created_by, ser_status;

    public String getSer_id() {
        return ser_id;
    }

    public void setSer_id(String ser_id) {
        this.ser_id = ser_id;
    }

    public String getSer_name() {
        return ser_name;
    }

    public void setSer_name(String ser_name) {
        this.ser_name = ser_name;
    }

    public String getSer_created_by() {
        return ser_created_by;
    }

    public void setSer_created_by(String ser_created_by) {
        this.ser_created_by = ser_created_by;
    }

    public String getSer_status() {
        return ser_status;
    }

    public void setSer_status(String ser_status) {
        this.ser_status = ser_status;
    }

    public ServiceData(String ser_id, String ser_name, String ser_created_by, String ser_status) {
        this.ser_id = ser_id;
        this.ser_name = ser_name;
        this.ser_created_by = ser_created_by;
        this.ser_status = ser_status;
    }

}
