package com.ondemandsalon.model;

public class Provider_Appo_data {

    String sana_saikh, color, s, today, type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Provider_Appo_data(String sana_saikh, String color, String s, String today, String type) {
        this.sana_saikh = sana_saikh;
        this.color = color;
        this.s = s;

        this.type = type;
        this.today = today;
    }

    public String getSana_saikh() {
        return sana_saikh;
    }

    public void setSana_saikh(String sana_saikh) {
        this.sana_saikh = sana_saikh;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }
}
