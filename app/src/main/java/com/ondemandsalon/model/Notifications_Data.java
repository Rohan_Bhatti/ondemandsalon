package com.ondemandsalon.model;

public class Notifications_Data {

    String notification_text, created_date, not_id, not_provider_id, business_name, user_avatar;

    public String getNotification_text() {
        return notification_text;
    }

    public void setNotification_text(String notification_text) {
        this.notification_text = notification_text;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getNot_id() {
        return not_id;
    }

    public void setNot_id(String not_id) {
        this.not_id = not_id;
    }

    public String getNot_provider_id() {
        return not_provider_id;
    }

    public void setNot_provider_id(String not_provider_id) {
        this.not_provider_id = not_provider_id;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public Notifications_Data(String notification_text, String created_date, String not_id, String not_provider_id, String business_name, String user_avatar) {
        this.notification_text = notification_text;
        this.created_date = created_date;
        this.not_id = not_id;

        this.not_provider_id = not_provider_id;
        this.business_name = business_name;
        this.user_avatar = user_avatar;
    }
}
