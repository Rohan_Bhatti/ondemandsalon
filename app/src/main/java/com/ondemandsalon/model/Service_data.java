package com.ondemandsalon.model;

public class Service_data {

    String sub_cat_name, sub_cat_price, sub_cat_image, sub_cat_time, sub_cat_parent_id, sub_cat_id, salon_contct_no;
    int status;
    String service_id, service_name, service_price, service_time, service_parent_id;
    String day, from, to;

    public Service_data(String sub_cat_name, int status, String sub_cat_price, String sub_cat_image, String sub_cat_time, String sub_cat_parent_id, String sub_cat_id, String salon_contact_no) {
        this.sub_cat_name = sub_cat_name;
        this.status = status;
        this.sub_cat_price = sub_cat_price;
        this.sub_cat_image = sub_cat_image;
        this.sub_cat_time = sub_cat_time;
        this.sub_cat_parent_id = sub_cat_parent_id;
        this.sub_cat_id = sub_cat_id;
        this.salon_contct_no = salon_contact_no;

    }

    public Service_data(String service_id, String service_name, String service_price, String service_time, String service_parent_id) {
        this.service_id = service_id;
        this.service_name = service_name;
        this.service_price = service_price;
        this.service_time = service_time;
        this.service_parent_id = service_parent_id;
    }

    public Service_data(String day, String from, String to) {
        this.day = day;
        this.from = from;
        this.to = to;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getSub_cat_price() {
        return sub_cat_price;
    }

    public void setSub_cat_price(String sub_cat_price) {
        this.sub_cat_price = sub_cat_price;
    }

    public String getSub_cat_image() {
        return sub_cat_image;
    }

    public void setSub_cat_image(String sub_cat_image) {
        this.sub_cat_image = sub_cat_image;
    }

    public String getSub_cat_time() {
        return sub_cat_time;
    }

    public void setSub_cat_time(String sub_cat_time) {
        this.sub_cat_time = sub_cat_time;
    }

    public String getSub_cat_parent_id() {
        return sub_cat_parent_id;
    }

    public void setSub_cat_parent_id(String sub_cat_parent_id) {
        this.sub_cat_parent_id = sub_cat_parent_id;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getSalon_contct_no() {
        return salon_contct_no;
    }

    public void setSalon_contct_no(String salon_contct_no) {
        this.salon_contct_no = salon_contct_no;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_price() {
        return service_price;
    }

    public void setService_price(String service_price) {
        this.service_price = service_price;
    }

    public String getService_time() {
        return service_time;
    }

    public void setService_time(String service_time) {
        this.service_time = service_time;
    }

    public String getService_parent_id() {
        return service_parent_id;
    }

    public void setService_parent_id(String service_parent_id) {
        this.service_parent_id = service_parent_id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
