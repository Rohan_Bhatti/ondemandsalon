package com.ondemandsalon.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Appointment_list_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.User_Appointment_data;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomTextviewSemiBold;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class User_Appointments extends Fragment {

    RecyclerView appointmentlist;
    Context context;
    ArrayList<User_Appointment_data> upcoming_appointments, past_appointments;

    RelativeLayout services_btn, info_btn, note;

    CustomTextviewSemiBold btnServices, btnInfo;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;

    Preferences preferences;


    public User_Appointments() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.user_appointments, container, false);
        appointmentlist = view.findViewById(R.id.appointmentlist);
        services_btn = view.findViewById(R.id.services_btn);
        info_btn = view.findViewById(R.id.info_btn);
        note = view.findViewById(R.id.noteg);

        btnServices = view.findViewById(R.id.btnServices);
        btnInfo = view.findViewById(R.id.btnInfo);

        getAppointments();

        services_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnServices.setTextColor(getResources().getColor(R.color.white));
                btnServices.setBackgroundColor(getResources().getColor(R.color.blue));
                btnInfo.setTextColor(getResources().getColor(R.color.blue));
                btnInfo.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                if (upcoming_appointments != null && upcoming_appointments.size() != 0) {
                    appointmentlist.setAdapter(new Appointment_list_adapter(upcoming_appointments));
                    appointmentlist.setLayoutManager(new LinearLayoutManager(context));
                    note.setVisibility(View.GONE);
                    appointmentlist.setVisibility(View.VISIBLE);
                } else {
                    note.setVisibility(View.VISIBLE);
                    appointmentlist.setVisibility(View.GONE);
                }
            }
        });

        info_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnServices.setTextColor(getResources().getColor(R.color.blue));
                btnServices.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btnInfo.setTextColor(getResources().getColor(R.color.white));
                btnInfo.setBackgroundColor(getResources().getColor(R.color.blue));

                if (past_appointments != null && past_appointments.size() != 0) {
                    note.setVisibility(View.GONE);
                    appointmentlist.setVisibility(View.VISIBLE);
                    appointmentlist.setAdapter(new Appointment_list_adapter(past_appointments));
                    appointmentlist.setLayoutManager(new LinearLayoutManager(context));
                } else {
                    note.setVisibility(View.VISIBLE);
                    appointmentlist.setVisibility(View.GONE);
                }

            }
        });


        return view;
    }

    private void getAppointments() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("user_id", preferences.getClientID());
                jsonObject.accumulate("role", "2");

                json json = new json(API.APPOINTMENT_LIST_USER, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.getString("status").equals("1")) {
                                JSONArray jsonArray = response.getJSONArray("upcoming_appointment_list");
                                if (jsonArray.length() != 0) {
                                    upcoming_appointments = new ArrayList<User_Appointment_data>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                                        String app_status = jsonObject1.getString("app_status");
                                        String app_salon_name = jsonObject1.getString("business_name");
                                        String app_salon_phone = jsonObject1.getString("user_phone");
                                        String app_service_time = jsonObject1.getString("app_service_time");
                                        String app_service_date = jsonObject1.getString("app_date");
                                        JSONArray app_services = jsonObject1.getJSONArray("service_list");
                                        String services = "";
                                        if (app_services.length() != 0) {
                                            StringBuilder stringBuilder = new StringBuilder();
                                            for (int j = 0; j < app_services.length(); j++) {
                                                JSONObject jsonObject2 = (JSONObject) app_services.get(j);
                                                String service_name = jsonObject2.getString("ser_cs_name");
                                                stringBuilder.append(service_name + ", ");
                                            }
                                            services = stringBuilder.toString();
                                        }
                                        upcoming_appointments.add(new User_Appointment_data(app_status, app_salon_name, app_salon_phone, app_service_time, app_service_date, services));
                                    }
                                }

                                JSONArray jsonArray1 = response.getJSONArray("past_appointment_list");
                                if (jsonArray1.length() != 0) {
                                    past_appointments = new ArrayList<User_Appointment_data>();
                                    for (int i = 0; i < jsonArray1.length(); i++) {
                                        if (jsonArray1.get(i).equals(""))
                                        {
                                            note.setVisibility(View.VISIBLE);
                                            appointmentlist.setVisibility(View.GONE);
                                        }
                                        else {
                                            JSONObject jsonObject1 = (JSONObject) jsonArray1.get(i);
                                            String app_status = jsonObject1.getString("app_status");
                                            String app_salon_name = jsonObject1.getString("business_name");
                                            String app_salon_phone = jsonObject1.getString("user_phone");
                                            String app_service_time = jsonObject1.getString("app_service_time");
                                            String app_service_date = jsonObject1.getString("app_date");
                                            JSONArray app_services = jsonObject1.getJSONArray("service_list");
                                            String services = "";
                                            if (app_services.length() != 0) {
                                                StringBuilder stringBuilder = new StringBuilder();
                                                for (int j = 0; j < app_services.length(); j++) {
                                                    JSONObject jsonObject2 = (JSONObject) app_services.get(j);
                                                    String service_name = jsonObject2.getString("ser_cs_name");

                                                    stringBuilder.append(service_name + ", ");
                                                }
                                                services = stringBuilder.toString();
                                            }
                                            past_appointments.add(new User_Appointment_data(app_status, app_salon_name, app_salon_phone, app_service_time, app_service_date, services));
                                        }
                                    }
                                }

                                if (upcoming_appointments != null && upcoming_appointments.size() != 0) {
                                    btnServices.setTextColor(getResources().getColor(R.color.white));
                                    btnServices.setBackgroundColor(getResources().getColor(R.color.blue));
                                    btnInfo.setTextColor(getResources().getColor(R.color.blue));
                                    btnInfo.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                                    appointmentlist.setAdapter(new Appointment_list_adapter(upcoming_appointments));
                                    appointmentlist.setLayoutManager(new LinearLayoutManager(context));

                                    note.setVisibility(View.GONE);
                                    appointmentlist.setVisibility(View.VISIBLE);

                                } else if (past_appointments != null && past_appointments.size() != 0) {
                                    btnServices.setTextColor(getResources().getColor(R.color.blue));
                                    btnServices.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                                    btnInfo.setTextColor(getResources().getColor(R.color.white));
                                    btnInfo.setBackgroundColor(getResources().getColor(R.color.blue));

                                    appointmentlist.setAdapter(new Appointment_list_adapter(past_appointments));
                                    appointmentlist.setLayoutManager(new LinearLayoutManager(context));

                                    note.setVisibility(View.GONE);
                                    appointmentlist.setVisibility(View.VISIBLE);
                                } else {
                                    note.setVisibility(View.VISIBLE);
                                    appointmentlist.setVisibility(View.GONE);
                                }
                            } else {
//                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                                note.setVisibility(View.VISIBLE);
                                appointmentlist.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
//                        Toast.makeText(context, "Somthing wents wrong\nPlease try again", Toast.LENGTH_SHORT).show();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

  /*  private void giveRatingUser() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("rat_out_of_5", "1");
                jsonObject.accumulate("rat_discription", "test test test");
                jsonObject.accumulate("rat_user_id", "1");
                jsonObject.accumulate("rat_salon_name", "enrich salon");

                json json = new json(API.REVIEW_RATING_BY_USER, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
        preferences = new Preferences(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
