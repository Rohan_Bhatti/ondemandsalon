package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Provider_Appointment_Adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.model.Provider_Appo_data;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;


public class Provider_Appointments extends Fragment {

    Context context;
    RelativeLayout from_date, to_date;
    CustomTextviewRegular from_date_txt, to_date_txt;
    RecyclerView upcoming_appointment_list_recycler;
    HomeNew p;
    ArrayList<Provider_Appo_data> lst;
    ImageView searchImg;
    RelativeLayout services_btn, info_btn;
    CustomTextviewSemiBold btnServices, btnInfo;
    private int mYear, mMonth, mDay;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;

    public Provider_Appointments() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Provider_Appointments(HomeNew provider) {
        p = provider;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.provider_appointments, container, false);
        from_date = view.findViewById(R.id.fromdate);
        to_date = view.findViewById(R.id.todate);
        from_date_txt = view.findViewById(R.id.from_date_txt);
        to_date_txt = view.findViewById(R.id.to_date_txt);

        services_btn = view.findViewById(R.id.services_btn);
        info_btn = view.findViewById(R.id.info_btn);

        btnServices = view.findViewById(R.id.btnServices);
        btnInfo = view.findViewById(R.id.btnInfo);

        searchImg = view.findViewById(R.id.searchImg);


        upcoming_appointment_list_recycler = view.findViewById(R.id.upcoming_appointment_list_recycler);


        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        from_date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        to_date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        getupcomingList();
        services_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnServices.setTextColor(getResources().getColor(R.color.white));
                btnServices.setBackgroundColor(getResources().getColor(R.color.blue));
                btnInfo.setTextColor(getResources().getColor(R.color.blue));
                btnInfo.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                getupcomingList();
            }
        });

        info_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnServices.setTextColor(getResources().getColor(R.color.blue));
                btnServices.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btnInfo.setTextColor(getResources().getColor(R.color.white));
                btnInfo.setBackgroundColor(getResources().getColor(R.color.blue));
                lst = new ArrayList<Provider_Appo_data>();
                lst.add(new Provider_Appo_data("Aasma", "facial", "5:00 PM", "Yesterday", "P"));
                lst.add(new Provider_Appo_data("Jerin", "Hair color", "8:00 PM", "Two Days Ago", "P"));
                lst.add(new Provider_Appo_data("William", "color", "9:00 PM", "25/07/2018", "P"));

                upcoming_appointment_list_recycler.setAdapter(new Provider_Appointment_Adapter(context, p, lst));
                upcoming_appointment_list_recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            }
        });


        return view;
    }

    private void getupcomingList() {

        lst = new ArrayList<Provider_Appo_data>();
        lst.add(new Provider_Appo_data("Sana Saikh", "color", "2:00 PM", "Today", "U"));
        lst.add(new Provider_Appo_data("Aasma", "Hair color", "3:00 PM", "Tommorow", "U"));
        lst.add(new Provider_Appo_data("Saista", "facial", "3:00 PM", "30/08/2018", "U"));

        upcoming_appointment_list_recycler.setAdapter(new Provider_Appointment_Adapter(context, p, lst));
        upcoming_appointment_list_recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);

    }

    private void List_upcoming_appointment() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("service_pro_id", "1");

                json json = new json(API.LIST_UPCOMING_APPOINTMENTS, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void List_past_appointment() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("service_pro_id", "1");

                json json = new json(API.LIST_OF_PAST_APPOINTMENT, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void Details_Of_appointment() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("app_user_id", "1");

                json json = new json(API.APPOINTMENT_DETAIL, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
