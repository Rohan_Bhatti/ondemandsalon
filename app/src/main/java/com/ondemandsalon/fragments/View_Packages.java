package com.ondemandsalon.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomViewPager;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;

public class View_Packages extends Fragment {

    CustomViewPager packages_viewpager;
    Package_Viewpager package_viewpager_adapter;
    CustomTextviewRegular standard, premier, advance;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;
    Context context;
    JSONObject Advance_package;
    JSONObject Standard_package;
    JSONObject Premium_package;


    public View_Packages() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view__packages, container, false);
        packages_viewpager = view.findViewById(R.id.packages_viewpager);
        standard = view.findViewById(R.id.standard_txt);
        advance = view.findViewById(R.id.advance_txt);
        premier = view.findViewById(R.id.premier_txt);

        getPackageData();
        standard.setEnabled(false);
        advance.setEnabled(false);
        premier.setEnabled(false);

        standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                packages_viewpager.setCurrentItem(0);
                advance.setAlpha((float) 0.4);
                premier.setAlpha((float) 0.4);
                standard.setAlpha((float) 1);
            }
        });

        advance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                packages_viewpager.setCurrentItem(1);
                standard.setAlpha((float) 0.4);
                premier.setAlpha((float) 0.4);
                advance.setAlpha((float) 1);
            }
        });

        premier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                packages_viewpager.setCurrentItem(2);
                standard.setAlpha((float) 0.4);
                advance.setAlpha((float) 0.4);
                premier.setAlpha((float) 1);
            }
        });


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.HOME_TOOL = YOUR_LOCATION;
                    HomeNew.toolTitle.setText(YOUR_LOCATION);
                    return true;
                }
                return false;
            }
        });


        return view;
    }

    private void getPackageData() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();

            json j = new json(API.VIEW_PACKAGE, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONObject jsonArray = response.getJSONObject("Packages_data");

                            Advance_package = jsonArray.getJSONObject("Advance");
                            Standard_package = jsonArray.getJSONObject("Standard");
                            Premium_package = jsonArray.getJSONObject("Premier");

                            SetViewPager();
                            standard.setEnabled(true);
                            advance.setEnabled(true);
                            premier.setEnabled(true);

                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    System.out.println(error.toString());
                    Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void SetViewPager() {
        package_viewpager_adapter = new Package_Viewpager(getChildFragmentManager());
        package_viewpager_adapter.AddFragment(new Package_Standard(Standard_package));
        package_viewpager_adapter.AddFragment(new Package_Advance(Advance_package));
        package_viewpager_adapter.AddFragment(new Package_Premier(Premium_package));

        packages_viewpager.disableScroll(true);
        packages_viewpager.setAdapter(package_viewpager_adapter);
        packages_viewpager.setOffscreenPageLimit(3);
        packages_viewpager.setCurrentItem(1);
        standard.setAlpha((float) 0.4);
        premier.setAlpha((float) 0.4);
        advance.setAlpha((float) 1);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        preferences = new Preferences(context);
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    class Package_Viewpager extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();

        public Package_Viewpager(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void AddFragment(Fragment fragment) {
            fragmentList.add(fragment);
        }
    }
}
