package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Morning_adapter;
import com.ondemandsalon.Adapter.User_service_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.Appointment_Confirm;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Categeory_Filter;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.model.Time;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomGridviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.ondemandsalon.helper.CONSTANTS.SERVICES;

public class Select_Appointment_Time extends Fragment {
    public static String Selemp = "0";
    RecyclerView user_service_recycler;
    ArrayList<Sub_cat> morlist, nooonlist, evelist;
    Context context;
    //    GridView morning;
    CustomGridviewRegular morning, afternoon, evening;
    RelativeLayout confirmrel, backRel, note;
    LinearLayout time_data;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    CalendarView calendarView;
    HomeNew h;
    int hour, minute;
    ArrayList<Time> timing;
    ArrayList<Sub_cat> employee_list;
    ArrayList<Categeory_Filter> filterlist;
    Preferences preferences;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    String from, to, from_min, to_min, from_ampm, to_ampm, Seldate, SelTime = "", salonid;


    public Select_Appointment_Time() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Select_Appointment_Time(HomeNew h, ArrayList<Time> timing, ArrayList<Sub_cat> employee_list, ArrayList<Categeory_Filter> filterlist, String salonid) {
        this.h = h;
        this.timing = timing;
        this.employee_list = employee_list;
        this.filterlist = filterlist;
        this.salonid = salonid;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.select_appointment_time, container, false);
        user_service_recycler = view.findViewById(R.id.user_service_recycler);
//        morning = view.findViewById(R.id.morning);
        morning = view.findViewById(R.id.morning);
        afternoon = view.findViewById(R.id.afternoon);
        evening = view.findViewById(R.id.evening);

        confirmrel = view.findViewById(R.id.confirmRel);
        backRel = view.findViewById(R.id.backRel);

        note = view.findViewById(R.id.noteh);
        time_data = view.findViewById(R.id.data_for_time);
        calendarView = view.findViewById(R.id.calender);

        calendarView.setMinDate(System.currentTimeMillis() - 1000);


        Seldate = sdf1.format(new Date());
        Calendar c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        Day_of_week(dayOfWeek);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

                Seldate = String.valueOf(year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth);
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, dayOfMonth);
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                Day_of_week(dayOfWeek);
            }
        });


        /*LayoutInflater inflater1 = LayoutInflater.from(context);
        View view1 = inflater1.inflate(R.layout.appointmentconfirmdialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view1);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();*/

        /*Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.thankyoudialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();*/

        confirmrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookAppointment();
//                context.startActivity(new Intent(context, Appointment_Confirm.class));
            }
        });

        backRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    ((HomeNew) context).getSupportFragmentManager().popBackStack();
                    HomeNew.HOME_TOOL = SERVICES;
                    HomeNew.toolTitle.setText(SERVICES);
                    return true;
                }
                return false;
            }
        });

        if (employee_list != null && employee_list.size() != 0) {
            user_service_recycler.setAdapter(new User_service_adapter(employee_list, getContext()));
            user_service_recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        }

        morning.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setTimeDifference(from, to, from_min, to_min, from_ampm, to_ampm, "Morning", position);
            }
        });

        afternoon.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setTimeDifference(from, to, from_min, to_min, from_ampm, to_ampm, "Noon", position);
            }
        });

        evening.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setTimeDifference(from, to, from_min, to_min, from_ampm, to_ampm, "Evening", position);
            }
        });

        return view;
    }

    private void Day_of_week(int dayOfWeek) {
        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                setTiming("SUNDAY");
                break;
            case Calendar.MONDAY:
                setTiming("MONDAY");
                break;
            case Calendar.TUESDAY:
                setTiming("TUESDAY");
                break;
            case Calendar.WEDNESDAY:
                setTiming("WEDNESDAY");
                break;
            case Calendar.THURSDAY:
                setTiming("THURSDAY");
                break;
            case Calendar.FRIDAY:
                setTiming("FRIDAY");
                break;
            case Calendar.SATURDAY:
                setTiming("SATURDAY");
                break;
        }
    }

    private void setTiming(String day) {
        if (timing != null && timing.size() != 0) {
            for (int i = 0; i < timing.size(); i++) {
                Time time = timing.get(i);
                if (time.getDay().toUpperCase().equals(day)) {
                    note.setVisibility(View.GONE);
                    time_data.setVisibility(View.VISIBLE);
                    String[] f = time.getFrom().split(" ");
                    String[] t = time.getTo().split(" ");
                    String[] frm = f[0].split(":");
                    String[] tom = t[0].split(":");

                    from = frm[0];
                    to = tom[0];
                    from_min = frm[1];
                    to_min = tom[1];
                    from_ampm = f[1];
                    to_ampm = t[1];
                    setTimeDifference(from, to, from_min, to_min, from_ampm, to_ampm, "All", 0);
//                    setTimeDifference("12","10","00","00","PM","PM");
                    break;
                } else {
                    note.setVisibility(View.VISIBLE);
                    time_data.setVisibility(View.GONE);
                }
            }
        }
    }

    private void setTimeDifference(String from, String to, String from_m, String to_m, String from_ampm, String to_ampm, String fr, int pos) {


        morning.setVisibility(View.VISIBLE);
        afternoon.setVisibility(View.VISIBLE);
        evening.setVisibility(View.VISIBLE);

        String cutTm = sdf.format(new Date());
        String[] ar = cutTm.split(" ");
        if (ar[0].equals(Seldate)) {
            String[] newAr = ar[1].split(":");
            String h = newAr[0];
            String m = String.valueOf(getMin(newAr[1]));
            if (ar[2].equals("PM")) {
                if ((Integer.parseInt(h) >= 4 && Integer.parseInt(newAr[1]) > 45) && Integer.parseInt(newAr[0]) < 12) {
                    morning.setVisibility(View.GONE);
                    afternoon.setVisibility(View.GONE);
                    if (Integer.parseInt(h) == 4)
                        h = "5";
                    setEvening(h, m, fr, pos);
                } else {
                    morning.setVisibility(View.GONE);
                    setNoon(h, m, fr, pos);
                    setEvening("5", "0", fr, pos);
                }
            } else {
                setMorning(h, m, fr, pos);
                setNoon("12", "0", fr, pos);
                setEvening("5", "0", fr, pos);
            }

        } else {
            setMorning(from, from_m, fr, pos);
            setNoon("12", "0", fr, pos);
            setEvening("5", "0", fr, pos);
        }


    }

    private int getMin(String s) {
        int m = Integer.parseInt(s);
        if (m == 0 || m > 45)
            m = 0;
        else if (m > 0 && m <= 15)
            m = 15;
        else if (m > 15 && m <= 30)
            m = 30;
        else if (m > 30 && m <= 45)
            m = 45;

        return m;
    }

    private void setEvening(String h, String min, String fr, int pos) {

        evelist = new ArrayList<>();
        hour = Integer.parseInt(h);
        minute = Integer.parseInt(min);

        for (int i = 0; i < i + 1; i++) {
            String st = "0";
            String g = String.format("%02d", hour) + ":" + String.format("%02d", minute);
            if (fr.equals("Evening") && pos == i) {
                st = "1";
                SelTime = g + " PM";
            }
            if (g.equals(to + ":" + to_min)) {
                evelist.add(new Sub_cat(g, st));
                break;
            } else
                evelist.add(new Sub_cat(g, st));
            minute = minute + 15;
            if (minute > 59) {
                minute = 0;
                hour = hour + 1;
            }


        }
        Morning_adapter morning_adapter2 = new Morning_adapter(context, evelist, "Evening");
        evening.setAdapter(morning_adapter2);
        evening.setExpanded(true);

    }

    private void setNoon(String h, String min, String fr, int pos) {

        nooonlist = new ArrayList<>();
        hour = Integer.parseInt(h);
        minute = Integer.parseInt(min);

        for (int i = 0; i < i + 1; i++) {
            String st = "0";
            String g = String.valueOf(hour) + ":" + String.format("%02d", minute);
            if (fr.equals("Noon") && pos == i) {
                st = "1";
                SelTime = g + " PM";
            }

            if (g.equals("4:45")) {
                nooonlist.add(new Sub_cat(g, st));
                break;
            } else
                nooonlist.add(new Sub_cat(g, st));
            minute = minute + 15;
            if (minute > 59) {
                minute = 0;
                if (hour == 12)
                    hour = 0;
                hour = hour + 1;
            }
        }


        Morning_adapter morning_adapter1 = new Morning_adapter(context, nooonlist, "Noon");
        afternoon.setAdapter(morning_adapter1);
        afternoon.setExpanded(true);

    }

    private void setMorning(String h, String min, String fr, int pos) {

        morlist = new ArrayList<>();
        hour = Integer.parseInt(h);
        minute = Integer.parseInt(min);
        for (int i = 0; i < i + 1; i++) {
            String g = String.valueOf(hour) + ":" + String.format("%02d", minute);
            String st = "0";
            if (fr.equals("Morning") && pos == i) {
                st = "1";
                SelTime = g + " AM";
            }

            if (g.equals("11:45")) {
                morlist.add(new Sub_cat(g, st));
                break;
            } else
                morlist.add(new Sub_cat(g, st));

            minute = minute + 15;
            if (minute > 59) {
                minute = 0;
                hour = hour + 1;
            }

        }
        Morning_adapter morning_adapter = new Morning_adapter(context, morlist, "Morning");
        morning.setAdapter(morning_adapter);
        morning.setExpanded(true);

    }

    private void bookAppointment() {

        if (SelTime.equals("")) {
            Toast.makeText(context, "Please select appointment time...", Toast.LENGTH_SHORT).show();
            return;
        }
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject main = new JSONObject();
            try {

                JSONArray category_data = new JSONArray();

                if (filterlist.size() != 0) {
                    for (Categeory_Filter filter : filterlist) {

                        JSONObject data = new JSONObject();

                        data.accumulate("category_id", filter.getSub_cat_parent_id());

                        ArrayList<Service_data> subserviceList = filter.getBooked_sub_categories();
                        if (subserviceList.size() != 0) {
                            JSONArray sub_categories = new JSONArray();
                            for (Service_data sdata : subserviceList) {
                                JSONObject sdatas = new JSONObject();
                                sdatas.accumulate("sub_cat_id", sdata.getSub_cat_id());
                                sub_categories.put(sdatas);
                            }
                            data.accumulate("sub_categories", sub_categories);
                        }
                        category_data.put(data);
                    }

                }


                main.accumulate("category_data", category_data);
                main.accumulate("date", Seldate);
                main.accumulate("time", SelTime);
                main.accumulate("service_pro_id", salonid);
                main.accumulate("user_id", preferences.getClientID());
                main.accumulate("emp_id", Selemp);

                System.out.println(main);

                json json = new json(API.BOOK_APPOINTMENT_USER, main, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        System.out.println(response);
                        try {
                            if (response.get("status").equals("1")) {
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                                context.startActivity(new Intent(context, Appointment_Confirm.class));
                            } else
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
        preferences = new Preferences(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
