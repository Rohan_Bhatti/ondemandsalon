package com.ondemandsalon.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.ondemandsalon.R;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextLight;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewLight;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class Provider_SignUp extends Fragment {

    GeoDataClient geoDataClient;
    PlaceDetectionClient placeDetectionClient;
    Context context;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    CustomTextviewLight businesslocationtxt;
    CustomEdittextRegular working_hour, business_email, password, confirm_password;
    CustomEdittextLight name_of_business, business_phone_num;
    RelativeLayout businesslocationrel, continue_btn;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;

    public Provider_SignUp() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.provider_sign_up, container, false);
        geoDataClient = Places.getGeoDataClient(context, null);
        placeDetectionClient = Places.getPlaceDetectionClient(context, null);

        businesslocationrel = view.findViewById(R.id.businesslocationrel);

        continue_btn = view.findViewById(R.id.signUpRel);
        name_of_business = view.findViewById(R.id.name_of_business);
        business_phone_num = view.findViewById(R.id.business_phone_number);
        working_hour = view.findViewById(R.id.working_hour);
        businesslocationtxt = view.findViewById(R.id.businesslocationtxt);
        business_email = view.findViewById(R.id.business_email);
        password = view.findViewById(R.id.password);
        confirm_password = view.findViewById(R.id.confirm_password);

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

        businesslocationrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(((Activity) context));
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(context, "Please try again...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    private void validate() {

        if (!validateEmail()) {
            return;
        }

        if (name_of_business.getText().toString().trim().isEmpty()) {
            name_of_business.setError("Please enter name of business");
            requestFocus(name_of_business);
        }

        if (businesslocationtxt.getText().toString().equals("Business Location")) {
            Toast.makeText(context, "Please select business location", Toast.LENGTH_SHORT).show();
            return;
        }

        String phone = business_phone_num.getText().toString().trim();
        if (phone.isEmpty() || phone.length() > 10 || phone.length() < 10) {
            business_phone_num.setError("Please enter valid phone number");
            requestFocus(business_phone_num);
            return;
        }

        if (working_hour.getText().toString().trim().isEmpty()) {
            working_hour.setError("Please enter working hours");
            requestFocus(working_hour);
            return;
        }

        if (password.getText().toString().trim().isEmpty()) {
            password.setError("Please enter password");
            requestFocus(password);
            return;
        }

        if (confirm_password.getText().toString().trim().isEmpty()) {
            confirm_password.setError("Please enter confirm password");
            requestFocus(confirm_password);
            return;
        }

        String pass = password.getText().toString().trim();
        String con_pass = confirm_password.getText().toString().trim();
        if (!pass.equals(con_pass)) {
            Toast.makeText(context, "Password and Confirm password must be same.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (connectionDetector.isNetworkAvailable()) {
            registration_service_provider();
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private boolean validateEmail() {
        String e = business_email.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email...", Toast.LENGTH_SHORT).show();
            requestFocus(business_email);
            return false;
        } else {
            if (!isValidEmail(e)) {
                Toast.makeText(context, "Please enter valid email...", Toast.LENGTH_SHORT).show();
                requestFocus(business_email);
                return false;
            }
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void registration_service_provider() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("business_name", "1");
                jsonObject.accumulate("user_phone", "1");
                jsonObject.accumulate("latitude", "1");
                jsonObject.accumulate("longitude", "1");
                jsonObject.accumulate("working_hours", "1");
                jsonObject.accumulate("user_email", "1");
                jsonObject.accumulate("user_password", "1");
                jsonObject.accumulate("role", "1");
                jsonObject.accumulate("status", "1");
                jsonObject.accumulate("user_token", "1");

                json json = new json(API.REGISTRATION, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(context, data);
                businesslocationtxt.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(context, data);
                Toast.makeText(context, status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }
}
