package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.activity.Opening_Hours;
import com.ondemandsalon.activity.Service_Category;
import com.ondemandsalon.model.Provider_Client;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.FontFamily;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;
import static com.ondemandsalon.helper.CONSTANTS.EMPLOYEE_LIST;

public class Add_employee extends Fragment {

    Context context;
    RelativeLayout select_category, select_day;
    FontFamily font;


    String frm;
    Provider_Client c;
    CustomEdittextRegular nm, lnm, phon, email, birth_date;
    CustomTextviewRegular sv, category, hours;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    ImageView profile_image;

    RelativeLayout saveRel, backrel;

    public Add_employee() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Add_employee(String frm, Provider_Client provider_client) {
        this.frm = frm;
        c = provider_client;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_employee, container, false);
        select_category = view.findViewById(R.id.select_Category);
        select_day = view.findViewById(R.id.select_hours);

        nm = view.findViewById(R.id.nm);
        lnm = view.findViewById(R.id.lnm);
        phon = view.findViewById(R.id.phonetxt);
//        streat = view.findViewById(R.id.streat);
        saveRel = view.findViewById(R.id.saveRel);
        backrel = view.findViewById(R.id.backRel);
        sv = view.findViewById(R.id.svtxt);
        category = view.findViewById(R.id.select_category_spinner);
        hours = view.findViewById(R.id.hours);
        email = view.findViewById(R.id.email_txt);
        birth_date = view.findViewById(R.id.birthdate);
        profile_image = view.findViewById(R.id.item_img);


        if (frm.equals("edit")) {
            String[] a = c.getName().split(" ");
            nm.setText(a[0]);
            lnm.setText(a[1]);
            phon.setText(c.getMobileno());
//            streat.setText(c.getLocation());
            sv.setText("Update");
        } else
            sv.setText("Save");

        select_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Service_Category.class).putExtra("frm", "back"));
            }
        });

        select_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Opening_Hours.class).putExtra("frm", "back"));
            }
        });

        saveRel = view.findViewById(R.id.saveRel);

        saveRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_1;
                HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_1);
                HomeNew.toolImg.setVisibility(View.VISIBLE);
//                validate();

            }
        });
        backrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();

                HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_1;
                HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_1);

                HomeNew.toolImg.setVisibility(View.VISIBLE);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();

                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_1;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_1);

                    HomeNew.toolImg.setVisibility(View.VISIBLE);
                    return true;
                }
                return false;
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        font = new FontFamily(context);
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void validate() {
        if (!validateEmail()) {
            return;
        }

        if (nm.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter valid name", Toast.LENGTH_SHORT).show();
            requestFocus(nm);
            return;
        }

        if (lnm.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter valid name", Toast.LENGTH_SHORT).show();
            requestFocus(lnm);
            return;
        }

        if (birth_date.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter birth of date", Toast.LENGTH_SHORT).show();
            requestFocus(birth_date);
            return;
        }

        if (category.getText().toString().equals("Select Category")) {
            Toast.makeText(context, "Please select category", Toast.LENGTH_SHORT).show();
            return;
        }

        if (hours.getText().toString().equals("Select Hours")) {
            Toast.makeText(context, "Please select hour", Toast.LENGTH_SHORT).show();
            return;
        }

        String phone = phon.getText().toString().trim();
        if (phone.isEmpty()) {
            Toast.makeText(context, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            requestFocus(phon);
            return;
        } else if (phone.length() < 10) {
            Toast.makeText(context, "Phone number must be atleast 10 digit", Toast.LENGTH_SHORT).show();
            requestFocus(phon);
            return;
        }

        if (connectionDetector.isNetworkAvailable())
            AddEmployee();
        else
            connectionDetector.error_Dialog();
    }

    private void AddEmployee() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("em_fname", nm.getText().toString().trim());
                jsonObject.accumulate("em_lname", lnm.getText().toString().trim());
                jsonObject.accumulate("em_phone", phon.getText().toString().trim());
                jsonObject.accumulate("em_email", email.getText().toString().trim());
                jsonObject.accumulate("em_birthday", birth_date.getText().toString().trim());
                jsonObject.accumulate("em_services", category.getText().toString());
                jsonObject.accumulate("em_time", hours.getText().toString());
                jsonObject.accumulate("em_image", profile_image.getDrawable());
                jsonObject.accumulate("cl_added_by", "1");

                json json = new json(API.ADD_EMPLOYEE, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void EditEmployee() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("em_fname", nm.getText().toString().trim());
                jsonObject.accumulate("em_lname", lnm.getText().toString().trim());
                jsonObject.accumulate("em_phone", phon.getText().toString().trim());
                jsonObject.accumulate("em_email", email.getText().toString().trim());
                jsonObject.accumulate("em_birthday", birth_date.getText().toString().trim());
                jsonObject.accumulate("em_services", category.getText().toString());
                jsonObject.accumulate("em_time", hours.getText().toString());
                jsonObject.accumulate("em_image", profile_image.getDrawable());
                jsonObject.accumulate("cl_added_by", "1");

                json json = new json(API.EDIT_EMPLOYEE, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void DeleteEmployee() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("em_id", "1");

                json json = new json(API.DELETE_EMPLOYEE, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private boolean validateEmail() {
        String e = email.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(email);
            return false;
        } else {
            if (!isValidEmail(e)) {
                Toast.makeText(context, "Please enter valid email", Toast.LENGTH_SHORT).show();
                requestFocus(email);
                return false;
            }
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
