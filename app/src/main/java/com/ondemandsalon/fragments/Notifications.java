package com.ondemandsalon.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Notifications_Adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Notifications_Data;
import com.ondemandsalon.model.User_Setting_Model;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Notifications extends Fragment {


    RecyclerView notRecycler;

    ProgDialog dialog;
    ConnectionDetector connectionDetector;
    Context c;
    Preferences preferences;

    ArrayList<Notifications_Data> list;

    public Notifications() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_notifications, container, false);


        notRecycler = v.findViewById(R.id.notRecycler);

        getNotifications();

        return v;
    }

    private void getNotifications() {

        if (connectionDetector.isNetworkAvailable()) {
            dialog.ProgressDialogStart();

            JSONObject obj = new JSONObject();
            try {
                obj.accumulate("user_id", preferences.getClientID());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            json j = new json(API.USER_NOTIFICATIONS, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    dialog.ProgressDialogStop();
                    System.out.println(response);

                    try {
                        if (response.getInt("status") == 1) {
                            JSONArray notification_detail = response.getJSONArray("notification_detail");

                            if (notification_detail.length() != 0) {

                                list = new ArrayList<>();

                                for (int i = 0; i < notification_detail.length(); i++) {
                                    JSONObject obj = notification_detail.getJSONObject(i);

                                    String notification_text = obj.getString("notification_text");
                                    String created_date = obj.getString("created_date");
                                    String not_id = obj.getString("not_id");
                                    String not_provider_id = obj.getString("not_provider_id");
                                    String business_name = obj.getString("business_name");
                                    String user_avatar = obj.getString("user_avatar");

                                    list.add(new Notifications_Data(notification_text, created_date, not_id, not_provider_id, business_name, user_avatar));
                                }

                                notRecycler.setAdapter(new Notifications_Adapter(c, list));

                                notRecycler.setVisibility(View.VISIBLE);
                                notRecycler.setLayoutManager(new LinearLayoutManager(c));

                            }
                        } else {

                            dialog.ProgressDialogStop();
                            notRecycler.setVisibility(View.GONE);
                            Toast.makeText(c, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    notRecycler.setVisibility(View.GONE);
                    dialog.ProgressDialogStop();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else
            connectionDetector.error_Dialog();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        c = context;
        connectionDetector = new ConnectionDetector(context);
        dialog = new ProgDialog(context);
        preferences = new Preferences(context);
    }
}
