package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.utils.CustomViewPager;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.ArrayList;
import java.util.List;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;
import static com.ondemandsalon.helper.CONSTANTS.APPOINTMENT;
import static com.ondemandsalon.helper.CONSTANTS.CLIENT;
import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;


public class Provider extends Fragment implements View.OnClickListener {

    public static String CLIENT_TEXT = CLIENT, APPOINTMENT_TEXT = APPOINTMENT, ACCOUNT_TEXT = ACCOUNT;
    Context context;
    CustomViewPager dashboardpager;
    LinearLayout dashLin, appointmentLin, clientLin, accLin;
    ImageView dashImg, appointmentImg, clientImg, accImg;
    CustomTextviewRegular dashTxt, appointmentTxt, clientTxt, accTxt;
    ViewPagerAdapter pagerAdapter;
    String role = "provider";
    Fragment client;
    FragmentTransaction ft;

    Preferences preferences;
    FragmentManager manager;

    public Provider() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Provider(FragmentManager supportFragmentManager) {
        manager = supportFragmentManager;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.provider, container, false);

        View v = inflater.inflate(R.layout.provider, container, false);

        dashboardpager = v.findViewById(R.id.dashboardpager);

        dashLin = v.findViewById(R.id.dashLin);
        appointmentLin = v.findViewById(R.id.appointmentLin);
        clientLin = v.findViewById(R.id.clientLin);
        accLin = v.findViewById(R.id.accLin);


        dashImg = v.findViewById(R.id.dashboardImg);

        appointmentImg = v.findViewById(R.id.appointmentImg);

        clientImg = v.findViewById(R.id.clientImg);

        accImg = v.findViewById(R.id.accountImg);


        dashTxt = v.findViewById(R.id.dashboardTxt);
        appointmentTxt = v.findViewById(R.id.appointmentTxt);
        clientTxt = v.findViewById(R.id.clientTxt);
        accTxt = v.findViewById(R.id.accontTxt);

        dashLin.setOnClickListener(this);
        appointmentLin.setOnClickListener(this);
        clientLin.setOnClickListener(this);
        accLin.setOnClickListener(this);

        setIconAndColor();
        changeIcAndColor(0);

        setViewPager();

        dashboardpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setIconAndColor();
                switch (position) {
                    case 0:
                        changeIcAndColor(0);
                        break;
                    case 1:
                        changeIcAndColor(1);
                        break;
                    case 2:
                        changeIcAndColor(2);
                        break;
                    case 3:
                        changeIcAndColor(3);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        preferences = new Preferences(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setIconAndColor() {

        dashImg.setImageResource(R.drawable.dashboard_ic_black);
        appointmentImg.setImageResource(R.drawable.appointments_ic_black);
        clientImg.setImageResource(R.drawable.clients_ic_black);
        accImg.setImageResource(R.drawable.accounts_ic_black);

        dashTxt.setTextColor(context.getResources().getColor(R.color.black));
        appointmentTxt.setTextColor(context.getResources().getColor(R.color.black));
        clientTxt.setTextColor(context.getResources().getColor(R.color.black));
        accTxt.setTextColor(context.getResources().getColor(R.color.black));

        dashTxt.setText("Dashboard");
        appointmentTxt.setText("Appointments");
        clientTxt.setText("Clients");
        accTxt.setText("Account");
    }

    private void setViewPager() {

        pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());

//        client = new Provider_Clients(this);

      /*  pagerAdapter.addFragment(new Provider_DashBoard(this), "");
        pagerAdapter.addFragment(new Provider_Appointments(this), "");
        pagerAdapter.addFragment(client, "");
        pagerAdapter.addFragment(new Provider_Account(this), "");
*/
        HomeScreen.setInstance(client);

        dashboardpager.disableScroll(true);
        dashboardpager.setAdapter(pagerAdapter);
        dashboardpager.setOffscreenPageLimit(4);
        dashboardpager.setCurrentItem(0);
        if (role.equals("user")) {
            dashImg.setImageResource(R.drawable.home_ic);
            dashTxt.setTextColor(context.getResources().getColor(R.color.blue));
            dashTxt.setText("User");
        } else {
            dashImg.setImageResource(R.drawable.dashboard_ic);
            dashTxt.setTextColor(context.getResources().getColor(R.color.blue));
            dashTxt.setText("Dashboard");
        }
    }


    @Override
    public void onClick(View view) {

        setIconAndColor();

        switch (view.getId()) {
            case R.id.dashLin:
                changeIcAndColor(0);
                break;
            case R.id.appointmentLin:
                changeIcAndColor(1);
                break;
            case R.id.clientLin:
                changeIcAndColor(2);
                break;
            case R.id.accLin:
                changeIcAndColor(3);
                break;
        }
    }

    private void changeIcAndColor(int i) {
        switch (i) {
            case 0:
                dashboardpager.setCurrentItem(0);
                dashImg.setImageResource(R.drawable.dashboard_ic);
                dashTxt.setTextColor(context.getResources().getColor(R.color.blue));
                HomeScreen.toolTitle.setText("Dashboard");
                HomeScreen.Tooltype = "";
                HomeScreen.tooImage.setVisibility(View.GONE);
                break;
            case 1:
                dashboardpager.setCurrentItem(1);
                appointmentImg.setImageResource(R.drawable.appointments_ic);
                appointmentTxt.setTextColor(context.getResources().getColor(R.color.blue));
                HomeScreen.toolTitle.setText(APPOINTMENT_TEXT);
                HomeScreen.tooImage.setImageResource(R.drawable.edit_white);
                HomeScreen.Tooltype = "";
                HomeScreen.tooImage.setVisibility(View.GONE);
                break;
            case 2:
                dashboardpager.setCurrentItem(2);
                clientImg.setImageResource(R.drawable.clients_ic);
                clientTxt.setTextColor(context.getResources().getColor(R.color.blue));
                HomeScreen.toolTitle.setText(CLIENT_TEXT);
                HomeScreen.tooImage.setImageResource(R.drawable.plus_white);
                HomeScreen.Tooltype = "Client";
                HomeScreen.tooImage.setVisibility(View.VISIBLE);
                HomeScreen.setInstance(client);
                break;
            case 3:
                dashboardpager.setCurrentItem(3);
                accImg.setImageResource(R.drawable.accounts_ic);
                accTxt.setTextColor(context.getResources().getColor(R.color.blue));
                HomeScreen.tooImage.setImageResource(R.drawable.plus_white);
                HomeScreen.toolTitle.setText(ACCOUNT_TEXT);
                HomeScreen.Tooltype = "";
                HomeScreen.tooImage.setVisibility(View.GONE);
                break;
        }
    }

 /*   public void changeFragment(Fragment f, int i, String toolTitle) {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();

        switch (i)
        {
            case 0:

                ft.replace(R.id.providerFrame, f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
                HomeScreen.toolTitle.setText(toolTitle);
                break;
            case 1:
                ft.replace(R.id.clientFrame, f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
                HomeScreen.toolTitle.setText(toolTitle);
                break;
            case 2:
                ft.replace(R.id.accountFrame, f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
                HomeScreen.toolTitle.setText(toolTitle);
                break;
        }

    *//*    if (f instanceof Service_Details)
            HomeScreen.setInstance(f);
        if (i == 0) {

        } else if (i == 1)
            dashboardpager.setCurrentItem(1);
        else {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.homeFrame, f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);
            ft.commit();
            HomeScreen.toolTitle.setText(toolTitle);
        }*//*
    }*/


    public void changeFragment(Fragment f, int i, String toolTitle) {

        ft = getFragmentManager().beginTransaction();

        switch (i) {
            case 0:
                ft.replace(R.id.dashFrame, f);
                break;
            case 1:
                ft.replace(R.id.appointmentFrame, f);
                APPOINTMENT_TEXT = toolTitle;
                break;
            case 2:
                ft.replace(R.id.clientFrame, f);
                CLIENT_TEXT = toolTitle;
                break;
            case 3:
                ft.replace(R.id.accountFrame, f);
                ACCOUNT_TEXT = toolTitle;
                break;
        }

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
        HomeScreen.toolTitle.setText(toolTitle);
    }

    public void logout() {

        if (!preferences.getUserLogin())
            preferences.setLogin(false);

        preferences.logoutProvider();
        if (preferences.getLogin()) {
            if (preferences.getProviderLogin())
                HomeScreen.loginTxt.setText("Business Profile");
            else
                HomeScreen.loginTxt.setText("Sell Your Services");
        } else
            HomeScreen.loginTxt.setText("Sell Your Services");

        HomeScreen.toolTitle.setText(YOUR_LOCATION);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, new User());
        fragmentTransaction.commitAllowingStateLoss();
    }

    /*public void goToSecondFragment() {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment secondFragment = new Search_All_Services();
        ft.replace(R.id.frame, secondFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }*/

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
