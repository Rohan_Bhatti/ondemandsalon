package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ondemandsalon.Adapter.Client_Adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.model.Provider_Client;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Provider_Clients extends Fragment implements HomeNew.ChangeVal {

    RecyclerView clientRecycler;
    Context context;
    ArrayList<Provider_Client> Provider_Clients;
    HomeNew p;

    public Provider_Clients() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public Provider_Clients(HomeNew provider) {
        p = provider;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_client, container, false);
        Provider_Clients = new ArrayList<>();
        clientRecycler = v.findViewById(R.id.clientRecycler);
        Provider_Clients.add(new Provider_Client("Laura Michelle", "4581889631", "Jabel Ali"));
        Provider_Clients.add(new Provider_Client("Jason Derulo", "4581889631", "Delhi"));
        Provider_Clients.add(new Provider_Client("Smith Ken", "4581889631", "Jabel ali"));
        Provider_Clients.add(new Provider_Client("Kevin Pet", "4581889631", "Goa"));
        Provider_Clients.add(new Provider_Client("Kit Harington", "4581889631", "Dubai"));

        clientRecycler.setAdapter(new Client_Adapter(context, Provider_Clients, "Client", p));
        clientRecycler.setLayoutManager(new LinearLayoutManager(context));

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public void change(int pos,String fro) {
        p.changeFragment(new Add_clients(context, "new", Provider_Clients.get(0)), 2, "New Client","New_Client","provider");
    }

}
