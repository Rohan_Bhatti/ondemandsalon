package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ondemandsalon.Adapter.Ratings_Reviews_Adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Ratings_Data;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;
import static com.ondemandsalon.helper.CONSTANTS.PROFILE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Ratings_reviews extends Fragment {

    RecyclerView ratingRecycler;
    Context context;
    ArrayList<Ratings_Data> ratingsList;
    CustomTextviewRegular note;
    HomeNew homeNew;
    String from;
    Preferences preferences;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;

    public Ratings_reviews() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Ratings_reviews(HomeNew homeNew,String frm) {
        this.homeNew = homeNew;
        from = frm;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_user_ratings_reviews, container, false);

        ratingRecycler = v.findViewById(R.id.ratingsRecycler);
        note = v.findViewById(R.id.note);

        if (from.equals("user")) {
            get_Rating_and_Reviews();
        } else {
            String cmt = "This is a very nice salon. Good Service.";
            ratingsList = new ArrayList<>();
            ratingsList.add(new Ratings_Data("Smith even", "02/08/2018", "4.3", cmt));
            ratingsList.add(new Ratings_Data("Zara larreson", "09/08/2018", "4.0", cmt + " Once visit."));
            ratingsList.add(new Ratings_Data("Brayn Smith", "15/07/2018", "3.0", cmt + " Professionalise service."));
            ratingsList.add(new Ratings_Data("James Doe", "12/05/2018", "3.5", cmt));

            ratingRecycler.setAdapter(new Ratings_Reviews_Adapter(context, ratingsList, homeNew,from));
            ratingRecycler.setLayoutManager(new LinearLayoutManager(context));
        }


        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    if (from.equals("user")) {
                        HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_0;
                        HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_0);
                    } else {
                        HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                        HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                    }
                    return true;
                }
                return false;
            }
        });

        return v;
    }

    private void get_Rating_and_Reviews() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject params = new JSONObject();
            try {
                params.accumulate("user_id", preferences.getClientID());
                params.accumulate("rat_role", "2");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            json j = new json(API.LIST_OF_RATING_AND_REVIEWS, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONArray list_of_reviews = response.getJSONArray("review_list");
                            note.setVisibility(View.VISIBLE);
                            ratingRecycler.setVisibility(View.GONE);
                            if (list_of_reviews.length() == 0)
                                return;
                            ratingRecycler.setVisibility(View.VISIBLE);
                            note.setVisibility(View.GONE);
                            ratingsList = new ArrayList<>();
                            for (int i = 0; i < list_of_reviews.length(); i++) {
                                JSONObject jsonObject = (JSONObject) list_of_reviews.get(i);
                                String salon_name = jsonObject.getString("business_name");
                                String rating_date = jsonObject.getString("rat_created_time");
                                String average_rating = jsonObject.getString("rat_out_of_5");
                                String rating_description = jsonObject.getString("rat_discription");
                                String rat_time = jsonObject.getString("rat_time");
                                String rat_quality = jsonObject.getString("rat_quality");
                                String rat_safety = jsonObject.getString("rat_safety");
                                String rat_result = jsonObject.getString("rat_result");
                                String rat_proffesnalism = jsonObject.getString("rat_proffesnalism");
                                String rat_rewarding = jsonObject.getString("rat_rewarding");

                                String rating_new_date = "";
                                try {
                                    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");
                                    Date dateObj = input.parse(rating_date);
                                    rating_new_date = output.format(dateObj);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                ratingsList.add(new Ratings_Data(salon_name, rating_new_date, average_rating, rating_description, rat_time, rat_quality, rat_safety, rat_result, rat_proffesnalism, rat_rewarding));
                            }
                            ratingRecycler.setAdapter(new Ratings_Reviews_Adapter(context, ratingsList, homeNew,from));
                            ratingRecycler.setLayoutManager(new LinearLayoutManager(context));
                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            note.setVisibility(View.VISIBLE);
                            ratingRecycler.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    System.out.println(error.toString());
                    Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
        preferences = new Preferences(context);
    }

    private void get_list_of_reviews_ratings() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("user_id", "1");
                jsonObject.accumulate("role", "1");

                json json = new json(API.LIST_OF_ALL_REVIEWS, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }
}
