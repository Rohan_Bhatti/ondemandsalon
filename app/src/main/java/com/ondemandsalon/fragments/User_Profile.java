package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.activity.SignUp_User;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.utils.UtilDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@SuppressLint("ValidFragment")
public class User_Profile extends Fragment {

    CustomTextviewRegular nameTxt, mailTxt, numberTxt, settings, user_type, premier_type;

    RelativeLayout reviews, account_details, settingRel, logoutRel,notRel;
    LinearLayout editprof, changepass;
    HomeNew u;
    Context context;
    UtilDialog dialog;
    View view;
    ImageView profile_image;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;
    String urlofprofile = "";
    String fnanme = "";
    String lnanme = "";

    @SuppressLint("ValidFragment")
    public User_Profile(HomeNew user) {
        // Required empty public constructor
        this.u = user;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.user_profile, container, false);
        account_details = v.findViewById(R.id.accountdetailRel);
        settingRel = v.findViewById(R.id.settingLin);
        settings = v.findViewById(R.id.settingsspinner);
        reviews = v.findViewById(R.id.reviewsRel);
        logoutRel = v.findViewById(R.id.logoutRel);
        profile_image = v.findViewById(R.id.item_img);

        nameTxt = v.findViewById(R.id.unameTxt);
        mailTxt = v.findViewById(R.id.emailTxt);
        numberTxt = v.findViewById(R.id.numberTxt);
        user_type = v.findViewById(R.id.user_type);
        premier_type = v.findViewById(R.id.premier_type);
        notRel = v.findViewById(R.id.notRel);

        getProfileData();

        reviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u.changeFragment(new Ratings_reviews(u,"user"), 3, "Ratings & Reviews", "RAT_REVIEW","user");
                HomeNew.PROFILE_LEVEL_1 = "Ratings & Reviews";
            }
        });

        settingRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u.changeFragment(new Settings(), 3, "Settings", "SETTINGS","user");
                HomeNew.PROFILE_LEVEL_1 = "Settings";
            }
        });

        notRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u.changeFragment(new Notifications(), 3, "Notifications", "Notifications","user");
                HomeNew.PROFILE_LEVEL_1 = "Notifications";
            }
        });

        account_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (view != null) {
                    ViewGroup parent = (ViewGroup) view.getParent();
                    if (parent != null) {
                        parent.removeView(view);
                    }
                }

                view = LayoutInflater.from(context).inflate(R.layout.bottom_layout_dialog, null);

                editprof = view.findViewById(R.id.editprofLin);
                changepass = view.findViewById(R.id.changpassLin);

                editprof.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismissBottomDialog();
                        u.changeFragment(new User_Edit_Profile(fnanme, lnanme, mailTxt.getText().toString(), numberTxt.getText().toString(), urlofprofile), 3, "Edit Profile", "E_PROFILE","user");
                        HomeNew.PROFILE_LEVEL_1 = "Edit Profile";
                    }
                });

                changepass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismissBottomDialog();
                        u.changeFragment(new Change_Password("FROM USER"), 3, "ChangeVal Password", "C_PASSWORD","user");
                        HomeNew.PROFILE_LEVEL_1 = "ChangeVal Password";
                    }
                });

                dialog.showBottomDialog(view);
            }
        });

        logoutRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u.logout("user");
            }
        });

       /* user_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
                ((TextView) adapterView.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        subscription.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
                ((TextView) adapterView.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        account_details.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        settings.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        reviews.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        return v;
    }

    private void getProfileData() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject params = new JSONObject();
            try {
                params.accumulate("id", preferences.getClientID());
                params.accumulate("role", "2");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            json j = new json(API.PROFILE, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONObject user_details = response.getJSONObject("user_details");
                            String name = user_details.getString("user_fname") + " " + user_details.getString("user_lname");
                            String email = user_details.getString("user_email");
                            String phone = user_details.getString("user_phone");
                            String profile_ = user_details.getString("user_avatar");

                            fnanme = user_details.getString("user_fname");
                            lnanme = user_details.getString("user_lname");

                            if (user_details.getString("subscription").equals("1")) {
                                premier_type.setText("Free");
                            }

                            if (user_details.getString("user_type").equals("0")) {
                                user_type.setText("Basic user");
                            }

                            nameTxt.setText(name);
                            mailTxt.setText(email);
                            numberTxt.setText(phone);
                            if (!profile_.equals("")) {
                                urlofprofile = profile_;
                                Glide.with(context).load(profile_).into(profile_image);
                            }
                        } else {
//                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    System.out.println(error.toString());
//                    Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        dialog = new UtilDialog(context);
        preferences = new Preferences(context);
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        /*if (isVisibleToUser) {
            nameTxt.setText(preferences.getName());
            mailTxt.setText(preferences.getMail());
            numberTxt.setText(preferences.getNumber());
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
