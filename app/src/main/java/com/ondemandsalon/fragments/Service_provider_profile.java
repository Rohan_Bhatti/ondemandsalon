package com.ondemandsalon.fragments;

import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Uploaded_images_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextLight;
import com.ondemandsalon.utils.CustomGridviewRegular;
import com.ondemandsalon.utils.CustomTextviewLight;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.FontFamily;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;

public class Service_provider_profile extends Fragment implements AdapterView.OnItemSelectedListener {
    CustomGridviewRegular uploaded_images;
    Context context;
    FontFamily font;
    RelativeLayout saveRel, backrel;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    ImageView profile;

    CustomEdittextLight business_name, business_phone_number, working_hours, business_email;
    CustomTextviewLight business_location;


    public Service_provider_profile() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_provider_profile, container, false);
        uploaded_images = view.findViewById(R.id.profile_images);
        profile = view.findViewById(R.id.profile_image);
        business_email = view.findViewById(R.id.business_email);
        business_location = view.findViewById(R.id.businesslocationtxt);
        business_name = view.findViewById(R.id.business_name);
        business_phone_number = view.findViewById(R.id.business_phone_number);
        working_hours = view.findViewById(R.id.working_hour);

        uploaded_images.setAdapter(new Uploaded_images_adapter(context));
        uploaded_images.setExpanded(true);

        getProfileDetails();

        saveRel = view.findViewById(R.id.saveRel);

        saveRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
                getFragmentManager().popBackStack();
                HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
            }
        });
        backrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private void validate() {
        if (!validateEmail()) {
            return;
        }

        if (business_name.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter name of business", Toast.LENGTH_SHORT).show();
            requestFocus(business_name);
            return;
        }

        if (business_location.getText().toString().equals("Business Location")) {
            Toast.makeText(context, "Please select business location", Toast.LENGTH_SHORT).show();
            requestFocus(business_location);
            return;
        }

        if (working_hours.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter working hours", Toast.LENGTH_SHORT).show();
            requestFocus(working_hours);
            return;
        }

        if (business_email.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter business email", Toast.LENGTH_SHORT).show();
            requestFocus(business_email);
            return;
        }

        String phone = business_phone_number.getText().toString().trim();
        if (phone.isEmpty() || phone.length() > 10 || phone.length() < 10) {
            Toast.makeText(context, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            requestFocus(business_phone_number);
            return;
        }

        if (connectionDetector.isNetworkAvailable())
            update_profile();
        else
            connectionDetector.error_Dialog();


    }

    private void update_profile() {
        JSONObject object = new JSONObject();
        progDialog.ProgressDialogStart();
        try {
            object.accumulate("id", "2");
            object.accumulate("user_email", business_email.getText().toString().trim());
            object.accumulate("user_fname", business_name.getText().toString().trim());
            object.accumulate("user_lname", business_phone_number.getText().toString().trim());
            object.accumulate("user_phone", business_location.getText().toString());
            object.accumulate("user_phone", working_hours.getText().toString());
            object.accumulate("img", profile.getDrawable());
            object.accumulate("role", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        json json = new json(API.UPDATE_PROFILE, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
            }
        });
        AppController.getInstance().addToRequestQueue(json);
    }

    private boolean validateEmail() {
        String e = business_email.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(business_email);
            return false;
        } else {
            if (!isValidEmail(e)) {
                Toast.makeText(context, "Please enter valid email", Toast.LENGTH_SHORT).show();
                requestFocus(business_email);
                return false;
            }
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void getProfileDetails() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject params = new JSONObject();
            try {
                params.accumulate("id", "2");
                params.accumulate("role", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            json j = new json(API.PROFILE, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {

                            String business_name = response.getString("business_name");
                            String business_phone_num = response.getString("business_name");
                            String business_location = response.getString("business_name");
                            String working_hours = response.getString("business_name");
                            String business_email_address = response.getString("business_name");
                            JSONArray images = response.getJSONArray("images");
                            ArrayList<String> img_list;
                            if (images.length() != 0) {
                                img_list = new ArrayList<>();
                                for (int i = 0; i < images.length(); i++) {
                                    img_list.add((String) images.get(i));
                                }
                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    System.out.println(error.toString());
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        font = new FontFamily(context);
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
        ((TextView) adapterView.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        ((TextView) adapterView.getChildAt(0)).setTypeface(font.titillium_regular);
        ((TextView) adapterView.getChildAt(0)).setEllipsize(TextUtils.TruncateAt.END);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
