package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.CategoryAdapter;
import com.ondemandsalon.Adapter.EventAdapter;
import com.ondemandsalon.Adapter.Review_Adapter;
import com.ondemandsalon.Adapter.SlidingImage_Adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.ForgotPasswordActivity;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.OnResponseListener;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Categeory_Filter;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.model.Time;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewBold;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;
import com.ondemandsalon.utils.LoginDialog;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.ondemandsalon.fragments.User.disableLogin;
import static com.ondemandsalon.helper.CONSTANTS.ALL_SERVICES;

@SuppressLint("ValidFragment")
public class Service_Details extends Fragment implements HomeScreen.changVal, CategoryAdapter.filter_data, EventAdapter.book_service_cart,HomeNew.ChangeVal{

    RecyclerView categoryRecycler, eventRecycler, reviewList;

    Context context;
    ArrayList<Sub_cat> nameList, categoryList, employee_list;

    RelativeLayout services_btn, info_btn, btnRatetheSalon, saveratingbtn;
    LinearLayout ll_shopeinfo, ll_details, ll_service, ll_info, ll_ratesalon;

    CustomTextviewSemiBold btnServices, btnInfo;
    CustomTextviewBold proccedBtn;
    CustomTextviewRegular full_address_of_salon, salon_time, book_services_list, total_payment, nm, nm1, nm2, nm3;
    CustomEdittextRegular rating_comment;

    ArrayList<String> images;
    ViewPager viewPager;
    ImageView backBtn;
    Preferences preferences;
    ArrayList<Service_data> eventList, tempList;

    RatingBar ratingBar, rat_time, rat_service, rat_safety, rat_result, rat_professionalism, rat_rewarding;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    String salon_id, salonid, serNames;
    ArrayList<String> serNmsArrray;
    int totalPay = 0;

    Dialog dialog;
    //    gotoNext n;
    CardView book_ley;
    ArrayList<Time> timing;
    View v;
    HomeNew h;
    String frm;
    ArrayList<Categeory_Filter> booked_main_category_list;
    ArrayList<Service_data> booked_sub_categories;
    JSONArray category_data = new JSONArray();

    LoginDialog loginDialog;


    @SuppressLint("ValidFragment")
    public Service_Details(HomeNew h, String salon_id, String usefor) {
        this.h = h;
        this.salon_id = salon_id;
        this.frm = usefor;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.service_details, container, false);
        categoryRecycler = v.findViewById(R.id.categoryRecycle);
        eventRecycler = v.findViewById(R.id.events);
        reviewList = v.findViewById(R.id.reviewlist);
        services_btn = v.findViewById(R.id.services_btn);
        info_btn = v.findViewById(R.id.info_btn);
        proccedBtn = v.findViewById(R.id.proccedBtn);
        btnRatetheSalon = v.findViewById(R.id.btnRatetheSalon);
        backBtn = v.findViewById(R.id.backBtn);
        viewPager = v.findViewById(R.id.intro_images_viewpager);
        ratingBar = v.findViewById(R.id.ratingBar);
        full_address_of_salon = v.findViewById(R.id.full_address_of_salon);
        salon_time = v.findViewById(R.id.salon_time);
        saveratingbtn = v.findViewById(R.id.saverating);

        book_services_list = v.findViewById(R.id.book_services_list);
        book_ley = v.findViewById(R.id.bookley);
        total_payment = v.findViewById(R.id.total_payment);

        rat_time = v.findViewById(R.id.rat_time);
        rat_result = v.findViewById(R.id.rat_result);
        rat_professionalism = v.findViewById(R.id.rat_professionalism);
        rat_rewarding = v.findViewById(R.id.rat_rewarding);
        rat_safety = v.findViewById(R.id.rat_safety);
        rat_service = v.findViewById(R.id.rat_service);
        rating_comment = v.findViewById(R.id.comment);

        ratingBar.setEnabled(false);

        nm = v.findViewById(R.id.nm);
        nm1 = v.findViewById(R.id.nm1);
        nm2 = v.findViewById(R.id.nm3);
        nm3 = v.findViewById(R.id.nm4);

        ll_shopeinfo = v.findViewById(R.id.ll_shopeinfo);
        ll_details = v.findViewById(R.id.ll_details);
        ll_service = v.findViewById(R.id.ll_service);
        ll_info = v.findViewById(R.id.ll_info);
        ll_ratesalon = v.findViewById(R.id.ll_ratesalon);
        btnServices = v.findViewById(R.id.btnServices);
        btnInfo = v.findViewById(R.id.btnInfo);

        booked_main_category_list = new ArrayList<>();

        getServiceProviderDetails(salon_id);

        services_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_service.setVisibility(View.VISIBLE);
                ll_info.setVisibility(View.GONE);
                btnServices.setTextColor(getResources().getColor(R.color.white));
                btnServices.setBackgroundColor(getResources().getColor(R.color.blue));
                btnInfo.setTextColor(getResources().getColor(R.color.blue));
                btnInfo.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                HomeNew.toolTitle.setText("Services");
            }
        });

        info_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_service.setVisibility(View.GONE);
                ll_info.setVisibility(View.VISIBLE);
                btnServices.setTextColor(getResources().getColor(R.color.blue));
                btnServices.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btnInfo.setTextColor(getResources().getColor(R.color.white));
                btnInfo.setBackgroundColor(getResources().getColor(R.color.blue));
                HomeNew.toolTitle.setText("Salon Information");
            }
        });

        saveratingbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit_rating();
            }
        });

        btnRatetheSalon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getUserLogin()) {
                    if (nameList != null && nameList.size() != 0) {
                        Boolean match = false;
                        if (!match) {
                            for (int i = 0; i < nameList.size(); i++) {
                                Sub_cat sub_cat = nameList.get(i);
                                if (sub_cat.getUser_id().equals(preferences.getClientID())) {
                                    match = true;
                                }
                            }
                        }

                        if (match) {
                            Toast.makeText(context, "You have already added your review and rating for this salon", Toast.LENGTH_SHORT).show();
                        } else {
                            ll_details.setVisibility(View.GONE);
                            ll_ratesalon.setVisibility(View.VISIBLE);
                            ll_shopeinfo.setVisibility(View.GONE);
                            HomeNew.toolTitle.setText(nm.getText().toString());
                        }
                    }
                } else {
                    loginDialog.openLogin("user", new OnResponseListener() {
                        @Override
                        public void onResponce(int res) {
                            switch (res) {
                                case 1:
                                    h.disableLogin("User NewLogin");
                                    break;
                                case 3:
                                    h.gotoN(-1,"rate");
                                    break;
                            }
                        }
                    });
//                    openDialog();
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_details.setVisibility(View.VISIBLE);
                ll_ratesalon.setVisibility(View.GONE);
                ll_shopeinfo.setVisibility(View.VISIBLE);
                ll_service.setVisibility(View.GONE);
                ll_info.setVisibility(View.VISIBLE);
                btnServices.setTextColor(getResources().getColor(R.color.blue));
                btnServices.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btnInfo.setTextColor(getResources().getColor(R.color.white));
                btnInfo.setBackgroundColor(getResources().getColor(R.color.blue));
                HomeNew.toolTitle.setText("Salon Information");
            }
        });

        proccedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                h.changeFragment(new Select_Appointment_Time(h, timing, employee_list, booked_main_category_list, salon_id), 0, "Book Services", "BOOK_SERVICE", "user");
            /*    h.setHomeDisable();
                h.inside = true;*/
            }
        });

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    ((HomeNew)context).getSupportFragmentManager().popBackStack();
                    HomeNew.HOME_TOOL = ALL_SERVICES;
                    HomeNew.toolTitle.setText(ALL_SERVICES);
                    return true;
                }
                return false;
            }
        });

        /*eventRecycler.setAdapter(new EventAdapter(categoryList));
        eventRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
      */
        return v;
    }

    private void submit_rating() {
        if (connectionDetector.isNetworkAvailable()) {

            String comment = rating_comment.getText().toString();
            String rattime = String.valueOf(rat_time.getRating());
            String ratresult = String.valueOf(rat_result.getRating());
            String ratprofessionalism = String.valueOf(rat_professionalism.getRating());
            String ratrewarding = String.valueOf(rat_rewarding.getRating());
            String ratsafety = String.valueOf(rat_safety.getRating());
            String ratservice = String.valueOf(rat_service.getRating());

            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("rat_time", rattime);
                jsonObject.accumulate("rat_quality", ratservice);
                jsonObject.accumulate("rat_safety", ratsafety);
                jsonObject.accumulate("rat_result", ratresult);
                jsonObject.accumulate("rat_rewarding", ratrewarding);
                jsonObject.accumulate("rat_proffesnalism", ratprofessionalism);
                jsonObject.accumulate("rat_discription", comment);
                jsonObject.accumulate("rat_give_by_user", preferences.getClientID());
                jsonObject.accumulate("rat_service_pro_id", salon_id);
                jsonObject.accumulate("rat_role", "2");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            json json = new json(API.ADD_RATING_AND_REVIEW, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            Toast.makeText(context, "Review and rating added successfully", Toast.LENGTH_SHORT).show();
                            ll_details.setVisibility(View.VISIBLE);
                            ll_ratesalon.setVisibility(View.GONE);
                            ll_shopeinfo.setVisibility(View.VISIBLE);
                            ll_service.setVisibility(View.GONE);
                            ll_info.setVisibility(View.VISIBLE);
                            btnServices.setTextColor(getResources().getColor(R.color.blue));
                            btnServices.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                            btnInfo.setTextColor(getResources().getColor(R.color.white));
                            btnInfo.setBackgroundColor(getResources().getColor(R.color.blue));
                            HomeScreen.toolTitle.setText("Salon Information");


                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    Toast.makeText(context, "Something wents wrong", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(json);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void getServiceProviderDetails(String salon_id) {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("shop_id", salon_id);

                json json = new json(API.SHOP_DETAILS, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.getString("status").equals("1")) {
                                JSONArray salon_images = response.getJSONArray("banner_images");
                                if (salon_images.length() != 0) {
                                    images = new ArrayList<>();
                                    for (int i = 0; i < salon_images.length(); i++) {
                                        JSONObject jsonObject = (JSONObject) salon_images.get(i);
                                        String imageurl = jsonObject.getString("banner_name");
                                        images.add(imageurl);
                                    }
                                    setUPSalonBanner();
                                }

                                JSONObject jsonObject1 = response.getJSONObject("service_provider_details");
                                String salon_id = jsonObject1.getString("id");
                                salonid = salon_id;
                                String salon_name = jsonObject1.getString("business_name");
                                String salon_location = jsonObject1.getString("user_location");
                                String salon_city = jsonObject1.getString("city");
                                String salon_rating = jsonObject1.getString("rat_out_of_5");
                                String salon_happy_customers = jsonObject1.getString("happy_customers");
                                String salon_total_reviews = jsonObject1.getString("total_review");
                                String salon_contact_no = jsonObject1.getString("user_phone");
                                String salon_address = jsonObject1.getString("business_full_address");

                                nm.setText(salon_name);
                                nm1.setText(salon_location);
                                nm2.setText(salon_total_reviews + " Reviews");
                                nm3.setText(salon_happy_customers + " % Happy customers");
                                ratingBar.setRating(Float.parseFloat(salon_rating));

                                JSONArray salon_categories = response.getJSONArray("category_details");
                                if (salon_categories.length() != 0) {
                                    categoryList = new ArrayList<>();
                                    for (int i = 0; i < salon_categories.length(); i++) {
                                        JSONObject jsonObject2 = (JSONObject) salon_categories.get(i);
                                        String cat_id = jsonObject2.getString("ser_id");
                                        String cat_name = jsonObject2.getString("ser_name");
                                        categoryList.add(new Sub_cat(cat_name, cat_id));
                                    }
                                    categoryRecycler.setAdapter(new CategoryAdapter(context, categoryList, Service_Details.this));
                                    categoryRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                }

                                JSONArray salon_sub_categories_of_categories = response.getJSONArray("category_services_details");
                                if (salon_sub_categories_of_categories.length() != 0) {
                                    eventList = new ArrayList<Service_data>();
                                    tempList = new ArrayList<>();
                                    for (int i = 0; i < salon_sub_categories_of_categories.length(); i++) {
                                        JSONObject jsonObject2 = (JSONObject) salon_sub_categories_of_categories.get(i);
                                        String sub_cat_id = jsonObject2.getString("ser_cs_id");
                                        String sub_cat_name = jsonObject2.getString("ser_cs_name");
                                        String sub_cat_parent_id = jsonObject2.getString("ser_cs_parent");
                                        String sub_cat_price = jsonObject2.getString("ser_cs_price");
                                        String sub_cat_time = jsonObject2.getString("ser_cs_time");
                                        String sub_cat_image = jsonObject2.getString("ser_cat_image");

                                        if (categoryList.get(0).getSub_cat_id().equals(sub_cat_parent_id)) {
                                            tempList.add(new Service_data(sub_cat_name, 0, sub_cat_price, sub_cat_image, sub_cat_time, sub_cat_parent_id, sub_cat_id, salon_contact_no));
                                        }

                                        eventList.add(new Service_data(sub_cat_name, 0, sub_cat_price, sub_cat_image, sub_cat_time, sub_cat_parent_id, sub_cat_id, salon_contact_no));
                                    }

                                    eventRecycler.setAdapter(new EventAdapter(tempList, context, Service_Details.this,h));
                                    eventRecycler.setLayoutManager(new LinearLayoutManager(context));
                                }

                                //info tab
                                full_address_of_salon.setText(salon_address);
                                JSONArray salon_timing = response.getJSONArray("time_info");
                                if (salon_timing.length() != 0) {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    timing = new ArrayList<>();
                                    for (int i = 0; i < salon_timing.length(); i++) {
                                        JSONObject jsonObject2 = (JSONObject) salon_timing.get(i);
                                        String day = jsonObject2.getString("oh_day");
                                        String from = jsonObject2.getString("oh_from_time");
                                        String to = jsonObject2.getString("oh_to_time");

                                        String time = day + " " + from + " - " + to + "\n";
                                        timing.add(new Time(day, from, to));
                                        stringBuilder.append(time);
                                    }
                                    salon_time.setText(stringBuilder);
                                }

                                JSONArray salon_reviews = response.getJSONArray("rating_lists");
                                if (salon_reviews.length() != 0) {
                                    nameList = new ArrayList<>();
                                    for (int i = 0; i < salon_reviews.length(); i++) {
                                        JSONObject jsonObject2 = (JSONObject) salon_reviews.get(i);
                                        String user_name = jsonObject2.getString("user_fname") + " " + jsonObject2.getString("user_lname");
                                        String user_rating = jsonObject2.getString("rat_out_of_5");
                                        String user_description = jsonObject2.getString("rat_discription");
                                        String r_time = jsonObject2.getString("rat_created_time");
                                        String user_id = jsonObject2.getString("id");
                                        String user_profile_image = jsonObject2.getString("user_avatar");
                                        String rating_time = "";
                                        try {
                                            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");
                                            Date dateObj = input.parse(r_time);
                                            rating_time = output.format(dateObj);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        nameList.add(new Sub_cat(user_name, user_rating, user_description, user_profile_image, rating_time, user_id));
                                    }
                                    reviewList.setAdapter(new Review_Adapter(nameList, context));
                                    reviewList.setLayoutManager(new LinearLayoutManager(context));
                                }

                                //employee list
                                JSONArray emp_list = response.getJSONArray("employee_list");
                                if (emp_list.length() != 0) {
                                    employee_list = new ArrayList<>();
                                    for (int i = 0; i < emp_list.length(); i++) {
                                        JSONObject jsonObject2 = (JSONObject) emp_list.get(i);
                                        String emp_id = jsonObject2.getString("em_id");
                                        String emp_fname = jsonObject2.getString("em_fname");
                                        String emp_lname = jsonObject2.getString("em_lname");
                                        String emp_image = jsonObject2.getString("em_image");

                                        employee_list.add(new Sub_cat(emp_id, emp_fname, emp_lname, emp_image));
                                    }
                                }


                            } else {
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void setUPSalonBanner() {
        final LinearLayout linearLayout = v.findViewById(R.id.vgb);

        viewPager.setAdapter(new SlidingImage_Adapter(context, images));

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                final LinearLayout linearLayout = v.findViewById(R.id.vgb);
                linearLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    ImageView image = new ImageView(context);
                    int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 8, getResources().getDisplayMetrics());
                    int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 8, getResources().getDisplayMetrics());
                    int marginleft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 5, getResources().getDisplayMetrics());
//                    image.setLayoutParams(new android.view.ViewGroup.LayoutParams(width, height));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
                    params.setMargins(marginleft, 0, 0, 0);
                    image.setLayoutParams(params);
                    if (i == position) {
                        image.setImageResource(R.drawable.whitedot);
                    } else {
                        image.setImageResource(R.drawable.whitedot);
                        image.setAlpha((float) 0.4);
                    }
                    linearLayout.addView(image);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        for (int i = 0; i < images.size(); i++) {
            ImageView image = new ImageView(context);
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 8, getResources().getDisplayMetrics());
            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 8, getResources().getDisplayMetrics());
            int marginleft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 5, getResources().getDisplayMetrics());
//            image.setLayoutParams(new android.view.ViewGroup.LayoutParams(width, height));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
            params.setMargins(marginleft, 0, 0, 0);
            image.setLayoutParams(params);

            if (i == 0) {
                image.setImageResource(R.drawable.whitedot);
            } else {
                image.setImageResource(R.drawable.whitedot);
                image.setAlpha((float) 0.4);
            }
            linearLayout.addView(image);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
        preferences = new Preferences(context);
        loginDialog = new LoginDialog(context);
//        n = (gotoNext) context;
//        dialog = new LoginDialog(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void change(int pos, String fro) {
        Service_data d = eventList.get(pos);
        if (!fro.equals("call")) {
            tempList.remove(pos);
            d.setStatus(1);
            tempList.add(pos, d);
            eventRecycler.setAdapter(new EventAdapter(tempList, context, Service_Details.this,h));
            eventRecycler.setLayoutManager(new LinearLayoutManager(context));
            book_ley.setVisibility(View.VISIBLE);
            doFilter("ADD", d);
        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(d.getSalon_contct_no()));
            context.startActivity(intent);
        }
    }

    private void openDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.login_dialog);

        ImageView closeImage = dialog.findViewById(R.id.closeImage);
        RelativeLayout signIn = dialog.findViewById(R.id.signInRel);
        CustomTextviewRegular signupTxt = dialog.findViewById(R.id.signupTxt);
        CustomTextviewRegular forgotpassword;
        forgotpassword = dialog.findViewById(R.id.forgotTxt);
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                context.startActivity(new Intent(context, ForgotPasswordActivity.class));
            }
        });
        final CustomEdittextRegular emailEdt, passswordEdt;
        emailEdt = dialog.findViewById(R.id.emailEdt);
        passswordEdt = dialog.findViewById(R.id.passwordEdt);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(emailEdt, passswordEdt);
            }
        });

        signupTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                n.gotoN(-1, "SignUp", "");
            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

    }

    private void validate(CustomEdittextRegular emailEdt, CustomEdittextRegular passswordEdt) {

        if (!validateEmail(emailEdt)) {
            return;
        }
        if (!validatePassword(passswordEdt)) {
            return;
        }
        if (connectionDetector.isNetworkAvailable()) {
            login(emailEdt, passswordEdt);
        } else
            connectionDetector.error_Dialog();
    }

    private boolean validateEmail(CustomEdittextRegular emailEdt) {
        String e = emailEdt.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        } else if (!isValidEmail(e)) {
            Toast.makeText(context, "Please enter valid email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        }
        return true;
    }

    private boolean validatePassword(CustomEdittextRegular passswordEdt) {
        if (passswordEdt.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter your password", Toast.LENGTH_SHORT).show();
            requestFocus(passswordEdt);
            return false;
        } else if (passswordEdt.getText().toString().trim().length() < 4) {
            Toast.makeText(context, "Please enter valid password", Toast.LENGTH_SHORT).show();
            requestFocus(passswordEdt);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void login(final CustomEdittextRegular emailEdt, final CustomEdittextRegular passswordEdt) {
        progDialog.ProgressDialogStart();
        JSONObject params = new JSONObject();
        try {
            final String eml = emailEdt.getText().toString();
            final String psd = passswordEdt.getText().toString();
            params.accumulate("user_email", eml);
            params.accumulate("user_password", psd);
            params.accumulate("role", "2");
            params.accumulate("device_type", "1");
            params.accumulate("device_token", "123");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        json j = new json(API.LOGIN, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
                System.out.println(response.toString());
                try {
                    if (response.getString("status").equals("1")) {
                        JSONObject jsonObject = response.getJSONObject("user_details");
                        preferences.setClientID(jsonObject.getString("id"));
                        preferences.setLogin(true);
                        preferences.setUserLogin(true);
                        preferences.lastLogin("user");
                        ll_details.setVisibility(View.GONE);
                        ll_ratesalon.setVisibility(View.VISIBLE);
                        ll_shopeinfo.setVisibility(View.GONE);
                        disableLogin();
                        HomeScreen.toolTitle.setText(nm.getText().toString());
                        if (dialog.isShowing())
                            dialog.dismiss();
                    } else {
                        Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                Toast.makeText(context, "Somthing wents wrong", Toast.LENGTH_SHORT).show();
                System.out.println(error.toString());
            }
        });
        AppController.getInstance().addToRequestQueue(j);
    }

    @Override
    public void add_service_to_cart(String work, Service_data data) {

        book_ley.setVisibility(View.VISIBLE);

        doFilter(work, data);
    }

    private void doFilter(String work, Service_data data) {

        if (booked_main_category_list.size() != 0) {
            for (int i = 0; i < booked_main_category_list.size(); i++) {
                Categeory_Filter filter = booked_main_category_list.get(i);
                if (filter.getSub_cat_parent_id().equals(data.getSub_cat_parent_id())) {
                    ArrayList<Service_data> serviceList = filter.getBooked_sub_categories();
                    if (serviceList.size() != 0) {
                        if (work.equals("REMOVE")) {
                            for (int j = 0; j < serviceList.size(); j++) {
                                Service_data sdata = serviceList.get(j);
                                if (sdata.getSub_cat_id().equals(data.getSub_cat_id())) {
                                    serviceList.remove(j);

                                    totalPay = totalPay - Integer.parseInt(data.getSub_cat_price());
                                    serNmsArrray.remove(data.getSub_cat_name());

                                    if (serviceList.size() == 0) {
                                        booked_main_category_list.remove(i);
                                        if (booked_main_category_list.size() == 0)
                                            book_ley.setVisibility(View.GONE);
                                        break;
                                    } else {
                                        filter.setBooked_sub_categories(serviceList);
                                        booked_main_category_list.remove(i);
                                        booked_main_category_list.add(i, filter);
                                        break;
                                    }

                                }
                            }
                            break;
                        } else {
                            serviceList.add(data);
                            totalPay = totalPay + Integer.parseInt(data.getSub_cat_price());
                            serNmsArrray.add(data.getSub_cat_name());
                            filter.setBooked_sub_categories(serviceList);
                            booked_main_category_list.remove(i);
                            booked_main_category_list.add(i, filter);
                            break;
                        }
                    } else {
                        serviceList.add(data);
                        totalPay = totalPay + Integer.parseInt(data.getSub_cat_price());
                        serNmsArrray.add(data.getSub_cat_name());
                        filter.setBooked_sub_categories(serviceList);
                        booked_main_category_list.remove(i);
                        booked_main_category_list.add(i, filter);
                        break;
                    }
                } else {
                    if (i == booked_main_category_list.size() - 1) {
                        totalPay = totalPay + Integer.parseInt(data.getSub_cat_price());
                        serNmsArrray.add(data.getSub_cat_name());
                        booked_sub_categories = new ArrayList<>();
                        booked_sub_categories.add(data);
                        booked_main_category_list.add(new Categeory_Filter(data.getSub_cat_parent_id(), booked_sub_categories));
                        break;
                    }
                }
            }
        } else {
            serNmsArrray = new ArrayList<>();
            serNmsArrray.add(data.getSub_cat_name());
            totalPay = Integer.parseInt(data.getSub_cat_price());
            booked_sub_categories = new ArrayList<>();
            booked_sub_categories.add(data);
            booked_main_category_list.add(new Categeory_Filter(data.getSub_cat_parent_id(), booked_sub_categories));
        }


        total_payment.setText(String.valueOf(totalPay) + "AED");
        boolean f = false;
        for (String str : serNmsArrray) {
            if (f)
                serNames = serNames + "\n" + str;
            else {
                f = true;
                serNames = str;
            }
        }
        book_services_list.setText(serNames);
    }

    @Override
    public void filter(String sub_cat_name, String sub_cat_id) {

        tempList = new ArrayList<>();
        for (int i = 0; i < eventList.size(); i++) {
            Service_data d = eventList.get(i);
            if (d.getSub_cat_parent_id().equals(sub_cat_id)) {
                tempList.add(d);
            }
        }

        if (booked_main_category_list != null) {
            if (booked_main_category_list.size() != 0) {
                for (Categeory_Filter filter : booked_main_category_list) {
                    if (filter.getSub_cat_parent_id().equals(sub_cat_id)) {
                        ArrayList<Service_data> sdatalist = filter.getBooked_sub_categories();

                        if (sdatalist.size() != 0) {
                            for (Service_data sdata : sdatalist) {
                                for (int i = 0; i < tempList.size(); i++) {
                                    Service_data d = tempList.get(i);
                                    if (d.getSub_cat_id().equals(sdata.getSub_cat_id())) {
                                        tempList.remove(i);
                                        tempList.add(i, sdata);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        eventRecycler.setAdapter(new EventAdapter(tempList, context, Service_Details.this,h));
        eventRecycler.setLayoutManager(new LinearLayoutManager(context));
    }
}
