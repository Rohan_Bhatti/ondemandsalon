package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.Payment;
import com.ondemandsalon.utils.CustomTextviewSemiBold;

import org.json.JSONException;
import org.json.JSONObject;


@SuppressLint("ValidFragment")
public class Package_Standard extends Fragment {

    CardView buy;
    Context c;
    CustomTextviewSemiBold price, description;
    JSONObject jsonObject;

    @SuppressLint("ValidFragment")
    public Package_Standard(JSONObject package_details) {
        this.jsonObject = package_details;
    }

    public Package_Standard() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_package__standard, container, false);

        buy = v.findViewById(R.id.buy_btn_s);
        price = v.findViewById(R.id.price_s);
        description = v.findViewById(R.id.description_s);

        try {
            price.setText(jsonObject.getString("pack_price"));
            description.setText(jsonObject.getString("pack_desc"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(c, Payment.class));
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        c = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
