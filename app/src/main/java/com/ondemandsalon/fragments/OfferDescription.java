package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.model.Shops;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;

import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfferDescription extends Fragment {

    Context c;
    CustomTextviewSemiBold titleTxt, startDate, endDate;
    CustomTextviewRegular name, area, desc;
    ImageView salonImg;
    Shops s;
    RatingBar salon_rating;


    public OfferDescription() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public OfferDescription(Shops s) {
        // Required empty public constructor
        this.s = s;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_offer_description, container, false);

        titleTxt = v.findViewById(R.id.titleTxt);
        startDate = v.findViewById(R.id.startdateTxt);
        endDate = v.findViewById(R.id.enddateTxt);
        name = v.findViewById(R.id.nm);
        area = v.findViewById(R.id.nm1);
        desc = v.findViewById(R.id.descTxt);
        salonImg = v.findViewById(R.id.salonImg);
        salon_rating = v.findViewById(R.id.ratingBar);


        Glide.with(c).load(s.getSalon_image()).into(salonImg);
        name.setText(s.getSalon_name());
        area.setText(s.getSalon_location());
        salon_rating.setRating(Float.parseFloat(s.getSalon_rating()));
        titleTxt.setText(s.getOffer_title());
        desc.setText(s.getOffer_description());
        startDate.setText(s.getFrom_date());
        endDate.setText(s.getTo_date());

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.HOME_TOOL = "Today's Offer";
                    HomeNew.toolTitle.setText("Today's Offer");
                    return true;
                }
                return false;
            }
        });


        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        c = context;
    }
}
