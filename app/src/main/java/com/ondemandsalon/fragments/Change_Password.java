package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class Change_Password extends Fragment {

    CustomEdittextRegular old_password, new_password, confirm_password;
    RelativeLayout change_password;
    Context context;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;
    String from;

    @SuppressLint("ValidFragment")
    public Change_Password(String s) {
        this.from = s;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_change__password, container, false);

        old_password = v.findViewById(R.id.oldpassEdta);
        new_password = v.findViewById(R.id.newpassEdta);
        confirm_password = v.findViewById(R.id.cnfrmpassEdta);
        change_password = v.findViewById(R.id.saveRel);

        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

        RelativeLayout backrel = v.findViewById(R.id.backRel);
        backrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                if (from.equals("FROM USER")) {
                    HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_0);
                } else {

                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                }
            }
        });
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    if (from.equals("FROM USER")) {
                        HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_0;
                        HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_0);
                    } else {

                        HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                        HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                    }

                    return true;
                }
                return false;
            }
        });

        return v;
    }

    private void validate() {

        String old_pass = old_password.getText().toString().trim();
        String new_pass = new_password.getText().toString().trim();
        String confirm_pass = confirm_password.getText().toString().trim();

        if (old_pass.isEmpty()) {
            old_password.setError("Please enter old password");
            requestFocus(old_password);
            return;
        }

        if (new_pass.isEmpty()) {
            new_password.setError("Please enter new password");
            requestFocus(new_password);
            return;
        }

        if (confirm_pass.isEmpty()) {
            confirm_password.setError("Please enter confirm password");
            requestFocus(confirm_password);
            return;
        }

        if (!new_pass.equals(confirm_pass)) {
            Toast.makeText(getContext(), "New password and Confirm password must be same", Toast.LENGTH_SHORT).show();
            return;
        }

        changePassword();
    }

    private void changePassword() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject params = new JSONObject();
            try {
                params.accumulate("user_old_password", old_password.getText().toString().trim());
                params.accumulate("user_new_password", new_password.getText().toString().trim());
                params.accumulate("user_id", preferences.getClientID());
                if (from.equals("FROM USER")) {
                    params.accumulate("role", "2");
                } else {
                    params.accumulate("role", "1");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            json j = new json(API.CHANGE_PASSWORD_USER, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            getFragmentManager().popBackStack();
                            if (from.equals("FROM USER")) {
                                HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_0;
                                HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_0);
                            } else {

                                HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                                HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                            }
                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    Toast.makeText(context, "Somthing wents wrong", Toast.LENGTH_SHORT).show();

                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
        preferences = new Preferences(context);
    }
}
