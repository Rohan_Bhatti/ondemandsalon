package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ondemandsalon.Adapter.offer_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.model.Shops;

import java.util.ArrayList;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewOffers extends Fragment {


    RecyclerView offer;
    Context context;
    HomeNew p;
    ArrayList<Shops> offerlist;

    public ViewOffers() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public ViewOffers(HomeNew p) {
        // Required empty public constructor
        this.p = p;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_view_offers, container, false);

        offer = v.findViewById(R.id.offerRecycler);
        offerlist = new ArrayList<>();

        offer.setLayoutManager(new LinearLayoutManager(context));
        offer.setAdapter(new offer_adapter(p,offerlist,"provider"));


        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                    return true;
                }
                return false;
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
