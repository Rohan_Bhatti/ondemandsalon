package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.ondemandsalon.Adapter.Appointment_details_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.model.Provider_Appo_data;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import static com.ondemandsalon.helper.CONSTANTS.APPOINTMENT;
import static com.ondemandsalon.helper.CONSTANTS.EMPLOYEE_LIST;

public class Appointment_details extends Fragment implements HomeNew.ChangeVal {
    RelativeLayout date_relative, time_relative;
    Context context;
    RecyclerView appointment_details_recycler;
    CustomTextviewRegular date_txt, time_txt, name, location, number;
    HomeNew p;
    Provider_Appo_data data;
    ArrayList<Service_data> sdatalist;
    SimpleDateFormat format, format1;
    private int mYear, mMonth, mDay, mHour, mMinute;

    public Appointment_details() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Appointment_details(Context context, HomeNew p, Provider_Appo_data data) {
        this.p = p;
        this.data = data;
    }

    public void enabletext() {

        date_relative.setEnabled(true);
        time_relative.setEnabled(true);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_details, container, false);
        appointment_details_recycler = view.findViewById(R.id.appintment_details_recycler);
        date_relative = view.findViewById(R.id.date_relative);
        time_relative = view.findViewById(R.id.time_relative);
        date_txt = view.findViewById(R.id.date_txt);
        time_txt = view.findViewById(R.id.time_txt);

        name = view.findViewById(R.id.nameTxt);
        location = view.findViewById(R.id.phoneTxt);
        number = view.findViewById(R.id.locationTxt);

        format = new SimpleDateFormat("dd/mm/yyyy");
        format1 = new SimpleDateFormat("hh:mm a");
        time_txt.setText(data.getS());
        date_txt.setText(format.format(new Date()));
        date_relative.setEnabled(false);
        time_relative.setEnabled(false);

        name.setText(data.getSana_saikh());

        sdatalist = new ArrayList<>();
//        sdatalist.add(new Service_data(data.getColor(), 0, "AED30"));

        appointment_details_recycler.setAdapter(new Appointment_details_adapter(context, sdatalist));
        appointment_details_recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        date_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        time_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        Time time = new Time(hourOfDay, minute, 0);
                        Format format = new SimpleDateFormat("hh:mm a");
                        time_txt.setText(format.format(time));
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.APPOINTMENT_TOOL = HomeNew.APPOINTMENT_LEVEL_1;
                    HomeNew.toolTitle.setText(HomeNew.APPOINTMENT_LEVEL_1);
                    HomeNew.toolImg.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void change(int pos, String fro) {
        enabletext();
    }
}
