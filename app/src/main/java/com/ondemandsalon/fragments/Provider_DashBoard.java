package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class Provider_DashBoard extends Fragment {

    Context context;
    RelativeLayout from_date, to_date;
    CustomTextviewRegular from_date_txt, to_date_txt;
    Provider p;
    private int mYear, mMonth, mDay;

    public Provider_DashBoard() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Provider_DashBoard(Provider provider) {
        p = provider;
    }

    @SuppressLint("ValidFragment")
    public Provider_DashBoard(HomeNew homeNew) {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.provider_dash_board, container, false);
        from_date = view.findViewById(R.id.from_date_relative);
        to_date = view.findViewById(R.id.to_date_relative);
        from_date_txt = view.findViewById(R.id.from_date_txt);
        to_date_txt = view.findViewById(R.id.to_date_txt);

        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        from_date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        to_date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
