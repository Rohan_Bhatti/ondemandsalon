package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ondemandsalon.Adapter.ShopAdapter;
import com.ondemandsalon.Adapter.offer_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Shops;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodaysOffer extends Fragment {

    RecyclerView todaysofferRecycle;
    ArrayList<Shops> shopsList;
    Context context;
    HomeNew h;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;


    public TodaysOffer() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public TodaysOffer(HomeNew h) {
        this.h = h;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_todays_offer, container, false);
        todaysofferRecycle = v.findViewById(R.id.todaysofferRecycle);

        getOffersList();

      /*  shopsList = new ArrayList<>();

        shopsList.add(new Shops("Dubai Celeb", "Jumeriah Bldg - 1", "4 Reviews", "10% Off on Hai Cut ,Hair Color,Hair Style"));
        shopsList.add(new Shops("Sentation", "Jumeriah Bldg - 1", "1 Reviews", "20AED Off on Hai Cut ,Hair Color,Hair Style"));
        shopsList.add(new Shops("Enhance Salon", "Satellite", "33 Reviews", "50% Off on any Service of hair category"));
        shopsList.add(new Shops("The New Look", "Shyamal", "48 Reviews", "30% Off on Medicure & Pedicure"));
        shopsList.add(new Shops("Enrich Salon", "Jodhpur", "6 Reviews", "40AED Off on Hair Cut and Shaving"));
        shopsList.add(new Shops("Lakme Salon", "Vastrapur", "100 Reviews", "10% Off on All Services"));

        todaysofferRecycle.setAdapter(new offer_adapter(h, shopsList));
        todaysofferRecycle.setLayoutManager(new LinearLayoutManager(context));*/

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.HOME_TOOL = YOUR_LOCATION;
                    HomeNew.toolTitle.setText(YOUR_LOCATION);
                    return true;
                }
                return false;
            }
        });

        return v;
    }

    private void getOffersList() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();

            json j = new json(API.TODAYS_OFFERS, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONArray offer_list = response.getJSONArray("offers_data");
                            if (offer_list.length() == 0)
                                return;

                            shopsList = new ArrayList<>();

                            for (int i = 0; i < offer_list.length(); i++) {
                                JSONObject jsonObject = (JSONObject) offer_list.get(i);
                                String from_date = jsonObject.getString("off_start_date");
                                String to_date = jsonObject.getString("off_end_date");
                                String date = from_date + " To " + to_date;
                                String offer_title = jsonObject.getString("off_name");
                                String offer_description = jsonObject.getString("off_desc");
                                String salon_name = jsonObject.getString("business_name");
                                String salon_image = jsonObject.getString("banner_name");
                                String salon_location = jsonObject.getString("user_location");
                                String salon_rating = jsonObject.getString("rat_out_of_5");
                                String salon_id = jsonObject.getString("id");

                                shopsList.add(new Shops(offer_title, date, offer_description, salon_image, salon_name, salon_location, salon_rating, from_date, to_date, salon_id));
                            }
                            todaysofferRecycle.setAdapter(new offer_adapter(h, shopsList,"user"));
                            todaysofferRecycle.setLayoutManager(new LinearLayoutManager(context));
                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    System.out.println(error.toString());
                    Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        preferences = new Preferences(context);
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
