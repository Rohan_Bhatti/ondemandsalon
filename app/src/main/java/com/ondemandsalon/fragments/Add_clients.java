package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.model.Provider_Client;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ondemandsalon.helper.CONSTANTS.CLIENT;
import static com.ondemandsalon.helper.CONSTANTS.EMPLOYEE_LIST;

public class Add_clients extends Fragment {

    RelativeLayout saveRel, backrel;
    String frm;
    Provider_Client c;
    CustomEdittextRegular nm, lnm, phon, streat, email, birth_date, city, post_code;
    CustomTextviewRegular sv;
    Context context;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    ImageView profile_image;

    public Add_clients() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Add_clients(Context context,String nw, Provider_Client c) {
        frm = nw;
        this.c = c;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_clients, container, false);

        nm = view.findViewById(R.id.nm);
        lnm = view.findViewById(R.id.lnm);
        phon = view.findViewById(R.id.phonetxt);
        streat = view.findViewById(R.id.streat);
        saveRel = view.findViewById(R.id.saveRel);
        backrel = view.findViewById(R.id.backRel);
        email = view.findViewById(R.id.email_text);
        birth_date = view.findViewById(R.id.birth_date);
        city = view.findViewById(R.id.city_txt);
        post_code = view.findViewById(R.id.postcode_txt);
        streat = view.findViewById(R.id.streat);
        sv = view.findViewById(R.id.svtxt);
        profile_image = view.findViewById(R.id.item_img);

        if (frm.equals("edit")) {
            String[] a = c.getName().split(" ");
            nm.setText(a[0]);
            lnm.setText(a[1]);
            phon.setText(c.getMobileno());
            streat.setText(c.getLocation());
            sv.setText("Update");
        } else
            sv.setText("Save");

        saveRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeNew)context).getSupportFragmentManager().popBackStack();
//                validate();
                HomeNew.CLIENT_TOOL = HomeNew.CLIENT_LEVEL_0;
                HomeNew.toolTitle.setText(HomeNew.CLIENT_LEVEL_0);
                HomeNew.toolImg.setVisibility(View.VISIBLE);
            }
        });
        backrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeNew)context).getSupportFragmentManager().popBackStack();
                HomeNew.CLIENT_TOOL = HomeNew.CLIENT_LEVEL_0;
                HomeNew.toolTitle.setText(HomeNew.CLIENT_LEVEL_0);
                HomeNew.toolImg.setVisibility(View.VISIBLE);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    ((HomeNew)context).getSupportFragmentManager().popBackStack();
                    HomeNew.CLIENT_TOOL = HomeNew.CLIENT_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.CLIENT_LEVEL_0);
                    HomeNew.toolImg.setVisibility(View.VISIBLE);
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private void validate() {
        if (!validateEmail()) {
            return;
        }

        if (nm.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter valid name", Toast.LENGTH_SHORT).show();
            requestFocus(nm);
            return;
        }

        if (lnm.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter valid name", Toast.LENGTH_SHORT).show();
            requestFocus(lnm);
            return;
        }

        if (birth_date.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter birth of date", Toast.LENGTH_SHORT).show();
            requestFocus(birth_date);
            return;
        }

        if (streat.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter street address", Toast.LENGTH_SHORT).show();
            requestFocus(streat);
            return;
        }
        if (city.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter city name", Toast.LENGTH_SHORT).show();
            requestFocus(city);
            return;
        }
        if (post_code.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter postal code", Toast.LENGTH_SHORT).show();
            requestFocus(post_code);
            return;
        }

        String phone = phon.getText().toString().trim();
        if (phone.isEmpty()) {
            Toast.makeText(context, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            requestFocus(phon);
            return;
        } else if (phone.length() < 10) {
            Toast.makeText(context, "Phone number must be atleast 10 digit", Toast.LENGTH_SHORT).show();
            requestFocus(phon);
            return;
        }

        if (connectionDetector.isNetworkAvailable())
            AddClient();
        else
            connectionDetector.error_Dialog();


    }

    private void AddClient() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("cl_fname", nm.getText().toString().trim());
                jsonObject.accumulate("cl_lname", lnm.getText().toString().trim());
                jsonObject.accumulate("cl_phone", phon.getText().toString().trim());
                jsonObject.accumulate("cl_email", email.getText().toString().trim());
                jsonObject.accumulate("cl_birthday", birth_date.getText().toString().trim());
                jsonObject.accumulate("cl_street", streat.getText().toString().trim());
                jsonObject.accumulate("cl_city", city.getText().toString().trim());
                jsonObject.accumulate("cl_postcode", post_code.getText().toString().trim());
                jsonObject.accumulate("cl_image", profile_image.getDrawable());
                jsonObject.accumulate("cl_added_by", "1");

                json json = new json(API.ADD_CLIENT, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private boolean validateEmail() {
        String e = email.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(email);
            return false;
        } else {
            if (!isValidEmail(e)) {
                Toast.makeText(context, "Please enter valid email", Toast.LENGTH_SHORT).show();
                requestFocus(email);
                return false;
            }
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
