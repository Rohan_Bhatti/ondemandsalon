package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.model.Ratings_Data;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;

/**
 * A simple {@link Fragment} subclass.
 */
public class Ratreviews_details extends Fragment {

    CustomTextviewSemiBold name;
    CustomTextviewRegular date;
    RatingBar r1, r2, r3, r4, r5, r6;
    CustomEdittextRegular commentEdt;
    RelativeLayout backrel;
    String from;
    Ratings_Data rdata;


    public Ratreviews_details() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Ratreviews_details(String nm, String dt, String cmt, String provider) {
        from = provider;
        /*this.nm = nm;
        this.dt = dt;
        this.cmt = cmt;*/
    }

    @SuppressLint("ValidFragment")
    public Ratreviews_details(Ratings_Data rdata, String user) {
        this.rdata = rdata;
        from = user;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_user_ratreviews_details, container, false);

        name = v.findViewById(R.id.name);
        date = v.findViewById(R.id.date);
        commentEdt = v.findViewById(R.id.commentEdt);
        r1 = v.findViewById(R.id.ratingB1);
        r2 = v.findViewById(R.id.ratingB2);
        r3 = v.findViewById(R.id.ratingB3);
        r4 = v.findViewById(R.id.ratingB4);
        r5 = v.findViewById(R.id.ratingB5);
        r6 = v.findViewById(R.id.ratingB6);
        backrel = v.findViewById(R.id.backRel);

        backrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                if (from.equals("USER")) {
                    HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_1;
                    HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_1);
                } else {
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_1;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_1);
                }
            }
        });

        if (from.equals("USER")) {
            name.setText(rdata.getSalon_name());
            date.setText(rdata.getRating_date());
            commentEdt.setText(rdata.getRating_description());
            r1.setRating(Float.parseFloat(rdata.getRat_time()));
            r2.setRating(Float.parseFloat(rdata.getRat_quality()));
            r3.setRating(Float.parseFloat(rdata.getRat_safety()));
            r4.setRating(Float.parseFloat(rdata.getRat_result()));
            r5.setRating(Float.parseFloat(rdata.getRat_proffesnalism()));
            r6.setRating(Float.parseFloat(rdata.getRat_rewarding()));

            r1.setEnabled(false);
            r2.setEnabled(false);
            r3.setEnabled(false);
            r4.setEnabled(false);
            r5.setEnabled(false);
            r6.setEnabled(false);
        }

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    if (from.equals("USER")) {
                        HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_1;
                        HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_1);
                    } else {
                        HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_1;
                        HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_1);
                    }
                    return true;
                }
                return false;
            }
        });

        return v;
    }

}
