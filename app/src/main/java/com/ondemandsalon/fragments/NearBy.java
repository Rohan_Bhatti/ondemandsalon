package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.ondemandsalon.Adapter.ShopAdapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Shops;
import com.ondemandsalon.model.User_Home_Category;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;

public class NearBy extends Fragment {


    RecyclerView nearByRecycler;
    ArrayList<Shops> shopsList;
    Context context;
    HomeNew h;
    String latitude, longitude;
    Preferences preferences;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;


    public NearBy() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public NearBy(HomeNew h, String lat, String lng) {
        this.h = h;
        this.latitude = lat;
        this.longitude = lng;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_near_by, container, false);
        nearByRecycler = v.findViewById(R.id.nearbyRecycle);

        getNearBySalon(latitude, longitude);

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.HOME_TOOL = YOUR_LOCATION;
                    HomeNew.toolTitle.setText(YOUR_LOCATION);
                    return true;
                }
                return false;
            }
        });

        return v;
    }

    private void getNearBySalon(String latitude, String longitude) {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("latitude", latitude);
                jsonObject.accumulate("longitude", longitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            json json = new json(API.NEAR_BY, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONArray Near_By_Places = response.getJSONArray("near_by_detail");
                            if (Near_By_Places.length() == 0)
                                return;
                            shopsList = new ArrayList<>();
                            for (int i = 0; i < Near_By_Places.length(); i++) {
                                JSONObject jsonObject1 = (JSONObject) Near_By_Places.get(i);
                                String salon_id = jsonObject1.getString("id");
                                String salon_image = jsonObject1.getString("banner_name");
                                String salon_name = jsonObject1.getString("business_name");
                                String salon_location = jsonObject1.getString("user_location");
                                String salon_rating = jsonObject1.getString("rat_out_of_5");
                                String salon_total_reviews = jsonObject1.getString("total_review");
                                String salon_happy_customers = jsonObject1.getString("happy_performance");

                                shopsList.add(new Shops(salon_image, salon_name, salon_location, salon_rating, salon_total_reviews, salon_happy_customers, salon_id));
                            }
                            nearByRecycler.setAdapter(new ShopAdapter(shopsList, h, "shops", context,"near_by"));
                            nearByRecycler.setLayoutManager(new LinearLayoutManager(context));
                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    Toast.makeText(context, "Something wents wrong", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(json);
        } else {
            connectionDetector.error_Dialog();
        }
    }


    /*public void getNearBySalon(final String latitude, final String longitude) {
        progDialog.ProgressDialogStart();
        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST, API.NEAR_BY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progDialog.ProgressDialogStop();
                System.out.println(response);
                try {
                    JSONObject newResponse = new JSONObject(response);
                    if (newResponse.getString("status").equals("1")) {
                        JSONArray Near_By_Places = newResponse.getJSONArray("near_by_detail");
                        if (Near_By_Places.length() == 0)
                            return;
                        shopsList = new ArrayList<>();
                        for (int i = 0; i < Near_By_Places.length(); i++) {
                            JSONObject jsonObject1 = (JSONObject) Near_By_Places.get(i);
                            String salon_id = jsonObject1.getString("id");
                            String salon_image = jsonObject1.getString("banner_name");
                            String salon_name = jsonObject1.getString("business_name");
                            String salon_location = jsonObject1.getString("user_location");
                            String salon_rating = jsonObject1.getString("rat_out_of_5");
                            String salon_total_reviews = jsonObject1.getString("total_review");
                            String salon_happy_customers = jsonObject1.getString("happy_performance");

                            shopsList.add(new Shops(salon_image, salon_name, salon_location, salon_rating, salon_total_reviews, salon_happy_customers, salon_id));
                        }
                        nearByRecycler.setAdapter(new ShopAdapter(shopsList, h, "shops", context));
                        nearByRecycler.setLayoutManager(new LinearLayoutManager(context));
                    } else {
                        Toast.makeText(context, newResponse.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjRequest);
    }*/


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        preferences = new Preferences(context);
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
