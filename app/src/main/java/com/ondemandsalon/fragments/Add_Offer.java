package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;

import java.util.Calendar;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;

/**
 * A simple {@link Fragment} subclass.
 */
public class Add_Offer extends Fragment {

    Context context;
    CustomTextviewSemiBold adTxt;
    String frm;


    RelativeLayout from_date, to_date, addrel,backrel;
    CustomTextviewRegular from_date_txt, to_date_txt;

    private int mYear, mMonth, mDay;

    public Add_Offer() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Add_Offer(String edit) {
        frm = edit;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add__offer, container, false);
        adTxt = v.findViewById(R.id.adtxt);
        addrel = v.findViewById(R.id.offeraddRel);
        backrel = v.findViewById(R.id.backRel);

        addrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                if (frm.equals("edit")) {
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_1;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_1);
                } else {
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                }
            }
        });

        backrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                if (frm.equals("edit")) {
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_1;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_1);
                } else {
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                }
            }
        });

        if (frm.equals("edit"))
            adTxt.setText("Update Offer");

        from_date = v.findViewById(R.id.fromdate);
        to_date = v.findViewById(R.id.todate);
        from_date_txt = v.findViewById(R.id.from_date_txt);
        to_date_txt = v.findViewById(R.id.to_date_txt);


        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        from_date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        to_date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();

                    if (frm.equals("edit")) {
                        HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_1;
                        HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_1);
                    } else {
                        HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                        HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                    }
                    return true;
                }
                return false;
            }
        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
