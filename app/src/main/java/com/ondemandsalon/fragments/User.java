package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.ForgotPasswordActivity;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.activity.LoginActivity;
import com.ondemandsalon.activity.SignUp_User;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomViewPager;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.LoginDialog;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ondemandsalon.helper.CONSTANTS.PROFILE;
import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;

/**
 * A simple {@link Fragment} subclass.
 */
public class User extends Fragment implements View.OnClickListener {

    public static LinearLayout dashLin, appointmentLin, clientLin, accLin, loginLin;
    public static String HOME_TEXT = YOUR_LOCATION, PROFILE_TEXT = PROFILE;
    public boolean inside = false;
    Context context;
    CustomViewPager dashboardpager;
    ImageView dashImg, appointmentImg, clientImg, accImg;
    CustomTextviewRegular dashTxt, appointmentTxt, clientTxt, accTxt;
    ViewPagerAdapter pagerAdapter;
    Preferences preferences;
    FragmentTransaction ft;
    gotoNext n;
    FragmentManager manager;
    Dialog dialog;

    ConnectionDetector connectionDetector;
    ProgDialog progDialog;

    public User() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public User(FragmentManager supportFragmentManager) {
        manager = supportFragmentManager;
    }

    public static void disableLogin() {
        loginLin.setVisibility(View.GONE);
        accLin.setVisibility(View.VISIBLE);
        clientLin.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        dashboardpager = v.findViewById(R.id.dashboardpager);

        dashLin = v.findViewById(R.id.dashLin);
        appointmentLin = v.findViewById(R.id.appointmentLin);
        clientLin = v.findViewById(R.id.clientLin);
        accLin = v.findViewById(R.id.accLin);
        loginLin = v.findViewById(R.id.loginLin);

        dashImg = v.findViewById(R.id.dashboardImg);

        appointmentImg = v.findViewById(R.id.appointmentImg);

        clientImg = v.findViewById(R.id.clientImg);

        accImg = v.findViewById(R.id.accountImg);


        dashTxt = v.findViewById(R.id.dashboardTxt);
        appointmentTxt = v.findViewById(R.id.appointmentTxt);
        clientTxt = v.findViewById(R.id.clientTxt);
        accTxt = v.findViewById(R.id.accontTxt);

        dashLin.setOnClickListener(this);
        appointmentLin.setOnClickListener(this);
        clientLin.setOnClickListener(this);
        accLin.setOnClickListener(this);
        loginLin.setOnClickListener(this);

        if (preferences.getLogin()) {
            if (preferences.getUserLogin())
                disableLogin();
            else
                enableLogin();
        } else {
            enableLogin();
        }

        setIconAndColor();

        setViewPager();

        dashboardpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setIconAndColor();
                switch (position) {
                    case 0:
                        if (inside) {
                            if (manager.getBackStackEntryCount() > 0) {
                                manager.popBackStackImmediate();
                            }
                            changeIcAndColor(0);
                        } else
                            changeIcAndColor(0);

                        break;
                    case 1:
                        changeIcAndColor(1);
                        break;
                    case 2:
                        changeIcAndColor(2);
                        break;
                    case 3:
                        changeIcAndColor(3);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;
    }

    private void enableLogin() {
        loginLin.setVisibility(View.VISIBLE);
        accLin.setVisibility(View.GONE);
        clientLin.setVisibility(View.GONE);
    }

    public void setHomeDisable() {
        dashImg.setImageResource(R.drawable.home_black_ic);
        dashTxt.setTextColor(context.getResources().getColor(R.color.black));
    }

    private void setIconAndColor() {

        dashImg.setImageResource(R.drawable.home_black_ic);
        appointmentImg.setImageResource(R.drawable.search);
        clientImg.setImageResource(R.drawable.appointments_ic_black);
        accImg.setImageResource(R.drawable.accounts_ic_black);

        dashTxt.setTextColor(context.getResources().getColor(R.color.black));
        appointmentTxt.setTextColor(context.getResources().getColor(R.color.black));
        clientTxt.setTextColor(context.getResources().getColor(R.color.black));
        accTxt.setTextColor(context.getResources().getColor(R.color.black));

        dashTxt.setText("Home");
        appointmentTxt.setText("Search");
        clientTxt.setText("My Appointments");
        accTxt.setText("Profile");
    }

    private void setViewPager() {

        pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());

/*        pagerAdapter.addFragment(new *//*Provider_DashBoard*//*User_Home(this), "");
        pagerAdapter.addFragment(new User_Search(this), "");
        pagerAdapter.addFragment(new User_Appointments(), "");
        pagerAdapter.addFragment(new User_Profile(this), "");*/

        dashboardpager.disableScroll(true);
        dashboardpager.setAdapter(pagerAdapter);
        dashboardpager.setOffscreenPageLimit(4);
        dashboardpager.setCurrentItem(0);
        dashImg.setImageResource(R.drawable.home_ic);
        dashTxt.setTextColor(context.getResources().getColor(R.color.blue));
        dashTxt.setText("Home");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        preferences = new Preferences(context);
        n = (gotoNext) context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    @Override
    public void onClick(View view) {

        setIconAndColor();

        switch (view.getId()) {
            case R.id.dashLin:
                changeIcAndColor(0);
                break;
            case R.id.appointmentLin:
                changeIcAndColor(1);
                break;
            case R.id.clientLin:
                changeIcAndColor(2);
                break;
            case R.id.accLin:
                changeIcAndColor(3);
                break;
            case R.id.loginLin:
                openDialog();
                break;
        }
    }

    private void openDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.login_dialog);

        ImageView closeImage = dialog.findViewById(R.id.closeImage);
        RelativeLayout signIn = dialog.findViewById(R.id.signInRel);
        CustomTextviewRegular signupTxt = dialog.findViewById(R.id.signupTxt);
        CustomTextviewRegular forgotpassword;
        forgotpassword = dialog.findViewById(R.id.forgotTxt);
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                context.startActivity(new Intent(context, ForgotPasswordActivity.class));
            }
        });
        final CustomEdittextRegular emailEdt, passswordEdt;
        emailEdt = dialog.findViewById(R.id.emailEdt);
        passswordEdt = dialog.findViewById(R.id.passwordEdt);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (dashboardpager.getCurrentItem() == 0) {
                    dashImg.setImageResource(R.drawable.home_ic);
                    dashTxt.setTextColor(context.getResources().getColor(R.color.blue));
                } else {
                    appointmentImg.setImageResource(R.drawable.search_blue_ic);
                    appointmentTxt.setTextColor(context.getResources().getColor(R.color.blue));
                }

            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(emailEdt, passswordEdt);
            }
        });

        signupTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                n.gotoN(-1, "SignUp","");
            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

    }

    private void changeIcAndColor(int i) {
        switch (i) {
            case 0:
                dashboardpager.setCurrentItem(0);
                dashImg.setImageResource(R.drawable.home_ic);
                dashTxt.setTextColor(context.getResources().getColor(R.color.blue));
                HomeScreen.toolTitle.setText(HOME_TEXT);
                break;
            case 1:
                dashboardpager.setCurrentItem(1);
                appointmentImg.setImageResource(R.drawable.search_blue_ic);
                appointmentTxt.setTextColor(context.getResources().getColor(R.color.blue));
                HomeScreen.toolTitle.setText("Search Services");
                break;
            case 2:
                dashboardpager.setCurrentItem(2);
                clientImg.setImageResource(R.drawable.appointments_ic);
                clientTxt.setTextColor(context.getResources().getColor(R.color.blue));
                HomeScreen.toolTitle.setText("My Appointments");
                break;
            case 3:
                dashboardpager.setCurrentItem(3);
                accImg.setImageResource(R.drawable.accounts_ic);
                accTxt.setTextColor(context.getResources().getColor(R.color.blue));
                HomeScreen.toolTitle.setText(PROFILE_TEXT);
                break;
        }
    }

    public void changeFragment(Fragment f, int i, String toolTitle, String tag) {

        if (f instanceof Service_Details)
            HomeScreen.setInstance(f);

        ft = getFragmentManager().beginTransaction();

        switch (i) {
            case 0:
                ft.replace(R.id.homeFrame, f);
                HOME_TEXT = toolTitle;
                break;
            case 1:
                ft.replace(R.id.searchFrame, f);
                break;
            case 2:
                ft.replace(R.id.appointmentFrame, f);
                break;
            case 3:
                ft.replace(R.id.userprofileFrame, f);
                PROFILE_TEXT = toolTitle;
                break;
            case 4:
                ft.replace(R.id.homeFrame, f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
                HomeScreen.toolTitle.setText(toolTitle);
                HOME_TEXT = toolTitle;
                dashboardpager.setCurrentItem(0);
                break;
        }

        if (i != 4) {
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(tag);
            ft.commit();
            HomeScreen.toolTitle.setText(toolTitle);
        }
    }

    public void logout() {

        if (!preferences.getProviderLogin())
            preferences.setLogin(false);
        preferences.logoutUser();
        enableLogin();
        if (preferences.getLogin()) {
            if (preferences.getUserLogin())
                HomeScreen.loginTxt.setText("Normal User");
            else
                HomeScreen.loginTxt.setText("Normal User");
        } else
            HomeScreen.loginTxt.setText("Sell Your Services");

        HomeScreen.toolTitle.setText(YOUR_LOCATION);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, new User());
        fragmentTransaction.commitAllowingStateLoss();
    }


    public interface gotoNext {
        public void gotoN(int position, String type,String fro);
    }


    /*public void goToSecondFragment() {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment secondFragment = new Search_All_Services();
        ft.replace(R.id.frame, secondFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }*/

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void validate(CustomEdittextRegular emailEdt, CustomEdittextRegular passswordEdt) {

        if (!validateEmail(emailEdt)) {
            return;
        }
        if (!validatePassword(passswordEdt)) {
            return;
        }
        if (connectionDetector.isNetworkAvailable()) {
            login(emailEdt, passswordEdt);
        } else
            connectionDetector.error_Dialog();
    }

    private boolean validateEmail(CustomEdittextRegular emailEdt) {
        String e = emailEdt.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        } else if (!isValidEmail(e)) {
            Toast.makeText(context, "Please enter valid email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        }
        return true;
    }

    private boolean validatePassword(CustomEdittextRegular passswordEdt) {
        if (passswordEdt.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter your password", Toast.LENGTH_SHORT).show();
            requestFocus(passswordEdt);
            return false;
        } else if (passswordEdt.getText().toString().trim().length() < 4) {
            Toast.makeText(context, "Please enter valid password", Toast.LENGTH_SHORT).show();
            requestFocus(passswordEdt);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

   /* public void login(final CustomEdittextRegular emailEdt, final CustomEdittextRegular passswordEdt) {
        progDialog.ProgressDialogStart();
        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST, API.LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progDialog.ProgressDialogStop();
                System.out.println(response);
                try {
                    JSONObject newResponse = new JSONObject(response);
                    if (newResponse.getString("status").equals("1")) {
                        JSONObject jsonObject = newResponse.getJSONObject("user_details");
                        preferences.setClientID(jsonObject.getString("id"));
                        dashImg.setImageResource(R.drawable.home_ic);
                        dashTxt.setTextColor(context.getResources().getColor(R.color.blue));
                        preferences.setLogin(true);
                        preferences.setUserLogin(true);
                        preferences.lastLogin("user");
                        disableLogin();
                        if (dialog.isShowing())
                            dialog.dismiss();
                    } else {
                        Toast.makeText(context, newResponse.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                final String eml = emailEdt.getText().toString();
                final String psd = passswordEdt.getText().toString();
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_email", eml);
                params.put("user_password", psd);
                params.put("role", "2");
                params.put("device_type", "1");
                params.put("device_token", "123");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjRequest);
    }*/

    private void login(final CustomEdittextRegular emailEdt, final CustomEdittextRegular passswordEdt) {
        progDialog.ProgressDialogStart();
        JSONObject params = new JSONObject();
        try {
            final String eml = emailEdt.getText().toString();
            final String psd = passswordEdt.getText().toString();
            params.accumulate("user_email", eml);
            params.accumulate("user_password", psd);
            params.accumulate("role", "2");
            params.accumulate("device_type", "1");
            params.accumulate("device_token", "123");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        json j = new json(API.LOGIN, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
                System.out.println(response.toString());
                try {
                    if (response.getString("status").equals("1")) {
                        JSONObject jsonObject = response.getJSONObject("user_details");
                        preferences.setClientID(jsonObject.getString("id"));
                        dashImg.setImageResource(R.drawable.home_ic);
                        dashTxt.setTextColor(context.getResources().getColor(R.color.blue));
                        preferences.setLogin(true);
                        preferences.setUserLogin(true);
                        preferences.lastLogin("user");
                        disableLogin();
                        if (dialog.isShowing())
                            dialog.dismiss();
                    } else {
                        Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                System.out.println(error.toString());
            }
        });
        AppController.getInstance().addToRequestQueue(j);
    }

}
