package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appyvet.materialrangebar.RangeBar;
import com.ondemandsalon.Adapter.CategoryAdapter;
import com.ondemandsalon.Adapter.ShopAdapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.ClickListener;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Shops;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.utils.UtilDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;

/**
 * A simple {@link Fragment} subclass.
 */
public class Search_All_Services extends Fragment implements CategoryAdapter.filter_data {

    RecyclerView categoryRecycler, ShopRecycler;
    Context context;
    ArrayList<Sub_cat> categoryList;
    ArrayList<Shops> shopsList, filterList, tempList;
    List<String> cities_list, ctid_list, location_list, loct_id, newloclist, main_category, sub_category, countries_list;
    LinearLayout main_layout;

    RangeBar rangeBar;
    CustomTextviewRegular minValue, maxValue, notj;
    int minvalue = 0, maxvalue = 0;
    HomeNew h;
    UtilDialog dialog;
    RelativeLayout country, city, location, note;
    String category_id;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;
    Bundle bundle;
    RatingBar ratingBar;

    CustomTextviewRegular countryTxt, cityTxt, locationTxt;
    String catNm = "", ctNm = "", areaNm = "", ctId = "0";
    boolean catSelect = false, ctSelect = false, areaSelect = false, ratingSelect = false, priceSelect = false;
    float fRatings = 0f;
    int minVal = 0, maxVal = 0;

    public Search_All_Services() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Search_All_Services(HomeNew home, String id) {
        h = home;
        this.category_id = id;
    }

    /*@SuppressLint("ValidFragment")
    public Search_All_Services(User home, Bundle bundle) {
        h = home;
        this.bundle = bundle;
        getSearchRelatedSalon(bundle);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.search_all_services, container, false);

        dialog = new UtilDialog(context);
        categoryRecycler = v.findViewById(R.id.categoryRecycle);
        ShopRecycler = v.findViewById(R.id.shopRecycler);
        note = v.findViewById(R.id.notev);

        rangeBar = v.findViewById(R.id.priceFilter);
        main_layout = v.findViewById(R.id.Main_layout);

        minValue = v.findViewById(R.id.minRange);
        maxValue = v.findViewById(R.id.maxRange);

        country = v.findViewById(R.id.countryRel);
        city = v.findViewById(R.id.cityRel);
        location = v.findViewById(R.id.locationRel);

        countryTxt = v.findViewById(R.id.countryTxt);
        cityTxt = v.findViewById(R.id.cityTxt);
        locationTxt = v.findViewById(R.id.locationTxt);
        notj = v.findViewById(R.id.notej);
        ratingBar = v.findViewById(R.id.ratingBar2);

        getCategoryRelatedSalon(category_id);

        country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.listBottomDialog(countryTxt, countries_list,/*Arrays.asList(getResources().getStringArray(R.array.country))*/new ClickListener() {
                    @Override
                    public void onSelect(String s) {
                    }
                });
            }
        });

        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.listBottomDialog(cityTxt, cities_list, /*Arrays.asList(getResources().getStringArray(R.array.city))*/new ClickListener() {
                    @Override
                    public void onSelect(String s) {
                        ctNm = s;
                        int pos = cities_list.indexOf(s);
                        ctId = ctid_list.get(pos);
                        newloclist = new ArrayList<>();
                        locationTxt.setText("All");
                        newloclist.add("All");
                        for (int i = 0; i < loct_id.size(); i++) {
                            if (ctId.equals(loct_id.get(i)))
                                newloclist.add(location_list.get(i));
                        }
                        areaNm = "";
                        filterShops(0);
                    }
                });
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ctNm.equals("")) {
                    if (!ctNm.equals("All")) {
                        dialog.listBottomDialog(locationTxt, newloclist, new ClickListener() {
                            @Override
                            public void onSelect(String s) {
                                areaNm = s;
                                filterShops(0);
                            }
                        });
                    } else {
                        Toast.makeText(context, "Please select city...", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    Toast.makeText(context, "Please select city...", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        });

        rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                minValue.setText(leftPinValue);
                maxValue.setText(rightPinValue);
                minVal = Integer.valueOf(leftPinValue);
                maxVal = Integer.valueOf(rightPinValue);
                filterShops(0);
            }
        });


        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                fRatings = v;
                filterShops(0);
            }
        });

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    ((HomeNew)context).getSupportFragmentManager().popBackStack();
                    HomeNew.HOME_TOOL = YOUR_LOCATION;
                    HomeNew.toolTitle.setText(YOUR_LOCATION);
                    return true;
                }
                return false;
            }
        });
        return v;
    }

    private void filterShops(int i) {

        switch (i) {
            case 0:
                if (shopsList != null) {
                    if (shopsList.size() != 0) {
                        filterList = new ArrayList<>();
                        if (!catNm.equals("")) {
                            filterList = getFilterList(shopsList, 0);
                            filterShops(1);
                        } else {
                            filterList = shopsList;
                            filterShops(1);
                        }
                    } else
                        filterShops(5);
                } else
                    filterShops(5);
                break;
            case 1:
                if (filterList != null) {
                    if (filterList.size() != 0) {
                        if (fRatings != 0) {
                            filterList = getFilterList(filterList, 1);
                            filterShops(2);
                        } else
                            filterShops(2);
                    } else
                        filterShops(5);
                } else
                    filterShops(5);
                break;
            case 2:
                if (filterList != null) {
                    if (filterList.size() != 0) {
                        if (maxVal != 0 && minVal != 0) {
                            filterList = getFilterList(filterList, 2);
                            filterShops(3);
                        } else
                            filterShops(3);
                    } else
                        filterShops(5);
                } else
                    filterShops(5);
                break;
            case 3:
                if (filterList != null) {
                    if (filterList.size() != 0) {
                        if (!ctNm.equals("")) {
                            if (!ctNm.equals("All")) {
                                filterList = getFilterList(filterList, 3);
                                filterShops(4);
                            } else
                                filterShops(5);
                        } else
                            filterShops(5);
                    } else
                        filterShops(5);
                } else
                    filterShops(5);
                break;
            case 4:
                if (filterList != null) {
                    if (filterList.size() != 0) {
                        if (!areaNm.equals("")) {
                            if (!areaNm.equals("All")) {
                                filterList = getFilterList(filterList, 4);
                                filterShops(5);
                            } else
                                filterShops(5);
                        } else
                            filterShops(5);
                    } else
                        filterShops(5);
                } else
                    filterShops(5);
                break;
            case 5:
                if (filterList != null) {
                    if (filterList.size() != 0) {
                        notj.setVisibility(View.GONE);
                        ShopRecycler.setVisibility(View.VISIBLE);
                        ShopRecycler.setAdapter(new ShopAdapter(filterList, h, "shops", context, "search_all_service"));
                        ShopRecycler.setLayoutManager(new LinearLayoutManager(context));

                    } else {

                        notj.setVisibility(View.VISIBLE);
                        ShopRecycler.setVisibility(View.GONE);

                    }
                } else {

                    notj.setVisibility(View.VISIBLE);
                    ShopRecycler.setVisibility(View.GONE);

                }
                break;
        }

    }

    private ArrayList<Shops> getFilterList(ArrayList<Shops> shopsList, int j) {
        int i;
        tempList = new ArrayList<>();
        switch (j) {
            case 0:
                for (i = 0; i < shopsList.size(); i++) {
                    Shops shops = shopsList.get(i);
                    ArrayList<Sub_cat> sub = shops.getSalon_sub_cat();
                    if (sub.size() != 0) {
                        for (int k = 0; k < sub.size(); k++) {
                            Sub_cat sub_cat = sub.get(k);
                            if (sub_cat.getSub_cat_name().equals(catNm)) {
                                tempList.add(shops);
                                break;
                            }
                        }
                    }
                }
                if (tempList.size() != 0)
                    return tempList;
                else
                    return null;
            case 1:
                for (i = 0; i < shopsList.size(); i++) {
                    Shops shops = shopsList.get(i);
                    if (fRatings == Float.parseFloat(shops.getSalon_rating()))
                        tempList.add(shops);
                }
                if (tempList.size() != 0)
                    return tempList;
                else
                    return null;
            case 2:
                for (i = 0; i < shopsList.size(); i++) {
                    Shops shops = shopsList.get(i);
                    if (minVal <= Integer.parseInt(shops.getSalon_min_price()) && maxVal >= Integer.parseInt(shops.getSalon_max_price()))
                        tempList.add(shops);
                }
                if (tempList.size() != 0)
                    return tempList;
                else
                    return null;
            case 3:
                for (i = 0; i < shopsList.size(); i++) {
                    Shops shops = shopsList.get(i);
                    if (ctNm.equals(shops.getSalon_city()))
                        tempList.add(shops);
                }
                if (tempList.size() != 0)
                    return tempList;
                else
                    return null;
            case 4:
                for (i = 0; i < shopsList.size(); i++) {
                    Shops shops = shopsList.get(i);
                    if (areaNm.equals(shops.getSalon_location()))
                        tempList.add(shops);
                }
                if (tempList.size() != 0)
                    return tempList;
                else
                    return null;
            default:
                return shopsList;
        }
    }

    private void getCategoryRelatedSalon(String category_id) {
        JSONObject object = new JSONObject();
        progDialog.ProgressDialogStart();
        try {
            object.accumulate("cat_id", category_id);

            final json json = new json(API.LIST_OF_SALONS_BASED_ON_HOME_CATEGORY, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            main_layout.setVisibility(View.VISIBLE);
                            note.setVisibility(View.GONE);
                            categoryList = new ArrayList<>();
                            shopsList = new ArrayList<>();
                            countries_list = new ArrayList<>();
                            cities_list = new ArrayList<>();
                            ctid_list = new ArrayList<>();
                            location_list = new ArrayList<>();
                            loct_id = new ArrayList<>();
                            main_category = new ArrayList<>();
                            sub_category = new ArrayList<>();

                            JSONArray cty = response.getJSONArray("cities");
                            JSONArray cuntry = response.getJSONArray("countries");
                            JSONArray lcns = response.getJSONArray("Location");

                            if (cty.length() != 0) {
                                cities_list.add("All");
                                ctid_list.add("0");
                                cityTxt.setText("All");
                                for (int i = 0; i < cty.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) cty.get(i);
                                    cities_list.add(jsonObject.getString("city_name"));
                                    ctid_list.add(jsonObject.getString("id"));
                                }
                            }

                            if (cuntry.length() != 0) {
                                for (int i = 0; i < cuntry.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) cuntry.get(i);
                                    countries_list.add(jsonObject.getString("country_name"));
                                    if (i == 0) {
                                        countryTxt.setText(jsonObject.getString("country_name"));
                                    }
                                }
                            }

                            if (lcns.length() != 0) {

                                locationTxt.setText("All");
                                for (int i = 0; i < lcns.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) lcns.get(i);
                                    location_list.add(jsonObject.getString("loc_name"));
                                    loct_id.add(jsonObject.getString("city_id"));
                                }
                            }

                            JSONArray sub_categories = response.getJSONArray("category_services_details");
                            if (sub_categories.length() != 0) {
                                categoryList.add(new Sub_cat("All", "", ""));
                                for (int i = 0; i < sub_categories.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) sub_categories.get(i);
                                    String cat_name = jsonObject.getString("ser_cat_name");
                                    String cat_id = jsonObject.getString("ser_cat_id");
                                    String parent_cat_id = jsonObject.getString("ser_cat_parent");
                                    categoryList.add(new Sub_cat(cat_name, cat_id, parent_cat_id));
                                }
                                categoryRecycler.setAdapter(new CategoryAdapter(context, categoryList, Search_All_Services.this));
                                categoryRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            }
                            JSONArray filtered_shops = response.getJSONArray("shops_details");
                            if (filtered_shops.length() != 0) {
                                for (int i = 0; i < filtered_shops.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) filtered_shops.get(i);
                                    String salon_id = jsonObject.getString("id");
                                    String salon_image = jsonObject.getString("banner_name");
                                    String salon_name = jsonObject.getString("business_name");
                                    String salon_location = jsonObject.getString("user_location");
                                    String salon_city = jsonObject.getString("city");
                                    String salon_rating = jsonObject.getString("rat_out_of_5");
                                    String salon_total_reviews = jsonObject.getString("total_review");
                                    String salon_happy_customers = jsonObject.getString("happy_performance");
                                    String salon_min_price = jsonObject.getString("salon_min_price");
                                    String salon_max_price = jsonObject.getString("salon_max_price");

                                    if (i == 0) {
                                        minvalue = Integer.parseInt(salon_min_price);
                                        maxvalue = Integer.parseInt(salon_max_price);
                                        if (maxvalue == minvalue) {
                                            rangeBar.setTickEnd(maxvalue + 10);
                                            rangeBar.setTickStart(minvalue);
                                        } else {
                                            rangeBar.setTickEnd(maxvalue);
                                            rangeBar.setTickStart(minvalue);
                                        }
                                    }

                                    if (minvalue > Integer.parseInt(salon_min_price)) {
                                        minvalue = Integer.parseInt(salon_min_price);
                                        minValue.setText(minvalue);
                                        rangeBar.setTickStart(minvalue);
                                    }

                                    if (Integer.parseInt(salon_max_price) > maxvalue) {
                                        maxvalue = Integer.parseInt(salon_max_price);
                                        maxValue.setText(maxvalue);
                                        rangeBar.setTickEnd(maxvalue);
                                    }


                                    ArrayList<Sub_cat> salon_sub_cat = new ArrayList<>();
                                    JSONArray sub_cat = jsonObject.getJSONArray("salon_sub_category_services");
                                    if (sub_cat.length() != 0) {
                                        for (int j = 0; j < sub_cat.length(); j++) {
                                            JSONObject jsonObject1 = (JSONObject) sub_cat.get(j);
                                            String sub_cat_name = jsonObject1.getString("ser_cs_name");
                                            String sub_cat_id = jsonObject1.getString("ser_cs_id");
                                            salon_sub_cat.add(new Sub_cat(sub_cat_name, sub_cat_id));
                                        }
                                    }
                                    shopsList.add(new Shops(salon_id, salon_image, salon_name, salon_location, salon_city, salon_rating, salon_total_reviews, salon_happy_customers, salon_min_price, salon_max_price, "UAE", salon_sub_cat));
                                }
                            }
                            notj.setVisibility(View.GONE);
                            ShopRecycler.setVisibility(View.VISIBLE);
                            ShopRecycler.setAdapter(new ShopAdapter(shopsList, h, "shops", context, "search_all_service"));
                            ShopRecycler.setLayoutManager(new LinearLayoutManager(context));
                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            main_layout.setVisibility(View.GONE);
                            note.setVisibility(View.VISIBLE);
//                            h.changeFragment(new User_Home(h), 0, YOUR_LOCATION, YOUR_LOCATION);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    Toast.makeText(context, "Somthing wents wrong", Toast.LENGTH_SHORT).show();
                    main_layout.setVisibility(View.GONE);
                    note.setVisibility(View.VISIBLE);
                }
            });
            AppController.getInstance().addToRequestQueue(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        preferences = new Preferences(context);
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    private void getSearchRelatedSalon(Bundle bundle) {
        String country = bundle.getString("country");
        String city = bundle.getString("city");
        String location = bundle.getString("location");
        String cat_id = bundle.getString("cat_id");
        String category_service_id = bundle.getString("category_service_id");
        String rating = bundle.getString("rating");
        String min_price = bundle.getString("min_price");
        String max_price = bundle.getString("max_price");
        JSONObject object = new JSONObject();
        progDialog.ProgressDialogStart();
        try {
            object.accumulate("country", country);
            object.accumulate("city", city);
            object.accumulate("location", location);
            object.accumulate("cat_id", cat_id);
            object.accumulate("category_service_id", category_service_id);
            object.accumulate("rating", rating);
            object.accumulate("min_price", min_price);
            object.accumulate("max_price", max_price);

            final json json = new json(API.POST_SEARCH_BASIC_INFO, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            main_layout.setVisibility(View.VISIBLE);
                            shopsList = new ArrayList<>();

                            JSONArray filtered_shops = response.getJSONArray("search_data");
                            if (filtered_shops.length() != 0) {
                                for (int i = 0; i < filtered_shops.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) filtered_shops.get(i);
                                    String salon_id = jsonObject.getString("id");
                                    String salon_image = jsonObject.getString("banner_name");
                                    String salon_name = jsonObject.getString("business_name");
                                    String salon_location = jsonObject.getString("user_location");
                                    String salon_city = jsonObject.getString("city");
                                    String salon_rating = jsonObject.getString("rat_out_of_5");
                                    String salon_total_reviews = jsonObject.getString("total_review");
                                    String salon_happy_customers = jsonObject.getString("happy_performance");

                                    shopsList.add(new Shops(salon_id, salon_image, salon_name, salon_location, salon_city, salon_rating, salon_total_reviews, salon_happy_customers));
                                }
                            }
                            ShopRecycler.setAdapter(new ShopAdapter(shopsList, h, "shops", context, "search_all_service"));
                            ShopRecycler.setLayoutManager(new LinearLayoutManager(context));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                }
            });
            AppController.getInstance().addToRequestQueue(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void filter(String sub_cat_name, String sub_cat_id) {
        if (!sub_cat_name.equals("All")) {

            catNm = sub_cat_name;
            filterShops(0);

        } else {
            catNm = "";
            filterShops(0);
        }
    }
}
