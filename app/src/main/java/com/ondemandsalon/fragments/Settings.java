package com.ondemandsalon.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.ClickListener;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.User_Setting_Model;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTextviewSemiBold;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.utils.UtilDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;
import static com.ondemandsalon.helper.CONSTANTS.PROFILE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends Fragment {

    CustomTextviewSemiBold notTitle, proxTitle;
    Switch notSwitch;
    CustomTextviewRegular bookingTitle, proxValue;
    RelativeLayout proxyrel;

    ProgDialog dialog;
    ConnectionDetector connectionDetector;
    Context c;
    ArrayList<User_Setting_Model> proxyList;
    List<String> proxyNames;

    UtilDialog utilDialog;
    Preferences preferences;


    public Settings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        notTitle = view.findViewById(R.id.notTitle);
        proxTitle = view.findViewById(R.id.proxTitle);
        notSwitch = view.findViewById(R.id.reminderSwith);
        bookingTitle = view.findViewById(R.id.bookingTitle);
        proxValue = view.findViewById(R.id.proxValue);
        proxyrel = view.findViewById(R.id.proxyRel);

        getSettings();

        proxyrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilDialog.listBottomDialog(proxValue, proxyNames, new ClickListener() {
                    @Override
                    public void onSelect(String s) {
                        int pos = proxyNames.indexOf(s);
                        changeValue(proxyList.get(pos).getSet_answer_que_id(), proxyList.get(pos).getSet_ans_id());
                    }
                });
            }
        });

        notSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    changeValue("1", "1");
                else
                    changeValue("1", "0");
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_0);
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private void changeValue(String qid, String aid) {

        if (connectionDetector.isNetworkAvailable()) {
            dialog.ProgressDialogStart();
            JSONObject param = new JSONObject();

            try {


                JSONArray quetion_answer_list = new JSONArray();

                JSONObject listObj = new JSONObject();

                listObj.accumulate("question_id", qid);
                listObj.accumulate("answer_id", aid);

                quetion_answer_list.put(listObj);

                param.accumulate("quetion_answer_list", quetion_answer_list);
                param.accumulate("user_id", preferences.getClientID());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            json j = new json(API.POST_USER_ACCOUNT_SETTINGS, param, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    dialog.ProgressDialogStop();
                    System.out.println(response);

                    try {
                        Toast.makeText(c, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println(error.toString());
                    dialog.ProgressDialogStop();
                    Toast.makeText(c, "Please try again...", Toast.LENGTH_SHORT).show();
                }
            });

            AppController.getInstance().addToRequestQueue(j);
        } else
            connectionDetector.error_Dialog();

    }

    private void getSettings() {
        if (connectionDetector.isNetworkAvailable()) {
            dialog.ProgressDialogStart();

            json j = new json(API.GET_USER_ACCOUNT_SETTINGS, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    dialog.ProgressDialogStop();
                    System.out.println(response);

                    try {
                        if (response.getInt("status") == 1) {
                            JSONArray qdata = response.getJSONArray("question_data");

                            if (qdata.length() != 0) {
                                JSONObject obj = qdata.getJSONObject(0);

                                notTitle.setText(obj.getString("set_que_name"));

                                JSONArray answer_list = obj.getJSONArray("answer_list");

                                JSONObject listObj = answer_list.getJSONObject(0);

                                bookingTitle.setText(listObj.getString("set_answer_list"));

                                JSONObject obj1 = qdata.getJSONObject(1);

                                proxTitle.setText(obj1.getString("set_que_name"));

                                JSONArray answer_list1 = obj1.getJSONArray("answer_list");

                                proxyList = new ArrayList<User_Setting_Model>();
                                proxyNames = new ArrayList<String>();

                                for (int i = 0; i < answer_list1.length(); i++) {
                                    JSONObject listobj1 = answer_list1.getJSONObject(i);
                                    proxyNames.add(listobj1.getString("set_answer_list"));
                                    proxyList.add(new User_Setting_Model(listobj1.getString("set_ans_id"), listobj1.getString("set_answer_list"), listobj1.getString("set_answer_que_id")));
                                }
                            }
                        } else {

                            dialog.ProgressDialogStop();
                            Toast.makeText(c, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    dialog.ProgressDialogStop();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else
            connectionDetector.error_Dialog();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        c = context;
        utilDialog = new UtilDialog(context);
        dialog = new ProgDialog(context);
        preferences = new Preferences(context);
        connectionDetector = new ConnectionDetector(context);
    }
}
