package com.ondemandsalon.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.FontFamily;
import com.ondemandsalon.utils.UtilDialog;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;

public class Account_settings extends Fragment implements AdapterView.OnItemSelectedListener {
    Context context;
    CustomTextviewRegular one_month, three_month;
    FontFamily font;
    UtilDialog dialog;

    RelativeLayout saveRel,backrel;

    public Account_settings() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_setting, container, false);
        one_month = view.findViewById(R.id.one_hour_txt);
        three_month = view.findViewById(R.id.three_months_txt);

        one_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.openListDialog("Client can book Appointment", context.getResources().getStringArray(R.array.one_hour_array), one_month);
            }
        });

        three_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.openListDialog("Client can book Appointment", context.getResources().getStringArray(R.array.three_month_array), three_month);
            }
        });

        saveRel = view.findViewById(R.id.saveRel);
        backrel = view.findViewById(R.id.backRel);

        saveRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeNew)context).getSupportFragmentManager().popBackStack();
                HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
            }
        });

        backrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeNew)context).getSupportFragmentManager().popBackStack();
                HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    ((HomeNew)context).getSupportFragmentManager().popBackStack();
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                    return true;
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        font = new FontFamily(context);
        dialog = new UtilDialog(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
        ((TextView) adapterView.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        ((TextView) adapterView.getChildAt(0)).setTypeface(font.titillium_regular);
        ((TextView) adapterView.getChildAt(0)).setEllipsize(TextUtils.TruncateAt.END);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
