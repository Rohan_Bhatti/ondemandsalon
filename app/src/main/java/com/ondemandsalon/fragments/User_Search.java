package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appyvet.materialrangebar.RangeBar;
import com.ondemandsalon.Adapter.ShopAdapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.helper.ClickListener;
import com.ondemandsalon.model.Shops;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.utils.UtilDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class User_Search extends Fragment {

    RelativeLayout search, country, city, location, parent_cat, child_cat, search_result_layout;
    RangeBar rangeBar;
    CustomTextviewRegular minValue, maxValue, countryTxt, cityTxt, locationTxt, parent_cat_txt, child_cat_txt;
    HomeNew h;
    UtilDialog dialog;
    LinearLayout main_lay_of_search;
    ArrayList<Shops> shopsList;
    RecyclerView ShopRecycler;
    Context context;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    List<String> cities_list, ctid_list, countries_list, location_list, loct_id, newloclist;
    ArrayList<Shops> total_main_categories;
    ArrayList<Sub_cat> main_sub_categories;
    ArrayList<String> m_categories;
    ArrayList<String> m_s_categories;
    RatingBar ratingBar;

    String catNm = "", ctNm = "", areaNm = "", ctId = "0";

    public User_Search() {
    }

    @SuppressLint("ValidFragment")
    public User_Search(HomeNew home) {
        // Required empty public constructor
        h = home;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.user_search, container, false);

        dialog = new UtilDialog(container.getContext());

        search = v.findViewById(R.id.serchRel);
        country = v.findViewById(R.id.countryRel);
        ShopRecycler = v.findViewById(R.id.shopRecycler);
        city = v.findViewById(R.id.cityRel);
        location = v.findViewById(R.id.locationRel);
        search_result_layout = v.findViewById(R.id.search_result_lay);
        main_lay_of_search = v.findViewById(R.id.main_search_lay);

        countryTxt = v.findViewById(R.id.countrytxt);
        cityTxt = v.findViewById(R.id.cityTxt);
        locationTxt = v.findViewById(R.id.locationTxt);
        rangeBar = v.findViewById(R.id.priceFilter);

        minValue = v.findViewById(R.id.minRange);
        maxValue = v.findViewById(R.id.maxRange);

        parent_cat = v.findViewById(R.id.parent);
        child_cat = v.findViewById(R.id.child);
        parent_cat_txt = v.findViewById(R.id.parent_cat_txt);
        child_cat_txt = v.findViewById(R.id.child_cat_txt);


        ratingBar = v.findViewById(R.id.searchRatingBar);

        get_search_basic_info();

        rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                minValue.setText(leftPinValue);
                maxValue.setText(rightPinValue);
            }
        });

        parent_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (m_categories != null) {
                    String[] mStringArray = new String[m_categories.size()];
                    mStringArray = m_categories.toArray(mStringArray);
                    dialog.openListWithoutDialog(mStringArray, parent_cat_txt);
                }
            }
        });

        parent_cat_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String txt = editable.toString();
                for (int i = 0; i < m_categories.size(); i++) {
                    Shops shops = total_main_categories.get(i);
                    if (shops.getCat_name().equals(txt)) {
                        ArrayList<Sub_cat> salon_sub_cat = shops.getSalon_sub_cat();
                        m_s_categories = new ArrayList<>();
                        for (int j = 0; j < salon_sub_cat.size(); j++) {
                            Sub_cat sub_cat = salon_sub_cat.get(j);
                            if (j == 0) {
                                child_cat_txt.setText(sub_cat.getSub_cat_name());
                            }
                            m_s_categories.add(sub_cat.getSub_cat_name());
                        }
                        return;
                    }
                }
            }
        });

        child_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] mStringArray = new String[m_s_categories.size()];
                mStringArray = m_s_categories.toArray(mStringArray);
                dialog.openListWithoutDialog(mStringArray, child_cat_txt);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });

        country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.listBottomDialog(countryTxt, countries_list, new ClickListener() {
                    @Override
                    public void onSelect(String s) {
                    }
                });
//                dialog.listBottomDialog(countryTxt, Arrays.asList(getResources().getStringArray(R.array.country)));
//                dialog.openListDialog("Select Country", getResources().getStringArray(R.array.country), countryTxt);
            }
        });

        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.listBottomDialog(cityTxt, cities_list, new ClickListener() {
                    @Override
                    public void onSelect(String s) {
                        ctNm = s;
                        int pos = cities_list.indexOf(s);
                        ctId = ctid_list.get(pos);
                        newloclist = new ArrayList<>();
                        locationTxt.setText("All");
                        newloclist.add("All");
                        for (int i = 0; i < loct_id.size(); i++) {
                            if (ctId.equals(loct_id.get(i)))
                                newloclist.add(location_list.get(i));
                        }

                    }
                });
//                dialog.listBottomDialog(cityTxt, Arrays.asList(getResources().getStringArray(R.array.city)));
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.listBottomDialog(locationTxt, newloclist, new ClickListener() {
                    @Override
                    public void onSelect(String s) {
                        if (!ctNm.equals("")) {
                            if (!ctNm.equals("All")) {
                                dialog.listBottomDialog(locationTxt, newloclist, new ClickListener() {
                                    @Override
                                    public void onSelect(String s) {
                                        areaNm = s;
                                    }
                                });
                            } else {
                                Toast.makeText(context, "Please select city...", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } else {
                            Toast.makeText(context, "Please select city...", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }
                });
//                dialog.listBottomDialog(locationTxt, Arrays.asList(getResources().getStringArray(R.array.location)));
            }
        });

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
//                    getFragmentManager().popBackStack();
                 /*   main_lay_of_search.setVisibility(View.VISIBLE);
                    search_result_layout.setVisibility(View.GONE);*/
                    return true;
                }
                return false;
            }
        });
        return v;
    }

    private void get_search_basic_info() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();

            json j = new json(API.GET_SEARCH_BASIC_INFO, new Response.Listener<JSONObject>() {
                @SuppressLint("ResourceType")
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {

                            countries_list = new ArrayList<>();
                            cities_list = new ArrayList<>();
                            ctid_list = new ArrayList<>();
                            location_list = new ArrayList<>();
                            loct_id = new ArrayList<>();

                            String min_price = response.getString("min_price");
                            String max_price = response.getString("max_price");

                            int minvalue = Integer.parseInt(min_price);
                            int maxvalue = Integer.parseInt(max_price);
                            if (maxvalue > minvalue) {
                                minValue.setText(min_price);
                                maxValue.setText(max_price);
                                if (maxvalue == minvalue) {
                                    rangeBar.setTickEnd(maxvalue + 10);
                                    rangeBar.setTickStart(minvalue);
                                } else {
                                    rangeBar.setTickEnd(maxvalue);
                                    rangeBar.setTickStart(minvalue);
                                }
                            } else {
                                minValue.setText("10");
                                maxValue.setText("100");
                                rangeBar.setTickEnd(100);
                                rangeBar.setTickStart(10);
                            }

                            JSONArray cty = response.getJSONArray("city_data");
                            JSONArray cuntry = response.getJSONArray("country_data");
                            JSONArray lcns = response.getJSONArray("location_data");

                           /* if (cty.length() != 0) {
                                for (int i = 0; i < cty.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) cty.get(i);
                                    cities_list.add(jsonObject.getString("city_name"));
                                }
                            }

                            if (cuntry.length() != 0) {
                                for (int i = 0; i < cuntry.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) cuntry.get(i);
                                    countries_list.add(jsonObject.getString("country_name"));
                                }
                            }

                            if (lcns.length() != 0) {
                                for (int i = 0; i < lcns.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) lcns.get(i);
                                    location_list.add(jsonObject.getString("loc_name"));
                                }
                            }
*/
                            if (cty.length() != 0) {
                                cities_list.add("All");
                                ctid_list.add("0");
                                cityTxt.setText("All");
                                for (int i = 0; i < cty.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) cty.get(i);
                                    cities_list.add(jsonObject.getString("city_name"));
                                    ctid_list.add(jsonObject.getString("id"));
                                }
                            }

                            if (cuntry.length() != 0) {
                                for (int i = 0; i < cuntry.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) cuntry.get(i);
                                    countries_list.add(jsonObject.getString("country_name"));
                                    if (i == 0) {
                                        countryTxt.setText(jsonObject.getString("country_name"));
                                    }
                                }
                            }

                            if (lcns.length() != 0) {

                                locationTxt.setText("All");
                                for (int i = 0; i < lcns.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) lcns.get(i);
                                    location_list.add(jsonObject.getString("loc_name"));
                                    loct_id.add(jsonObject.getString("city_id"));
                                }
                            }
                            JSONArray main_categories = response.getJSONArray("category_data");
                            if (main_categories.length() != 0) {
                                total_main_categories = new ArrayList<>();
                                m_categories = new ArrayList<>();
                                for (int i = 0; i < main_categories.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) main_categories.get(i);
                                    String cat_id = jsonObject.getString("ser_id");
                                    String cat_name = jsonObject.getString("ser_name");
                                    m_categories.add(cat_name);
                                    if (jsonObject.has("service_data")) {
                                        JSONArray sub_categories = jsonObject.getJSONArray("service_data");
                                        if (sub_categories.length() != 0) {
                                            main_sub_categories = new ArrayList<>();
                                            m_s_categories = new ArrayList<>();
                                            for (int j = 0; j < sub_categories.length(); j++) {
                                                JSONObject jsonObject1 = (JSONObject) sub_categories.get(j);
                                                String sub_cat_id = jsonObject1.getString("ser_cat_id");
                                                String sub_cat_name = jsonObject1.getString("ser_cat_name");
                                                if (i == 0) {
                                                    m_s_categories.add(sub_cat_name);
                                                    if (j == 0) {
                                                        child_cat_txt.setText(sub_cat_name);
                                                    }
                                                }
                                                main_sub_categories.add(new Sub_cat(sub_cat_name, sub_cat_id));
                                            }
                                        }
                                        total_main_categories.add(new Shops(cat_id, cat_name, main_sub_categories));
                                    }
                                    if (i == 0) {
                                        parent_cat_txt.setText(cat_name);
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    System.out.println(error.toString());
                    Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void getServiceProvider() {
        progDialog.ProgressDialogStart();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("ser_parent_id", "");
            jsonObject.accumulate("ser_child_id", "");
            jsonObject.accumulate("curr_location", locationTxt.getText().toString());
            jsonObject.accumulate("curr_time", Calendar.getInstance().getTimeInMillis());
            jsonObject.accumulate("curr_city", cityTxt.getText().toString());
            jsonObject.accumulate("curr_mex", maxValue.getText().toString());
            jsonObject.accumulate("curr_min", minValue.getText().toString());

            json json = new json(API.SEARCH_SERVICE_PROVIDER_USING_REQUIRED_FIELD_USER, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.get("status").equals("1")) {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                }
            });
            AppController.getInstance().addToRequestQueue(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void validate() {
        if (countryTxt.getText().toString().equals("Country")) {
            Toast.makeText(context, "Please select country", Toast.LENGTH_SHORT).show();
            return;
        }

       /* if (cityTxt.getText().toString().equals("City")) {
            Toast.makeText(context, "Please select city", Toast.LENGTH_SHORT).show();
            return;
        }

        if (locationTxt.getText().toString().equals("Location")) {
            Toast.makeText(context, "Please select location", Toast.LENGTH_SHORT).show();
            return;
        }
*/
        if (connectionDetector.isNetworkAvailable()) {
            Bundle bundle = new Bundle();
            bundle.putString("country", countryTxt.getText().toString());
            bundle.putString("city", cityTxt.getText().toString());
            bundle.putString("location", locationTxt.getText().toString());
            bundle.putString("cat_id", cat_id(parent_cat_txt.getText().toString()));
            bundle.putString("category_service_id", sub_cat_id(parent_cat_txt.getText().toString(), child_cat_txt.getText().toString()));
            bundle.putString("rating", String.valueOf(ratingBar.getRating()));
            bundle.putString("min_price", minValue.getText().toString());
            bundle.putString("max_price", maxValue.getText().toString());

//            h.changeFragment(new Search_All_Services(h, bundle), 4, ALL_SERVICES, "ALL_SERVICES");

            getSearchRelatedSalon(bundle);
        } else
            connectionDetector.error_Dialog();
    }

    private void getSearchRelatedSalon(Bundle bundle) {
        String country = bundle.getString("country");
        String city = bundle.getString("city");
        String location = bundle.getString("location");
        String cat_id = bundle.getString("cat_id");
        String category_service_id = bundle.getString("category_service_id");
        String rating = bundle.getString("rating");
        String min_price = bundle.getString("min_price");
        String max_price = bundle.getString("max_price");
        JSONObject object = new JSONObject();

        progDialog.ProgressDialogStart();

        try {
            object.accumulate("country", country);
            object.accumulate("city", city);
            object.accumulate("location", location);
            object.accumulate("cat_id", cat_id);
            object.accumulate("category_service_id", category_service_id);
            object.accumulate("rating", rating);
            object.accumulate("min_price", min_price);
            object.accumulate("max_price", max_price);

            /*object.accumulate("country", "UAE");
            object.accumulate("city", "Dubai");
            object.accumulate("location", "Jumeirah");
            object.accumulate("cat_id", "1");
            object.accumulate("category_service_id", "2");
            object.accumulate("rating", "4");
            object.accumulate("min_price", "30");
            object.accumulate("max_price", "30");*/

            final json json = new json(API.POST_SEARCH_BASIC_INFO, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            search_result_layout.setVisibility(View.VISIBLE);
                            main_lay_of_search.setVisibility(View.GONE);
                            shopsList = new ArrayList<>();

                            JSONArray filtered_shops = response.getJSONArray("search_data");
                            if (filtered_shops.length() != 0) {
                                for (int i = 0; i < filtered_shops.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) filtered_shops.get(i);
                                    String salon_id = jsonObject.getString("id");
                                    String salon_image = jsonObject.getString("banner_name");
                                    String salon_name = jsonObject.getString("business_name");
                                    String salon_location = jsonObject.getString("user_location");
                                    String salon_city = jsonObject.getString("city");
                                    String salon_rating = jsonObject.getString("rat_out_of_5");
                                    String salon_total_reviews = jsonObject.getString("total_review");
                                    String salon_happy_customers = jsonObject.getString("happy_performance");

                                    shopsList.add(new Shops(salon_id, salon_image, salon_name, salon_location, salon_city, salon_rating, salon_total_reviews, salon_happy_customers));
                                }
                            }
                            ShopRecycler.setAdapter(new ShopAdapter(shopsList, h, "shops", context, "user_search"));
                            ShopRecycler.setLayoutManager(new LinearLayoutManager(context));
                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String sub_cat_id(String p, String c) {
        for (int i = 0; i < m_categories.size(); i++) {
            Shops shops = total_main_categories.get(i);
            if (shops.getCat_name().equals(p)) {
                ArrayList<Sub_cat> salon_sub_cat = shops.getSalon_sub_cat();
                for (int j = 0; j < salon_sub_cat.size(); j++) {
                    Sub_cat sub_cat = salon_sub_cat.get(j);
                    if (sub_cat.getSub_cat_name().equals(c)) {
                        return sub_cat.getSub_cat_id();
                    }
                }
            }
        }
        return "0";
    }

    private String cat_id(String s) {
        for (int i = 0; i < m_categories.size(); i++) {
            Shops shops = total_main_categories.get(i);
            if (shops.getCat_name().equals(s)) {
                return shops.getCat_id();
            }
        }
        return "0";
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }


}
