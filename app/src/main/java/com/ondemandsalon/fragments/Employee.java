package com.ondemandsalon.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Client_Adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.model.Provider_Client;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;

/**
 * A simple {@link Fragment} subclass.
 */
public class Employee extends Fragment implements HomeNew.ChangeVal {

    RecyclerView clientRecycler;
    Context context;
    HomeNew p;
    ArrayList<Provider_Client> Provider_Clients;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;

    public Employee() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Employee(Context context, HomeNew p) {
        this.p = p;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.employee, container, false);

        clientRecycler = view.findViewById(R.id.clientRecycler);
        Provider_Clients = new ArrayList<>();

        Provider_Clients.add(new Provider_Client("Laura Michelle", "4581889631", "Jabel Ali"));
        Provider_Clients.add(new Provider_Client("Jason Derulo", "4581889631", "Delhi"));
        Provider_Clients.add(new Provider_Client("Smith Ken", "4581889631", "Jabel ali"));
        Provider_Clients.add(new Provider_Client("Kevin Pet", "4581889631", "Goa"));
        Provider_Clients.add(new Provider_Client("Kit Harington", "4581889631", "Dubai"));

        clientRecycler.setAdapter(new Client_Adapter(context, Provider_Clients, "Employee", p));
        clientRecycler.setLayoutManager(new LinearLayoutManager(context));

        if (connectionDetector.isNetworkAvailable()) {
            getClientList();
        } else {
            connectionDetector.error_Dialog();
        }

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack();
                    HomeNew.ACCOUNT_TOOL = HomeNew.ACCOUNT_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.ACCOUNT_LEVEL_0);
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private void getClientList() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("cl_added_by", "1");

                json json = new json(API.CLIENT_LISTING, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {
                                JSONArray jsonArray = response.getJSONArray("client_detail");
                                if (jsonArray.length() != 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void getEmployeeList() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("em_added_by", "1");

                json json = new json(API.EMPLOYEE_LISTING, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {
                                JSONArray jsonArray = response.getJSONArray("employees_detail");
                                if (jsonArray.length() != 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        progDialog = new ProgDialog(context);
        connectionDetector = new ConnectionDetector(context);
    }

    @Override
    public void change(int pos,String fro) {
        p.changeFragment(new Add_employee("new", Provider_Clients.get(0)), 3, "New Employee","N_EMPLOYEE","provider");
    }
}
