package com.ondemandsalon.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.helper.AppHelper;
import com.ondemandsalon.helper.PathUtil;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.helper.VolleyMultipartRequest;
import com.ondemandsalon.model.Service_list;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class User_Edit_Profile extends Fragment {


    private static final String IMAGE_DIRECTORY_NAME = "OnDemandSalon";
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;
    CustomEdittextRegular email, fname, lname, phoneTxt;
    Preferences preferences;
    RelativeLayout updateRel, backrel;
    CircleImageView uimage;
    Context context;
    String ImagePath = null;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    String firstname, lastname, mail, number, urlofprofile;
    private int SELECT_FILE = 0;
    private int REQUEST_CAMERA = 1;
    private Uri fileUri; // file url to store image/video

    public User_Edit_Profile(String fnanme, String lnanme, String mail, String number, String urlofprofile) {
        this.firstname = fnanme;
        this.lastname = lnanme;
        this.mail = mail;
        this.number = number;
        this.urlofprofile = urlofprofile;
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_user__edit__profile, container, false);

        email = v.findViewById(R.id.emailTxt);
        fname = v.findViewById(R.id.fnameTxt);
        lname = v.findViewById(R.id.lnameTxt);
        phoneTxt = v.findViewById(R.id.phoneTxt);
        updateRel = v.findViewById(R.id.update_btn);
        backrel = v.findViewById(R.id.backRel);
        uimage = v.findViewById(R.id.uimage);

        email.setText(mail);
        fname.setText(firstname);
        lname.setText(lastname);
        phoneTxt.setText(number);
        if (!urlofprofile.equals("")) {
            Glide.with(context).load(urlofprofile).into(uimage);
        }

        updateRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                preferences.setData(fname.getText().toString() + " " + lname.getText().toString(), email.getText().toString(), phoneTxt.getText().toString());
                validate();
            }
        });
        backrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                preferences.setData(fname.getText().toString() + " " + lname.getText().toString(), email.getText().toString(), phoneTxt.getText().toString());
                ((HomeNew) context).getSupportFragmentManager().popBackStack();
                HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_0;
                HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_0);
            }
        });

        uimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageDialog();
            }
        });

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                  /*  List<Fragment> list = ((HomeNew) context).getSupportFragmentManager().getFragments();
                    for (int i = list.size() - 1; i >= 0; i--) {
                        Fragment f = list.get(i);
                        if (f instanceof User_Edit_Profile) {
                            ((HomeNew) context).getSupportFragmentManager().getFragments().remove(f);


                            return true;
                        }
                    }*/
                    ((HomeNew) context).getSupportFragmentManager().popBackStack();
                    HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_0;
                    HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_0);

                }
                return false;
            }
        });

        return v;
    }

    private void validate() {
        if (!validateEmail()) {
            return;
        }

        if (fname.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter your first name...", Toast.LENGTH_SHORT).show();
            requestFocus(fname);
            return;
        }

        if (lname.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Please enter your last name...", Toast.LENGTH_SHORT).show();
            requestFocus(lname);
            return;
        }

        String phone = phoneTxt.getText().toString().trim();
        if (phone.isEmpty()) {
            Toast.makeText(context, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            requestFocus(phoneTxt);
            return;
        }

        if (connectionDetector.isNetworkAvailable())
            update_profile();
        else
            connectionDetector.error_Dialog();

    }

    private void update_profile() {
        progDialog.ProgressDialogStart();
        VolleyMultipartRequest request = new VolleyMultipartRequest(Request.Method.POST, API.EDIT_PROFILE, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                progDialog.ProgressDialogStop();
                String resultResponse = new String(response.data);
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    if (result.getString("status").equals("1")) {
                        Toast.makeText(context, "Update profile successfully", Toast.LENGTH_SHORT).show();
                        ((HomeNew) context).getSupportFragmentManager().popBackStack();
                        HomeNew.PROFILE_TOOL = HomeNew.PROFILE_LEVEL_0;
                        HomeNew.toolTitle.setText(HomeNew.PROFILE_LEVEL_0);
                    } else {
                        Toast.makeText(getActivity(), result.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                Toast.makeText(getActivity(), "Something went wrong\nPlease try again", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", preferences.getClientID());
                params.put("user_fname", fname.getText().toString());
                params.put("user_lname", lname.getText().toString());
                params.put("user_phone", phoneTxt.getText().toString().trim());
                params.put("role", "2");
                params.put("device_type", "1");
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                if (ImagePath != null) {
                    params.put("user_avatar", new DataPart(ImagePath, AppHelper.getFileDataFromDrawable(getActivity(), uimage.getDrawable()), "image/jpeg"));
                } else {
                    params.put("user_avatar", new DataPart("", AppHelper.getFileDataFromDrawable(getActivity(), uimage.getDrawable()), "image/jpeg"));
                }
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(request);
    }

    private boolean validateEmail() {
        String e = email.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(context, "Please enter your email...", Toast.LENGTH_SHORT).show();
            requestFocus(email);
            return false;
        } else {
            if (!isValidEmail(e)) {
                Toast.makeText(context, "Please enter valid email...", Toast.LENGTH_SHORT).show();
                requestFocus(email);
                return false;
            }
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void selectImageDialog() {

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            return;
        }

        String[] items = new String[]{"Camera", "Gallery", "Cancel"};
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Select Image From");
        dialog.setCancelable(false);
        dialog.setItems(items, new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        cameraIntent();
                        break;
                    case 1:
                        galleryIntent();
                        break;
                }
            }
        });
        dialog.show();
    }

    private void galleryIntent() {
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(i, REQUEST_CAMERA);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        preferences = new Preferences(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    public Uri getOutputMediaFileUri(int type) {
        return FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", getOutputMediaFile(type));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                setDataOfCamera(data);
                /*
                performCrop(getImageUri(Set_Clinical_Rec_Data.this, b));*/
            } else if (requestCode == SELECT_FILE) {
                onselectFromGalleryResult(data);
            }
        }
    }

    private void setDataOfCamera(Intent data) {
        try {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), fileUri);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                ImagePath = fileUri.getPath();
                System.out.println("Path " + ImagePath);
                if (ImagePath.contains("/external_files")) {
                    ImagePath = ImagePath.replace("/external_files", Environment.getExternalStorageDirectory().getPath());
                }
                uimage.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void onselectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        String path = null;
        try {
            path = PathUtil.getPath(getActivity(), data.getData());
            File imageFile = new File(path);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            if (imageFile.exists()) {
                bm = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


        if (bm != null) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
            ImagePath = path;
            System.out.println("Path " + ImagePath);
            uimage.setImageBitmap(bm);
//            Toast.makeText(Clinical_Records.this, "Image Attached Successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Please Try Again", Toast.LENGTH_SHORT).show();
        }
    }

}
