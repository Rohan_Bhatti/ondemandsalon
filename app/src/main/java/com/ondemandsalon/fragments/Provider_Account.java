package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.activity.HomeScreen;
import com.ondemandsalon.activity.Opening_Hours;
import com.ondemandsalon.activity.Service_Category;
import com.ondemandsalon.activity.Services_and_Prices;
import com.ondemandsalon.activity.Subscription_Plan;
import com.ondemandsalon.utils.FontFamily;
import com.ondemandsalon.utils.UtilDialog;

public class Provider_Account extends Fragment {

    Context context;
    FontFamily font;
    HomeNew p;

    UtilDialog dialog;

    LinearLayout accsetting, openinghours, changepass, subplan, addofferLin, viewofferLin;
    View view;

    RelativeLayout company_info, category, empRel, serviceRel, ratingRel, settingrel, logoutRel, addofferRel;


    public Provider_Account() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public Provider_Account(HomeNew provider) {
        p = provider;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.provider_account, container, false);
        company_info = v.findViewById(R.id.companyinfoRel);
        category = v.findViewById(R.id.categoryRel);
        empRel = v.findViewById(R.id.empRel);
        serviceRel = v.findViewById(R.id.serviceRel);
        ratingRel = v.findViewById(R.id.ratingRel);
        settingrel = v.findViewById(R.id.settingRel);
        addofferRel = v.findViewById(R.id.addofferRel);

        logoutRel = v.findViewById(R.id.logoutRel);

        company_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.changeFragment(new Service_provider_profile(), 3, "Edit Service Provider Profile", "Provider_Profile", "provider");
                HomeNew.ACCOUNT_LEVEL_1 = "Edit Service Provider Profile";
            }
        });

        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Service_Category.class).putExtra("frm", "back"));
            }
        });

        serviceRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Services_and_Prices.class).putExtra("frm", "back"));
            }
        });

        empRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment f = new Employee(context, p);
                p.changeFragment(f, 3, "Employee List","Emp_list","provider");
                HomeNew.ACCOUNT_LEVEL_1 = "Employee List";
            }
        });

        ratingRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.changeFragment(new Ratings_reviews(p,"provider"), 3, "Ratings & Reviews","RAT_REVIEW","provider");
                HomeNew.ACCOUNT_LEVEL_1 = "Ratings & Reviews";
            }
        });

        settingrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* p.changeFragment(new Account_settings(), 3, "Account");
                HomeScreen.Tooltype = "";*/

                if (view != null) {
                    ViewGroup parent = (ViewGroup) view.getParent();
                    if (parent != null) {
                        parent.removeView(view);
                    }
                }

                view = LayoutInflater.from(context).inflate(R.layout.setting_bottom_layout, null);

                accsetting = view.findViewById(R.id.acsettingLin);
                changepass = view.findViewById(R.id.changpassLin);
                openinghours = view.findViewById(R.id.openinghoursLin);
                subplan = view.findViewById(R.id.viewsubLin);


                accsetting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismissBottomDialog();
                        p.changeFragment(new Account_settings(), 3, "Account","Account_Settings","provider");
                        HomeNew.ACCOUNT_LEVEL_1 = "Account";
                    }
                });

                changepass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismissBottomDialog();
                        p.changeFragment(new Change_Password("FROM PROVIDER"), 3, "ChangeVal Password", "C_PASSWORD","provider");
                        HomeNew.ACCOUNT_LEVEL_1 = "ChangeVal Password";
                    }
                });

                openinghours.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismissBottomDialog();
                        startActivity(new Intent(context, Opening_Hours.class).putExtra("frm", "back"));
                    }
                });

                subplan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismissBottomDialog();
                        startActivity(new Intent(context, Subscription_Plan.class).putExtra("frm", "back"));
                    }
                });

                dialog.showBottomDialog(view);
            }
        });

        addofferRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (view != null) {
                    ViewGroup parent = (ViewGroup) view.getParent();
                    if (parent != null) {
                        parent.removeView(view);
                    }
                }

                view = LayoutInflater.from(context).inflate(R.layout.offer_bottom_layout, null);

                addofferLin = view.findViewById(R.id.addofferLin);
                viewofferLin = view.findViewById(R.id.viewoffersLin);


                addofferLin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismissBottomDialog();
                        p.changeFragment(new Add_Offer("new"), 3, "Add Offers","ADD_OFFER","provider");
                        HomeNew.ACCOUNT_LEVEL_1 = "Add Offers";
                    }
                });

                viewofferLin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismissBottomDialog();
                        p.changeFragment(new ViewOffers(p), 3, "View Offers","V_OFFER","provider");
                        HomeNew.ACCOUNT_LEVEL_1 = "View Offers";
                    }
                });

                dialog.showBottomDialog(view);
            }
        });

        logoutRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p.logout("provider");
            }
        });


        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        dialog = new UtilDialog(context);
        font = new FontFamily(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
