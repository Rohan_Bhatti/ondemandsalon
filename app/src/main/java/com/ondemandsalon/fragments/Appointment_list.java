package com.ondemandsalon.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RelativeLayout;

import com.ondemandsalon.Adapter.Upcoming_appointment_list_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.utils.CustomTextviewRegular;

import java.util.Calendar;


public class Appointment_list extends Fragment {

    Context context;
    RelativeLayout from_date, to_date;
    private int mYear, mMonth, mDay;
    CustomTextviewRegular from_date_txt, to_date_txt;
    RecyclerView upcoming_appointment_list_recycler;

    public Appointment_list() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_list, container, false);
        from_date = view.findViewById(R.id.fromdate);
        to_date = view.findViewById(R.id.todate);
        from_date_txt = view.findViewById(R.id.from_date_txt);
        to_date_txt = view.findViewById(R.id.to_date_txt);
        upcoming_appointment_list_recycler = view.findViewById(R.id.upcoming_appointment_list_recycler);
        upcoming_appointment_list_recycler.setAdapter(new Upcoming_appointment_list_adapter(context));
        upcoming_appointment_list_recycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        from_date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        to_date_txt.setText("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + "");
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
