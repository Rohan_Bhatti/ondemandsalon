package com.ondemandsalon.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Grid_Todays_items_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.activity.HomeNew;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.User_Home_Category;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomGridviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.ondemandsalon.helper.CONSTANTS.ALL_SERVICES;

public class User_Home extends Fragment {


    Context context;
    CustomGridviewRegular grid_today_items;
    Grid_Todays_items_adapter grid_todays_items_adapter;
    ArrayList<User_Home_Category> item_names, tempItems;
    ArrayList<String> nms;
    CardView viewPackagescard, nearbyCard, todaysofferCard;
//    User h;
    TextView searchEdt;
    Preferences preferences;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;

    HomeNew homeNew;


    public User_Home() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public User_Home(Context homeNew) {
        this.homeNew = (HomeNew) homeNew;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = LayoutInflater.from(context).inflate(R.layout.fragment_user_home, container, false);

        viewPackagescard = view.findViewById(R.id.viewPackagescard);
        todaysofferCard = view.findViewById(R.id.todaysofferscard);
        nearbyCard = view.findViewById(R.id.nearbycard);
        grid_today_items = view.findViewById(R.id.today_item);
        grid_today_items.setVerticalScrollBarEnabled(false);
        searchEdt = view.findViewById(R.id.searchEdt);

        getServices();

        searchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tempItems = new ArrayList<>();
                for (int i = 0; i < nms.size(); i++) {
                    if (nms.get(i).toUpperCase().contains(s.toString().toUpperCase())) {
                        tempItems.add(item_names.get(i));
                    }
                }


                grid_todays_items_adapter = new Grid_Todays_items_adapter(context, tempItems);
                grid_today_items.setAdapter(grid_todays_items_adapter);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        grid_today_items.setExpanded(true);

        grid_today_items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User_Home_Category user_home_category = item_names.get(position);
                homeNew.changeFragment(new Search_All_Services(homeNew, user_home_category.getId()), 0, ALL_SERVICES, "ALL_SERVICE","user");
                HomeNew.HOME_LEVEL_1 = ALL_SERVICES;
          /*      h.setHomeDisable();
                h.inside = true;*/
            }
        });

        viewPackagescard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeNew.changeFragment(new View_Packages(), 0, "Packages", "PACKAGES","user");
//                h.setHomeDisable();
//                h.inside = true;
            }
        });

        todaysofferCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeNew.changeFragment(new TodaysOffer(homeNew), 0, "Today's Offer", "TODAY_OFFER","user");
                HomeNew.HOME_LEVEL_1 = "Today's Offer";
      /*          h.setHomeDisable();
                h.inside = true;*/
            }
        });

        nearbyCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeNew.changeFragment(new NearBy(homeNew, "123", "456"), 0, "Near By", "NEAR_BY","user");
                HomeNew.HOME_LEVEL_1 = "Near By";
          /*      h.setHomeDisable();
                h.inside = true;*/
            }
        });

        return view;
    }

    private void getServices() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            json json = new json(API.LIST_OF_CATEGORY_HOMEPAGE, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONArray list = response.getJSONArray("service_data");
                            if (list.length() != 0) {
                                item_names = new ArrayList<User_Home_Category>();
                                nms = new ArrayList<>();
                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    String service_name = jsonObject.getString("ser_name");
                                    String service_image = jsonObject.getString("ser_img");
                                    String service_id = jsonObject.getString("ser_id");

                                    item_names.add(new User_Home_Category(service_name, service_image, service_id));
                                    nms.add(service_name);
                                }
                                grid_todays_items_adapter = new Grid_Todays_items_adapter(context, item_names);
                                grid_today_items.setAdapter(grid_todays_items_adapter);
                            }
                        } else {
                            Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    Toast.makeText(context, "Something wents wrong", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(json);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        preferences = new Preferences(context);
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        progDialog = new ProgDialog(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
