package com.ondemandsalon.webservices;

public class API {
    public static final String MAIN = "https://portfolio.maven-infotech.com/ondemandsaloon/";


//    public static final String GET_LIST_OF_SERVICES = MAIN + "list_of_services.php";

//    public static final String FORGOT_PASSWORD = MAIN + "forget_password.php";

    //4. reset password
//    public static final String RESET_PASSWORD = MAIN + "reset_password.php";

    //5. Change Password after login
//    public static final String CHANGE_PASSWORD_AFTER_LOGIN = MAIN + "change_password.php";


    //7. edit/update profile
    public static final String UPDATE_PROFILE = MAIN + "edit_profile.php";

    //list of servivces on home page(user)
//    public static final String LIST_OF_SERVICES_HOMEPAGE_USER = MAIN + "list_of_services.php";

    //33. List of services category based on main category id (User)
    // its temporary call in User_Edit_Profile() fragment
//    public static final String LIST_OF_SERVICES_CATEGORY_BASED_ON_MAIN_CATEGORY_ID_USER = MAIN + "list_of_services_category_based_on_category_id_user.php";

    //34. Search service provider using required field (User)
    public static final String SEARCH_SERVICE_PROVIDER_USING_REQUIRED_FIELD_USER = MAIN + "search_service_providers.php";

    //35. Search service provider details by its ID (User)
//    public static final String SEARCH_SERVICE_PROVIDER_DETAILS = MAIN + "service_provider_detail_by_id.php";

    //36. List of user appointment by user ID (User)
//    public static final String LIST_OF_USER_APPOINTMENTS = MAIN + "list_of_my_appoinments.php";

    //37. Review & Rating By User (User)
//    public static final String REVIEW_RATING_BY_USER = MAIN + "review_rating.php";

    //38. List of all Review & Rating (User)
    // its temporary call in User_Appointments() fragment
    public static final String LIST_OF_ALL_REVIEWS = MAIN + "list_of_all_reviews.php";

    //39. Book Appointments by user (User)
    public static final String BOOK_APPOINTMENT_USER = MAIN + "booked_appoinment_by_user.php";

    //18. Add Client(Service Provider)
//    public static final String ADD_CLIENT = MAIN + "add_client.php";

    //19. Edit Client(Service Provider)
//    public static final String EDIT_CLIENT = MAIN + "edit_client.php";

    //20. Delete Client(Service Provider)
//    public static final String DELETE_CLIENT = MAIN + "delete_client.php";

    //21. Client Listing(Service Provider Based on Service provider ID)
//    public static final String CLIENT_LISTING = MAIN + "client_listing.php";

    //22. Employee Listing(Service Provider Based on Service provider ID)
//    public static final String EMPLOYEE_LISTING = MAIN + "employee_listing.php";

    //23. Employee Add(Service Provider)
//    public static final String ADD_EMPLOYEE = MAIN + "add_employee.php";

    //24. Employee Edit(Service Provider)
//    public static final String EDIT_EMPLOYEE = MAIN + "edit_employee.php";

    //25. Employee Delete(Service Provider)
    public static final String DELETE_EMPLOYEE = MAIN + "delete_employee.php";

    //27. List of subscription plan (Service Provider)
//    public static final String LIST_OF_SUBSCRIPTION_PLAN = MAIN + "list_of_subscription_plan.php";

    //28. Select subscription plan(Service Provider)
    public static final String SELECT_SUBSRIPTION_PLAN = MAIN + "selected_subscription_plan.php";

    //29. List of upcoming appointments based on service provider id(Service Provider)
    public static final String LIST_UPCOMING_APPOINTMENTS = MAIN + "list_of_upcoming_appointments.php";

    //30. Appointments details based on appointment id(Service Provider)
    public static final String APPOINTMENT_DETAIL = MAIN + "appoinment_details_based_on_sel_user.php";

    //32. List of past appointments based on service provider id(Service Provider)
    public static final String LIST_OF_PAST_APPOINTMENT = MAIN + "list_of_past_appointments.php";


    // before login ( for user )
    public static final String REGISTRATION = MAIN + "registration.php"; //(COMPLETE)

    public static final String LOGIN = MAIN + "login.php"; //(COMPLETE)

    public static final String PROFILE = MAIN + "profile.php"; //(COMPLETE)

    public static final String FORGET_PASSWORD = MAIN + "forget_password.php";//(COMPLETE)

    public static final String CHANGE_PASSWORD_USER = MAIN + "change_password.php";

    public static final String EDIT_PROFILE = MAIN + "edit_profile.php";


    //List of Category on homepage  (COMPLETE)
    public static final String LIST_OF_CATEGORY_HOMEPAGE = MAIN + "list_of_categories.php";

    //View Packeges (COMPLETE)
    public static final String VIEW_PACKAGE = MAIN + "list_of_packages.php";

    //Today's Offers (COMPLETE)
    public static final String TODAYS_OFFERS = MAIN + "list_of_offers.php";

    //near by (COMPLETE)
    public static final String NEAR_BY = MAIN + "near_by.php";

    //List of Services based on category ID (COMPLETE)
    public static final String LIST_OF_SALONS_BASED_ON_HOME_CATEGORY = MAIN + "list_of_services_category_based_on_category_id.php";

    //Services based on selected shop (COMPLETE)
    public static final String SHOP_DETAILS = MAIN + "services_based_on_shop_id.php";

    //Get Search Information
    public static final String GET_SEARCH_BASIC_INFO = MAIN + "get_search_info.php";

    //Post Search Information
    public static final String POST_SEARCH_BASIC_INFO = MAIN + "post_search_info.php";

    //List of rating & review
    public static final String LIST_OF_RATING_AND_REVIEWS = MAIN + "list_of_all_reviews.php";

    //Add Rating & Notes
    public static final String ADD_RATING_AND_REVIEW = MAIN + "add_rating_user.php";

    //User Upcoming & Past Appoinment List
    public static final String APPOINTMENT_LIST_USER = MAIN + "list_of_appoinments_by_user_id.php";

    //Book Appoinment By User
    public static final String BOOK_APPOINTMENT = MAIN + "booked_appoinment_by_user.php";


    //provider
    //List of categories
    public static final String GET_CATEGORY_LIST_PROVIDER = MAIN + "get_categories_list.php";

    //Post categories based on provider id
    public static final String POST_CATEGORY_LIST_PROVIDER = MAIN + "post_category_list.php";

    //Get category list services
    public static final String GET_SERVICES_OF_CATEGORIES = MAIN + "get_category_service_list.php";

    //Post category services list
    public static final String POST_SERVICES_OF_CATEGORIES = MAIN + "post_category_service_list.php";

    //post opening hour based on provider id
    public static final String POST_OPENING_HOURS = MAIN + "post_opening_time.php";

    //Get Subscription Plan Details
    public static final String GET_SUBSCRIPTION_PLAN = MAIN + "get_subscription_plan.php";

    //Post Subscription Plan Details
    public static final String POST_SUBSCRIPTION_PLAN = MAIN + "post_subscription_plan.php";


    //new
    //Client Listing Based on provider Id
    public static final String CLIENT_LISTING = MAIN + "client_listing.php";

    //update client
    public static final String UPDATE_CLIENT = MAIN + "edit_client.php";

    //add client
    public static final String ADD_CLIENT = MAIN + "add_client.php";

    //Employee Listing Based on provider Id
    public static final String EMPLOYEE_LISTING = MAIN + "employee_listing.php";

    //add category using provider id
    public static final String ADD_CATEGORY = MAIN + "add_category.php";

    //list_of_categories_based_on_provider_id
    public static final String LIST_CATEGORIES_BEFORE_ADD_NEW_CATEGORY = MAIN + "list_of_categories_based_on_provider_id.php";

    //add category services based on provider id
    public static final String ADD_NEW_SERVICE = MAIN + "add_category_services.php";

    //delete category services based on provider id
    public static final String DELETE_SERVICE = MAIN + "delete_category_services.php";

    //Dashboard
    public static final String DASHBOARD = MAIN + "dashboard.php";

    //add employee
    public static final String ADD_EMPLOYEE = MAIN + "add_employee.php";

    //Edit Employee
    public static final String EDIT_EMPLOYEE = MAIN + "edit_employee.php";

    //Get Banner Images based on provider id
    public static final String GET_BANNER_IMAGES = MAIN + "get_profile_banner_image_by_provider_id.php";

    //Edit Company Info
    public static final String EDIT_PROVIDER_PROFILE = MAIN + "edit_company_info.php";

    //Post Banner Images based on provider id
    public static final String ADD_BANNER_IMAGE = MAIN + "post_profile_banner_image_by_provider_id.php";

    //delete banner based on banner id in edit profile
    public static final String DELETE_BANNER_IMAGE = MAIN + "delete_banner_based_on_banner_id.php";


    //pending
    //Get Account Details Question-answer
    public static final String GET_ACCOUNT_DETAILS = MAIN + "get_account_setting_details.php";

    //Post Account Details  Question-answer
    public static final String POST_ACCOUNT_DETAILS = MAIN + "post_account_setting_details_by_provider.php";

    //Get User Account Settings
    public static final String GET_USER_ACCOUNT_SETTINGS = MAIN + "get_user_account_settings.php";

    //Post User Account Settings
    public static final String POST_USER_ACCOUNT_SETTINGS = MAIN + "post_account_setting_details_by_user.php";

    //User Notifications
    public static final String USER_NOTIFICATIONS = MAIN + "user_notifications.php";

}
