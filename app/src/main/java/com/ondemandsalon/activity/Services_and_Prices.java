package com.ondemandsalon.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Add_Services_Category_adapter;
import com.ondemandsalon.Adapter.Services_Adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Services_and_Prices extends AppCompatActivity implements Services_Adapter.addremove {

    RecyclerView services_recycler;
    RelativeLayout addServiceRel, addCategoryrel;
    ArrayList<Service_data> servicelst;
    RelativeLayout saveRel;
    ImageView navMenu;
    String frm;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.services_and_prices);

        services_recycler = findViewById(R.id.services_recycler);
        frm = getIntent().getStringExtra("frm");
        navMenu = findViewById(R.id.navMenu);
        preferences = new Preferences(this);
        connectionDetector = new ConnectionDetector(this);
        progDialog = new ProgDialog(this);
        context = this;

        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        saveRel = findViewById(R.id.saveRel);

        getServicesOfCategories();

        addServiceRel = findViewById(R.id.addServiceRel);
        addCategoryrel = findViewById(R.id.addCategoryRel);

        addServiceRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Services_and_Prices.this, New_Service.class), 1);
            }
        });

        addCategoryrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Services_and_Prices.this, New_Category.class), 2);
            }
        });

        saveRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!frm.equals("back")) {
                    postServivecesOfCategories();
                } else {
                    onBackPressed();
                }
            }
        });
    }

    private void postServivecesOfCategories() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("category_service_list", getCatList());
                jsonObject.accumulate("provider_id", preferences.getProviderId());

                json json = new json(API.POST_SERVICES_OF_CATEGORIES, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.getString("status").equals("1")) {
                                startActivityForResult(new Intent(Services_and_Prices.this, Subscription_Plan.class).putExtra("frm", "result"), 3);
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                        Toast.makeText(context, "Somthing wents wrong\nPlease try again", Toast.LENGTH_SHORT).show();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private JSONArray getCatList() {
        JSONArray category_list = new JSONArray();
        if (servicelst != null && servicelst.size() != 0) {
            for (int i = 0; i < servicelst.size(); i++) {
                Service_data service_data = servicelst.get(i);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("ser_cs_id", service_data.getService_id());
                    jsonObject.put("ser_cs_name", service_data.getService_name());
                    jsonObject.put("ser_cs_parent", service_data.getService_parent_id());
                    jsonObject.put("ser_cs_price", service_data.getService_price());
                    jsonObject.put("ser_cs_time", service_data.getService_time());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                category_list.put(jsonObject);
            }
            return category_list;
        }
        return category_list;
    }

    private void getServicesOfCategories() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();

            json j = new json(API.GET_SERVICES_OF_CATEGORIES, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONArray jsonArray = response.getJSONArray("catogory_services_list");
                            if (jsonArray.length() != 0) {
                                servicelst = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String service_id = jsonObject.getString("ser_cat_id");
                                    String service_name = jsonObject.getString("ser_cat_name");
                                    String service_price = jsonObject.getString("ser_cat_price");
                                    String service_time = jsonObject.getString("ser_cat_time");
                                    String service_parent_id = jsonObject.getString("ser_cat_parent");

                                    servicelst.add(new Service_data(service_id, service_name, service_price + " AED", service_time, service_parent_id));
                                }
                                services_recycler.setAdapter(new Services_Adapter(context, servicelst));
                                services_recycler.setLayoutManager(new LinearLayoutManager(context));
                            }
                        } else {
                            Toast.makeText(Services_and_Prices.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    Toast.makeText(Services_and_Prices.this, "Somthing wents wrong", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
//                servicelst.add(new Service_data(data.getStringExtra("sname"), 5, "AED50"));
                services_recycler.setAdapter(new Services_Adapter(this, servicelst));
                services_recycler.setLayoutManager(new LinearLayoutManager(this));

            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
//                servicelst.add(new Service_data(data.getStringExtra("ct"), 5, "AED50"));
                services_recycler.setAdapter(new Services_Adapter(this, servicelst));
                services_recycler.setLayoutManager(new LinearLayoutManager(this));

            }
        } else if ((requestCode == 3)) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    @Override
    public void AddRemove(String reason, String service_id, String s) {
        if (servicelst != null && servicelst.size() != 0) {
            if (reason.equals("TIME")) {
                for (int i = 0; i < servicelst.size(); i++) {
                    Service_data sdata = servicelst.get(i);
                    if (sdata.getService_id().equals(service_id)) {
                        sdata.setService_time(s);
                        servicelst.remove(i);
                        servicelst.add(i, sdata);
                        break;
                    }
                }
            } else if (reason.equals("PRICE")) {
                for (int i = 0; i < servicelst.size(); i++) {
                    Service_data sdata = servicelst.get(i);
                    if (sdata.getService_id().equals(service_id)) {
                        sdata.setService_price(s);
                        servicelst.remove(i);
                        servicelst.add(i, sdata);
                        break;
                    }
                }
            } else if (reason.equals("REMOVE")) {
                for (int i = 0; i < servicelst.size(); i++) {
                    Service_data sdata = servicelst.get(i);
                    if (sdata.getService_id().equals(service_id)) {
                        servicelst.remove(i);
                        break;
                    }
                }
            }
            services_recycler.setAdapter(new Services_Adapter(context, servicelst));
            services_recycler.setLayoutManager(new LinearLayoutManager(context));
        }
    }
}
