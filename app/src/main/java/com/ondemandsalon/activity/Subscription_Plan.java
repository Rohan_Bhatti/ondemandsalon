package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Subscription_plan_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Subscription_Plan extends AppCompatActivity {

    RecyclerView subscription_plan_recycler;
    RelativeLayout continueRel;

    CheckBox check1, check2, check3;
    ImageView navMenu, p1, p2, p3, a1, a2, a3, b1, b2, b3, c1, c2, c3, d1, d2, d3, e1, e2, e3;
    String frm;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;
    String plan_id = "";
    String plan1_id = "";
    String plan2_id = "";
    String plan3_id = "";
    CustomTextviewRegular package1price, package2price, package3price, duration1, duration2, duration3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscription_plan);
        frm = getIntent().getStringExtra("frm");
        continueRel = findViewById(R.id.continueRel);
        check1 = findViewById(R.id.check1);
        check2 = findViewById(R.id.check2);
        check3 = findViewById(R.id.check3);
        p1 = findViewById(R.id.imgp1);
        p2 = findViewById(R.id.imgp2);
        p3 = findViewById(R.id.imgp3);
        navMenu = findViewById(R.id.navMenu);
        package1price = findViewById(R.id.packageprice1);
        package2price = findViewById(R.id.packageprice2);
        package3price = findViewById(R.id.packageprice3);
        connectionDetector = new ConnectionDetector(this);
        progDialog = new ProgDialog(this);
        preferences = new Preferences(this);

        duration1 = findViewById(R.id.duration1);
        duration2 = findViewById(R.id.duration2);
        duration3 = findViewById(R.id.duration3);

        a1 = findViewById(R.id.a1);
        a2 = findViewById(R.id.a2);
        a3 = findViewById(R.id.a3);
        b1 = findViewById(R.id.b1);
        b2 = findViewById(R.id.b2);
        b3 = findViewById(R.id.b3);
        c1 = findViewById(R.id.c1);
        c2 = findViewById(R.id.c2);
        c3 = findViewById(R.id.c3);
        d1 = findViewById(R.id.d1);
        d2 = findViewById(R.id.d2);
        d3 = findViewById(R.id.d3);
        e1 = findViewById(R.id.e1);
        e2 = findViewById(R.id.e2);
        e3 = findViewById(R.id.e3);

        List_Subscription_Plan();

        check1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    plan_id = plan1_id;
                    p1.setVisibility(View.VISIBLE);
                    check2.setChecked(false);
                    check3.setChecked(false);
                } else {
                    p1.setVisibility(View.INVISIBLE);
                }
            }
        });
        check2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    plan_id = plan2_id;
                    p2.setVisibility(View.VISIBLE);
                    check1.setChecked(false);
                    check3.setChecked(false);
                } else {
                    p2.setVisibility(View.INVISIBLE);
                }
            }
        });
        check3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    plan_id = plan3_id;
                    p3.setVisibility(View.VISIBLE);
                    check1.setChecked(false);
                    check2.setChecked(false);
                } else {
                    p3.setVisibility(View.INVISIBLE);
                }
            }
        });

        continueRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!plan_id.equals("")) {
                    post_subscription_plan();
                } else {
                    Toast.makeText(Subscription_Plan.this, "Please select any subscription plan", Toast.LENGTH_SHORT).show();
                }
            }
        });


        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

      /*  subscription_plan_recycler = findViewById(R.id.subscription_plan_recycler);
        subscription_plan_recycler.setAdapter(new Subscription_plan_adapter(this));
        subscription_plan_recycler.setLayoutManager(new LinearLayoutManager(this));*/
    }

    private void post_subscription_plan() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("provider_id", preferences.getProviderId());
                jsonObject.accumulate("plan_id", plan_id);

                json json = new json(API.POST_SUBSCRIPTION_PLAN, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.getString("status").equals("1")) {
                                onBackPressed();
                                Toast.makeText(Subscription_Plan.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Subscription_Plan.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                        Toast.makeText(Subscription_Plan.this, "Somthing wents wrong\nPlease try again", Toast.LENGTH_SHORT).show();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        setResult(RESULT_OK, i);
        finish();
    }

    private void List_Subscription_Plan() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();

            json json = new json(API.GET_SUBSCRIPTION_PLAN, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONArray jsonArray = response.getJSONArray("catogory_services_list");
                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    String id = jsonObject.getString("sp_id");
                                    String sp_listing_business = jsonObject.getString("sp_listing_business");
                                    String sp_personalized_setting = jsonObject.getString("sp_personalized_setting");
                                    String sp_add_limited_services = jsonObject.getString("sp_add_limited_services");
                                    String sp_in_top_listing = jsonObject.getString("sp_in_top_listing");
                                    String sp_promotion = jsonObject.getString("sp_promotion");
                                    String sp_duration = jsonObject.getString("sp_duration");
                                    String sp_price = jsonObject.getString("sp_price");
                                    String[] price = sp_price.split("\\.");

                                    if (id.equals("1")) {
                                        plan1_id = id;
                                        package1price.setText("AED" + price[0]);
                                        duration1.setText(sp_duration);
                                        if (sp_listing_business.equals("Yes")) {
                                            a1.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            a1.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_personalized_setting.equals("Yes")) {
                                            b1.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            b1.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_add_limited_services.equals("Yes")) {
                                            c1.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            c1.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_in_top_listing.equals("Yes")) {
                                            d1.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            d1.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_promotion.equals("Yes")) {
                                            e1.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            e1.setImageResource(R.drawable.red_close);
                                        }
                                    } else if (id.equals("2")) {
                                        plan2_id = id;
                                        package2price.setText("AED" + price[0]);
                                        duration2.setText(sp_duration);

                                        if (sp_listing_business.equals("Yes")) {
                                            a2.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            a2.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_personalized_setting.equals("Yes")) {
                                            b2.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            b2.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_add_limited_services.equals("Yes")) {
                                            c2.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            c2.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_in_top_listing.equals("Yes")) {
                                            d2.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            d2.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_promotion.equals("Yes")) {
                                            e2.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            e2.setImageResource(R.drawable.red_close);
                                        }

                                    } else if (id.equals("3")) {
                                        plan3_id = id;
                                        package3price.setText("AED" + price[0]);
                                        duration3.setText(sp_duration);

                                        if (sp_listing_business.equals("Yes")) {
                                            a3.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            a3.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_personalized_setting.equals("Yes")) {
                                            b3.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            b3.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_add_limited_services.equals("Yes")) {
                                            c3.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            c3.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_in_top_listing.equals("Yes")) {
                                            d3.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            d3.setImageResource(R.drawable.red_close);
                                        }
                                        if (sp_promotion.equals("Yes")) {
                                            e3.setImageResource(R.drawable.booked_green_sign);
                                        } else {
                                            e3.setImageResource(R.drawable.red_close);
                                        }
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(Subscription_Plan.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    Toast.makeText(Subscription_Plan.this, "Somthing wents wrong", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(json);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void Select_Subscription_Plan() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("cl_added_by", "1");

                json json = new json(API.SELECT_SUBSRIPTION_PLAN, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.get("status").equals("1")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

}
