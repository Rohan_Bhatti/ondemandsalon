package com.ondemandsalon.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextLight;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewLight;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ondemandsalon.fragments.User.disableLogin;

public class SignUp_Provider extends AppCompatActivity {

    RelativeLayout signUprel;
    CustomEdittextLight business_email, password, confirm_password, name_of_business, business_phone_num, country, city, location, address;
    ImageView navMenu;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_provider);

        signUprel = findViewById(R.id.signUpRel);
        navMenu = findViewById(R.id.navMenu);
        name_of_business = findViewById(R.id.businessnameedt);
        business_phone_num = findViewById(R.id.businiess_number_edt);
        country = findViewById(R.id.countryedt);
        city = findViewById(R.id.cityedt);
        location = findViewById(R.id.locationedt);
        address = findViewById(R.id.addressedt);
        business_email = findViewById(R.id.business_email_edt);
        password = findViewById(R.id.password_edt);
        confirm_password = findViewById(R.id.confirm_password_edt);
        connectionDetector = new ConnectionDetector(this);
        progDialog = new ProgDialog(this);
        preferences = new Preferences(this);

        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        signUprel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });


    }

    private void validate() {

        if (name_of_business.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter name of business", Toast.LENGTH_SHORT).show();
            requestFocus(name_of_business);
            return;
        }

        if (business_phone_num.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter business phone number", Toast.LENGTH_SHORT).show();
            requestFocus(business_phone_num);
            return;
        }

        if (country.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter country", Toast.LENGTH_SHORT).show();
            requestFocus(country);
            return;
        }

        if (city.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter city", Toast.LENGTH_SHORT).show();
            requestFocus(city);
            return;
        }

        if (location.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter location", Toast.LENGTH_SHORT).show();
            requestFocus(location);
            return;
        }

        if (address.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter address", Toast.LENGTH_SHORT).show();
            requestFocus(address);
            return;
        }

        if (!validateEmail()) {
            return;
        }

        String pass = password.getText().toString().trim();
        if (pass.isEmpty()) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            requestFocus(password);
            return;
        } else if (pass.length() < 4) {
            Toast.makeText(this, "Password must be atleast 4 digit", Toast.LENGTH_SHORT).show();
            requestFocus(password);
            return;
        }

        String s = confirm_password.getText().toString().trim();
        if (s.isEmpty()) {
            Toast.makeText(this, "Please enter confirm password", Toast.LENGTH_SHORT).show();
            requestFocus(confirm_password);
            return;
        } else if (s.length() < 4) {
            Toast.makeText(this, "Confirm password must be atleast 4 digit", Toast.LENGTH_SHORT).show();
            requestFocus(confirm_password);
            return;
        }

        if (!pass.equals(s)) {
            Toast.makeText(this, "Password and Confirm password must be same", Toast.LENGTH_SHORT).show();
            requestFocus(password);
            return;
        }

        if (connectionDetector.isNetworkAvailable()) {
            signup();
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private boolean validateEmail() {
        String e = business_email.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(business_email);
            return false;
        } else if (!isValidEmail(e)) {
            Toast.makeText(this, "Please enter valid email", Toast.LENGTH_SHORT).show();
            requestFocus(business_email);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void signup() {
        progDialog.ProgressDialogStart();
        JSONObject params = new JSONObject();
        try {
            params.accumulate("business_name", name_of_business.getText().toString());
            params.accumulate("user_phone", business_phone_num.getText().toString().trim());
            params.accumulate("user_location", location.getText().toString());
            params.accumulate("user_email", business_email.getText().toString());
            params.accumulate("user_password", password.getText().toString());
            params.accumulate("city", city.getText().toString());
            params.accumulate("country", country.getText().toString().trim());
            params.accumulate("business_full_address", address.getText().toString());
            params.accumulate("role", "1");
            params.accumulate("device_type", "1");
            params.accumulate("device_token", "123");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        json j = new json(API.REGISTRATION, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
                System.out.println(response.toString());
                try {
                    if (response.getString("status").equals("1")) {
                        JSONObject jsonObject = response.getJSONObject("service_provide_details");
                        preferences.setClientID(jsonObject.getString("id"));
//                        Toast.makeText(SignUp_Provider.this, "sucess", Toast.LENGTH_SHORT).show();
                        startActivityForResult(new Intent(SignUp_Provider.this, Opening_Hours.class).putExtra("frm", "result").putExtra("class", "Registration"), 1);
                    } else {
                        Toast.makeText(SignUp_Provider.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                System.out.println(error.toString());
            }
        });
        AppController.getInstance().addToRequestQueue(j);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }
}
