package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.ondemandsalon.activity.HomeScreen.Current_user;
import static com.ondemandsalon.activity.HomeScreen.loginTxt;

public class ForgotPasswordActivity extends AppCompatActivity {

    ImageView closeImage;

    CustomEdittextRegular email;

    RelativeLayout forgotPsdrel;

    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        connectionDetector = new ConnectionDetector(this);
        progDialog = new ProgDialog(this);
        preferences = new Preferences(this);

        closeImage = findViewById(R.id.closeImage);
        email = findViewById(R.id.emailEdt);

        forgotPsdrel = findViewById(R.id.forgotPsdRel);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        forgotPsdrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

    }

    private void validate() {

        if (!validateEmail()) {
            return;
        }

        if (connectionDetector.isNetworkAvailable()) {
            forgotPasd();
        } else
            connectionDetector.error_Dialog();

    }

    private void forgotPasd() {

        progDialog.ProgressDialogStart();

        final String eml = email.getText().toString();

        JSONObject params = new JSONObject();

        try {
            params.accumulate("user_email", eml);
            if (Current_user.equals("SERVICE PROVIDER")) {
                params.accumulate("role", "1");
            } else if (Current_user.equals("USER")) {
                params.accumulate("role", "2");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        json j = new json(API.FORGET_PASSWORD, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
                try {
                    if (response.getString("status").equals("1")) {
                        Toast.makeText(ForgotPasswordActivity.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        finish();
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                Toast.makeText(ForgotPasswordActivity.this, "Somthing wents wrong", Toast.LENGTH_SHORT).show();

            }
        });

        AppController.getInstance().addToRequestQueue(j);
    }

    private boolean validateEmail() {
        String e = email.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(email);
            return false;
        } else {
            if (!isValidEmail(e)) {
                Toast.makeText(this, "Please enter valid email", Toast.LENGTH_SHORT).show();
                requestFocus(email);
                return false;
            }
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
