package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ondemandsalon.R;
import com.ondemandsalon.utils.CustomEdittextRegular;

public class SignUp_Email extends AppCompatActivity {

    RelativeLayout signInrrel;
    CustomEdittextRegular emailtxt;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_email);

        pos = getIntent().getIntExtra("pos", 0);
        final String fro = getIntent().getStringExtra("for");
        signInrrel = findViewById(R.id.signInRel);
        emailtxt = findViewById(R.id.emailEdt);

        signInrrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateEmail(emailtxt)) {
                    return;
                } else {
                    startActivityForResult(new Intent(SignUp_Email.this, SignUp_User.class).putExtra("pos", pos).putExtra("for", fro).putExtra("email", emailtxt.getText().toString()),1);
                }
            }
        });
    }

    private boolean validateEmail(CustomEdittextRegular emailEdt) {
        String e = emailEdt.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(SignUp_Email.this, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        } else if (!isValidEmail(e)) {
            Toast.makeText(SignUp_Email.this, "Please enter valid email", Toast.LENGTH_SHORT).show();
            requestFocus(emailEdt);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }
}
