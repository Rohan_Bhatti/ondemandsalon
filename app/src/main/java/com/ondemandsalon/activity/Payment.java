package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ondemandsalon.R;
import com.ondemandsalon.helper.OnResponseListener;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.LoginDialog;

public class Payment extends AppCompatActivity {

    RelativeLayout payNowRel;
    Spinner select_date, select_year;
    CustomEdittextRegular card_number_edt;
    ImageView navMenu;
    LoginDialog dialog;
    Preferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);

        payNowRel = findViewById(R.id.payNowRel);
        navMenu = findViewById(R.id.navMenu);
        preferences = new Preferences(this);

        dialog = new LoginDialog( this);

        payNowRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preferences.getUserLogin())
                    startActivity(new Intent(Payment.this, Thank_You.class));
                else
                    dialog.openLogin("user", new OnResponseListener() {
                        @Override
                        public void onResponce(int res) {
                        }
                    });
            }
        });

        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        select_date = findViewById(R.id.date_spinner);
        select_year = findViewById(R.id.year_spinner);
        card_number_edt = findViewById(R.id.card_number_edt);

        card_number_edt.addTextChangedListener(new TextWatcher() {

            Boolean isDelete;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (before == 0)
                    isDelete = false;
                else
                    isDelete = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String source = editable.toString();
                int length = source.length();

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(source);

                if (length > 0 && length % 5 == 0) {
                    if (isDelete)
                        stringBuilder.deleteCharAt(length - 1);
                    else
                        stringBuilder.insert(length - 1, " ");

                    card_number_edt.setText(stringBuilder);
                    card_number_edt.setSelection(card_number_edt.getText().length());
                }
            }
        });

        select_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
                ((TextView) adapterView.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        select_date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
                ((TextView) adapterView.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

/*    @Override
    public void returnLoginValue(String rtn) {
        if (rtn.equals("ULogin")) {
            startActivity(new Intent(Payment.this, Thank_You.class));
        } else if (rtn.equals("USignUp")){
            startActivityForResult(new Intent(Payment.this, SignUp_Social.class).putExtra("pos", -1), 1);
        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                preferences.setLogin(true);
                preferences.lastLogin("user");
                preferences.setUserLogin(true);
                if (data.getIntExtra("pos", 0) == -1) {
                    startActivity(new Intent(Payment.this, Thank_You.class));
                }
            }
        }
    }
}
