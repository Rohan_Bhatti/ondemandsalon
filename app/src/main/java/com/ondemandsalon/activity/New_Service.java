package com.ondemandsalon.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ondemandsalon.Adapter.Add_Category_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.utils.CustomEdittextLight;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewLight;
import com.ondemandsalon.utils.UtilDialog;

import java.util.ArrayList;

public class New_Service extends AppCompatActivity {


    CustomTextviewLight select_service_spinner;
    CustomEdittextLight snm;
    RecyclerView add_services_recycler;
    RelativeLayout saverel, saveAndAddAnother;
    UtilDialog dialog;
    LinearLayout addduration;

    ImageView navMenu;
    ArrayList<Service_data> slist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_service);
        dialog = new UtilDialog(this);

        navMenu = findViewById(R.id.navMenu);

        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        slist = new ArrayList<>();
        snm = findViewById(R.id.snm);
        select_service_spinner = findViewById(R.id.select_service_spinner);
        addduration = findViewById(R.id.addduartion);
        add_services_recycler = findViewById(R.id.add_services_recycler);


        select_service_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.openListDialog("Select Category", getResources().getStringArray(R.array.Service_category), select_service_spinner);
            }
        });


        addduration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

      /*  select_service_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.blue));
                ((TextView) adapterView.getChildAt(0)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
*/
        saverel = findViewById(R.id.saveRel);
        saveAndAddAnother = findViewById(R.id.saveRelAndAddAnother);

        saverel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("sname", snm.getText().toString());
                i.putExtra("frm", "new");
                setResult(RESULT_OK, i);
                finish();
            }
        });

        saveAndAddAnother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("sname", snm.getText().toString());
                i.putExtra("frm", "new");
                setResult(RESULT_OK, i);
                finish();
            }
        });
    }

    private void openDialog() {

        final AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final CustomEdittextRegular duration, price;
        RelativeLayout btnDone;


        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.duration_dialog, null);
        builder.setView(dialogView);
        dialog = builder.create();

        duration = dialogView.findViewById(R.id.duration);
        price = dialogView.findViewById(R.id.price);
        btnDone = dialogView.findViewById(R.id.ok);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                slist.add(new Service_data(duration.getText().toString(), 0, price.getText().toString()));
                add_services_recycler.setAdapter(new Add_Category_adapter(New_Service.this, slist));
                add_services_recycler.setLayoutManager(new LinearLayoutManager(New_Service.this));
                dialog.dismiss();
            }
        });


        dialog.show();


    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }*/
}
