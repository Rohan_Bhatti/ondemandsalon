package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;

public class Thank_You extends AppCompatActivity {

    RelativeLayout continueRel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thank_you);

        continueRel = findViewById(R.id.continueRel);

        continueRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Thank_You.this, HomeScreen.class));
            }
        });

    }
}
