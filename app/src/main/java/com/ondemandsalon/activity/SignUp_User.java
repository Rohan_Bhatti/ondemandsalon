package com.ondemandsalon.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.ServiceData;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.ondemandsalon.fragments.User.disableLogin;

public class SignUp_User extends AppCompatActivity {

    CustomEdittextRegular email_edt, first_name_edt, last_name_edt, phone_number_edt, password_edt;
    ImageView closeImage;
    RelativeLayout signUpRel;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;

    String service = "",fro;
    int pos;

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_user);

        connectionDetector = new ConnectionDetector(this);
        progDialog = new ProgDialog(this);
        preferences = new Preferences(this);
        pos = getIntent().getIntExtra("pos", 0);
        fro = getIntent().getStringExtra("for");
        String emailfrom = getIntent().getStringExtra("email");

        closeImage = findViewById(R.id.closeImage);
        signUpRel = findViewById(R.id.signUpRel);

        email_edt = findViewById(R.id.email_edt);
        first_name_edt = findViewById(R.id.first_name_edt);
        last_name_edt = findViewById(R.id.last_name_edt);
        phone_number_edt = findViewById(R.id.phone_number_edt);
        password_edt = findViewById(R.id.password_edt);

        email_edt.setText(emailfrom);
        requestFocus(first_name_edt);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        signUpRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
    }

    private void validate() {

        if (!validateEmail()) {
            return;
        }

        if (first_name_edt.getText().toString().trim().isEmpty()) {
            first_name_edt.setError("Please enter first name");
            requestFocus(first_name_edt);
            return;
        }

        if (last_name_edt.getText().toString().trim().isEmpty()) {
            last_name_edt.setError("Please enter last name");
            requestFocus(last_name_edt);
            return;
        }

        String phone = phone_number_edt.getText().toString().trim();
        if (phone.isEmpty()) {
            Toast.makeText(this, "Please enter phone number", Toast.LENGTH_SHORT).show();
            requestFocus(phone_number_edt);
            return;
        } else if (phone.length() < 10) {
            Toast.makeText(this, "Phone number must be atleast 10 digit", Toast.LENGTH_SHORT).show();
            requestFocus(phone_number_edt);
            return;
        }

        String pass = password_edt.getText().toString().trim();
        if (pass.isEmpty()) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            requestFocus(password_edt);
            return;
        } else if (pass.length() < 4) {
            Toast.makeText(this, "Please enter 4 digit password", Toast.LENGTH_SHORT).show();
            requestFocus(password_edt);
            return;
        }

        if (connectionDetector.isNetworkAvailable()) {
            signup();
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private void signup() {
        progDialog.ProgressDialogStart();
        JSONObject params = new JSONObject();
        try {
            params.accumulate("user_email", email_edt.getText().toString().trim());
            params.accumulate("user_fname", first_name_edt.getText().toString().trim());
            params.accumulate("user_lname", last_name_edt.getText().toString().trim());
            params.accumulate("user_phone", phone_number_edt.getText().toString().trim());
            params.accumulate("user_password", password_edt.getText().toString().trim());
            params.accumulate("role", "2");
            params.accumulate("device_type", "1");
            params.accumulate("device_token", "123");
            params.accumulate("latitude", "123");
            params.accumulate("longitude", "123");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        json j = new json(API.REGISTRATION, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
                System.out.println(response.toString());
                try {
                    if (response.getString("status").equals("1")) {
                        JSONObject jsonObject = response.getJSONObject("user_details");
                        preferences.setClientID(jsonObject.getString("id"));
                        preferences.setLogin(true);
                        preferences.setUserLogin(true);
                        preferences.lastLogin("user");
                        onBackPressed();
                    } else {
                        Toast.makeText(SignUp_User.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                System.out.println(error.toString());
            }
        });
        AppController.getInstance().addToRequestQueue(j);
    }


    /*public void signup() {
        progDialog.ProgressDialogStart();
        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST, API.REGISTRATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progDialog.ProgressDialogStop();
                System.out.println(response);
                try {
                    JSONObject newResponse = new JSONObject(response);
                    if (newResponse.getString("status").equals("1")) {
                        JSONObject jsonObject = newResponse.getJSONObject("user_details");
                        preferences.setUserLogin(true);
                        preferences.setClientID(jsonObject.getString("id"));
                        startActivity(new Intent(SignUp_User.this, HomeScreen.class));
                        preferences.setLogin(true);
                        preferences.setUserLogin(true);
                        preferences.lastLogin("user");
                        disableLogin();
                        finish();
                    } else {
                        Toast.makeText(SignUp_User.this, newResponse.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                Toast.makeText(SignUp_User.this, "Please try again", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_email", email_edt.getText().toString().trim());
                params.put("user_fname", first_name_edt.getText().toString().trim());
                params.put("user_lname", last_name_edt.getText().toString().trim());
                params.put("user_phone", phone_number_edt.getText().toString().trim());
                params.put("user_password", password_edt.getText().toString().trim());
                params.put("role", "2");
                params.put("device_type", "1");
                params.put("device_token", "123");
                params.put("latitude", "123");
                params.put("longitude", "123");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjRequest);
    }*/

    private boolean validateEmail() {
        String e = email_edt.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(email_edt);
            return false;
        } else {
            if (!isValidEmail(e)) {
                Toast.makeText(this, "Please enter valid email", Toast.LENGTH_SHORT).show();
                requestFocus(email_edt);
                return false;
            }
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

  /*  private void outFocus(View view) {
        if (view != null) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }*/

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("pos", pos);
        i.putExtra("for", fro);
        setResult(RESULT_OK, i);
        finish();
    }


}
