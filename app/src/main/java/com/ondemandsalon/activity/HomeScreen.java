package com.ondemandsalon.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ondemandsalon.Adapter.EventAdapter;
import com.ondemandsalon.R;
import com.ondemandsalon.fragments.Account_settings;
import com.ondemandsalon.fragments.Add_clients;
import com.ondemandsalon.fragments.Add_employee;
import com.ondemandsalon.fragments.Appointment_details;
import com.ondemandsalon.fragments.Change_Password;
import com.ondemandsalon.fragments.Employee;
import com.ondemandsalon.fragments.NearBy;
import com.ondemandsalon.fragments.Provider;
import com.ondemandsalon.fragments.Ratings_reviews;
import com.ondemandsalon.fragments.Ratreviews_details;
import com.ondemandsalon.fragments.Search_All_Services;
import com.ondemandsalon.fragments.Select_Appointment_Time;
import com.ondemandsalon.fragments.Service_Details;
import com.ondemandsalon.fragments.Service_provider_profile;
import com.ondemandsalon.fragments.TodaysOffer;
import com.ondemandsalon.fragments.User;
import com.ondemandsalon.fragments.User_Edit_Profile;
import com.ondemandsalon.fragments.View_Packages;
import com.ondemandsalon.helper.OnResponseListener;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.CustomTextviewLight;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.LoginDialog;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;
import static com.ondemandsalon.helper.CONSTANTS.ALL_SERVICES;
import static com.ondemandsalon.helper.CONSTANTS.APPOINTMENT;
import static com.ondemandsalon.helper.CONSTANTS.CLIENT;
import static com.ondemandsalon.helper.CONSTANTS.EMPLOYEE_LIST;
import static com.ondemandsalon.helper.CONSTANTS.PROFILE;
import static com.ondemandsalon.helper.CONSTANTS.RATING_AND_REVIEWS;
import static com.ondemandsalon.helper.CONSTANTS.SERVICES;
import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;

public class HomeScreen extends AppCompatActivity implements EventAdapter.gotoNext, User.gotoNext {

    private static final String TAG_USER = "USER", TAG_PROVIDER = "PROVIDER";
    public static CustomTextviewRegular toolTitle;
    public static String CURRENT_TAG = TAG_USER;
    // index to identify current nav menu item
    public static int navItemIndex = 0;
    public static RelativeLayout sellYourServiceRel;
    public static ImageView tooImage;
    public static String Tooltype;
    public static CustomTextviewLight loginTxt;
    static Context c;
    static changVal val;
    ImageView navMenu/*, logout*/;
    Preferences preferences;
    LoginDialog dialog;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    public static String Current_user = "";

    public static void setInstance(Fragment f) {
        val = (changVal) f;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        preferences = new Preferences(this);
        dialog = new LoginDialog( this);

        toolTitle = findViewById(R.id.toolbar_title);
        tooImage = findViewById(R.id.toolImg);
        toolTitle.setText(YOUR_LOCATION);

        loginTxt = findViewById(R.id.logintxt);

        c = this;

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        navMenu = findViewById(R.id.navMenu);

        sellYourServiceRel = findViewById(R.id.sellYourServiceRel);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            if (preferences.getLogin()) {
                loadHomeFragment(preferences.getlastLogin());
            } else
                loadHomeFragment("user");
        }


        loginTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                switch (editable.toString()) {
                    case "Normal User":
                        Current_user = "SERVICE PROVIDER";
                        break;
                    case "Sell Your Services":
                        Current_user = "SERVICE PROVIDER";
                        break;
                    case "Business Profile":
                        Current_user = "USER";
                        break;
                }
            }
        });

        sellYourServiceRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tp = loginTxt.getText().toString();
                switch (tp) {
                    case "Sell Your Services":
                        dialog.openLogin("provider", new OnResponseListener() {
                            @Override
                            public void onResponce(int res) {
                            }
                        });
//                        openDialog("provider");
                        break;
                    case "Normal User":
                        preferences.lastLogin("user");
                        loadHomeFragment("user");
                        break;
                    case "Business Profile":
                        preferences.lastLogin("provider");
                        loadHomeFragment("provider");
                        break;
                    case "User Login":
                        dialog.openLogin("user", new OnResponseListener() {
                            @Override
                            public void onResponce(int res) {
                            }
                        });
//                        openDialog("user");
                        break;
                }
            }
        });

        tooImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Tooltype) {
                    case "Client":
                        HomeScreen.tooImage.setVisibility(View.GONE);
                        val.change(0,"");
                        break;
                    case "Employee":
                        HomeScreen.tooImage.setVisibility(View.GONE);
                        val.change(0,"");
                        break;
                    case "Appointment":
//                        Appointment_details.enabletext();
                        break;
                }
            }
        });


        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawers();
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    public void loadHomeFragment(final String role) {

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment;
                if (role.equals("user")) {
                    CURRENT_TAG = TAG_USER;
                    fragment = getHomeFragment();
                    toolTitle.setText(YOUR_LOCATION);
                    tooImage.setVisibility(View.GONE);

                    if (preferences.getLogin()) {
                        if (preferences.getProviderLogin())
                            loginTxt.setText("Business Profile");
                        else
                            loginTxt.setText("Sell Your Services");
                    } else
                        loginTxt.setText("Sell Your Services");


                } else {
                    CURRENT_TAG = TAG_PROVIDER;
                    toolTitle.setText("Dashboard");
                    fragment = new Provider(getSupportFragmentManager());
                    tooImage.setVisibility(View.GONE);
                    if (preferences.getUserLogin())
                        loginTxt.setText("Normal User");
                    else
                        loginTxt.setText("Normal User");
                }

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        drawer.closeDrawers();

        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                User homeFragment = new User(getSupportFragmentManager());
                return homeFragment;

            default:
                return new User(getSupportFragmentManager());
        }
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment("user");

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

      /*  if (shouldLoadHomeFragOnBackPress) {
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment("user");
                return;
            }
        }*/

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            HomeScreen.toolTitle.setText(getoolTitle(getSupportFragmentManager().getFragments().get(1)));
        } else
            finishAffinity();
    }

    private String getoolTitle(Fragment fragment) {
        if (fragment instanceof View_Packages || fragment instanceof NearBy || fragment instanceof TodaysOffer || fragment instanceof Search_All_Services) {
            User.HOME_TEXT = YOUR_LOCATION;
            return YOUR_LOCATION;
        } else if (fragment instanceof Service_Details) {
            User.HOME_TEXT = ALL_SERVICES;
            return ALL_SERVICES;
        } else if (fragment instanceof Select_Appointment_Time) {
            User.HOME_TEXT = SERVICES;
            return SERVICES;
        } else if (fragment instanceof Ratings_reviews || fragment instanceof User_Edit_Profile || fragment instanceof Change_Password || fragment instanceof Service_provider_profile || fragment instanceof Employee || fragment instanceof Account_settings) {
            if (CURRENT_TAG.equals("USER")) {
                User.PROFILE_TEXT = PROFILE;
                return PROFILE;
            } else {
                Provider.ACCOUNT_TEXT = ACCOUNT;
                return ACCOUNT;
            }
        } else if (fragment instanceof Ratreviews_details) {
            if (CURRENT_TAG.equals("USER")) {
                User.PROFILE_TEXT = RATING_AND_REVIEWS;
            } else {
                Provider.ACCOUNT_TEXT = RATING_AND_REVIEWS;
            }

            return RATING_AND_REVIEWS;
        } else if (fragment instanceof Add_employee) {
            Provider.ACCOUNT_TEXT = EMPLOYEE_LIST;
            return EMPLOYEE_LIST;
        } else if (fragment instanceof Appointment_details) {
            Provider.APPOINTMENT_TEXT = APPOINTMENT;
            return APPOINTMENT;
        } else if (fragment instanceof Add_clients) {
            Provider.CLIENT_TEXT = CLIENT;
            return CLIENT;
        } else {
            return "";
        }
    }

    @Override
    public void gotoN(int position, String type,String fro) {
        switch (type) {
            case "SignUp":
                startActivityForResult(new Intent(HomeScreen.this, SignUp_Social.class).putExtra("pos", position).putExtra("for", fro), 1);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                preferences.setLogin(true);
                preferences.lastLogin("user");
                preferences.setUserLogin(true);
                if (data.getIntExtra("pos", 0) == -1) {
                    loadHomeFragment("user");
                } else {
                    val.change(data.getIntExtra("pos", 0),data.getStringExtra("for"));
                }
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                loadHomeFragment("provider");
                preferences.setLogin(true);
                preferences.lastLogin("provider");
                preferences.setProviderLogin(true);
            }
        }
    }

    /*@Override
    public void returnLoginValue(String rtn) {


        switch (rtn) {
            case "ULogin":
                preferences.setLogin(true);
                preferences.setUserLogin(true);
                preferences.lastLogin("user");
                loadHomeFragment("user");
                break;
            case "PLogin":
                preferences.setLogin(true);
                preferences.setProviderLogin(true);
                preferences.lastLogin("provider");
                loadHomeFragment("provider");
                break;
            case "USignUp":
                startActivityForResult(new Intent(HomeScreen.this, SignUp_Social.class).putExtra("pos", -1).putExtra("for", ""), 1);
                break;
            case "PSignUp":
                startActivityForResult(new Intent(HomeScreen.this, SignUp_Provider.class), 2);
                break;

        }
    }*/

    public interface changVal {
        public void change(int pos,String fro);
    }
}
