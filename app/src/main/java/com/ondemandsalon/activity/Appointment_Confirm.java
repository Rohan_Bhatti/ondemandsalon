package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;

public class Appointment_Confirm extends AppCompatActivity {

    RelativeLayout okGotItRel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appointment_confirm);

        okGotItRel = findViewById(R.id.okGotitRel);

        okGotItRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Appointment_Confirm.this, Payment.class));
            }
        });
    }
}
