package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ondemandsalon.Adapter.Add_Services_Category_second_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.utils.CustomEdittextRegular;

public class New_Category extends AppCompatActivity {


    RecyclerView add_services_category_second_recycler;
    RelativeLayout serviceRel;
    CustomEdittextRegular edtCat;
    ImageView navMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_category);


        navMenu = findViewById(R.id.navMenu);

        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        serviceRel = findViewById(R.id.serviceRel);
        edtCat = findViewById(R.id.edtCat);

        serviceRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra("frm", "ct");
                data.putExtra("ct", edtCat.getText().toString());
                setResult(RESULT_OK, data);
                finish();
            }
        });

        add_services_category_second_recycler = findViewById(R.id.add_services_category_second_recycler);
        add_services_category_second_recycler.setAdapter(new Add_Services_Category_second_adapter(this));
        add_services_category_second_recycler.setLayoutManager(new LinearLayoutManager(this));
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }*/
}
