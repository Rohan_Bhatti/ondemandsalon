package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.ondemandsalon.R;

public class SignUp_Social extends AppCompatActivity {

    LinearLayout emailMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_social);

        final int pos = getIntent().getIntExtra("pos", 0);
        final String fro = getIntent().getStringExtra("for");

        emailMobile = findViewById(R.id.emailMobile);

        emailMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SignUp_Social.this, SignUp_Email.class).putExtra("pos", pos).putExtra("for", fro), 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }
}
