package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewLight;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    ImageView closeImage;

    CustomEdittextRegular email, password;
    CustomTextviewRegular signupTxt;
    CustomTextviewLight forgotTxt;
    RelativeLayout signinRel;

    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        preferences = new Preferences(this);

        if (preferences.getLogin()) {
            startActivity(new Intent(this, HomeScreen.class));
            finish();
        }

     /*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources()
                    .getColor(R.color.white));
        }*/

        connectionDetector = new ConnectionDetector(this);
        progDialog = new ProgDialog(this);

        closeImage = findViewById(R.id.closeImage);
        email = findViewById(R.id.emailEdt);
        password = findViewById(R.id.passwordEdt);
        signupTxt = findViewById(R.id.signupTxt);
        forgotTxt = findViewById(R.id.forgotTxt);
        signinRel = findViewById(R.id.signInRel);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        signupTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUp_User.class));
            }
        });

        forgotTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

        signinRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
    }

    private void validate() {

        if (!validateEmail()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }
        if (connectionDetector.isNetworkAvailable()) {
//            login();
        } else
            connectionDetector.error_Dialog();

    }

    /*private void login() {

        progDialog.ProgressDialogStart();
        final String eml = email.getText().toString();
        final String psd = password.getText().toString();

        JSONObject params = new JSONObject();

        try {
            params.accumulate("user_email", eml);
            params.accumulate("user_password", psd);
            params.accumulate("role", "2");
            params.accumulate("device_type", "1");
            params.accumulate("device_token", "123");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        json j = new json(API.LOGIN, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progDialog.ProgressDialogStop();
                System.out.println(response.toString());
                try {
                    if (response.getString("status").equals("1")) {
                        preferences.setUserLogin(true);
                        preferences.setClientID(response.getString("id"));
                        startActivity(new Intent(LoginActivity.this, HomeScreen.class));
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                System.out.println(error.toString());
            }
        });

        AppController.getInstance().addToRequestQueue(j);
    }*/


    /*public void login() {
        progDialog.ProgressDialogStart();
        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST, API.LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progDialog.ProgressDialogStop();
                System.out.println(response);
                try {
                    JSONObject newResponse = new JSONObject(response);
                    if (newResponse.getString("status").equals("1")) {
                        preferences.setUserLogin(true);
                        preferences.setClientID(newResponse.getString("id"));
                        startActivity(new Intent(LoginActivity.this, HomeScreen.class));
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, newResponse.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progDialog.ProgressDialogStop();
                Toast.makeText(LoginActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                final String eml = email.getText().toString();
                final String psd = password.getText().toString();
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_email", eml);
                params.put("user_password", psd);
                params.put("role", "2");
                params.put("device_type", "1");
                params.put("device_token", "123");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjRequest);
    }*/

    private boolean validateEmail() {
        String e = email.getText().toString().trim();
        if (e.isEmpty()) {
            Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
            requestFocus(email);
            return false;
        } else if (!isValidEmail(e)) {
            Toast.makeText(this, "Please enter valid email", Toast.LENGTH_SHORT).show();
            requestFocus(email);
            return false;
        }
        return true;
    }

    private boolean validatePassword() {
        if (password.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
            requestFocus(password);
            return false;
        } else if (password.getText().toString().trim().length() < 4) {
            Toast.makeText(this, "Please enter valid password", Toast.LENGTH_SHORT).show();
            requestFocus(password);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void outFocus(View view) {
        if (view != null) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
