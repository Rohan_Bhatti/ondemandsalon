package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.fragments.Account_settings;
import com.ondemandsalon.fragments.Add_Offer;
import com.ondemandsalon.fragments.Add_clients;
import com.ondemandsalon.fragments.Add_employee;
import com.ondemandsalon.fragments.Appointment_details;
import com.ondemandsalon.fragments.Change_Password;
import com.ondemandsalon.fragments.Employee;
import com.ondemandsalon.fragments.NearBy;
import com.ondemandsalon.fragments.OfferDescription;
import com.ondemandsalon.fragments.Provider;
import com.ondemandsalon.fragments.Provider_Account;
import com.ondemandsalon.fragments.Provider_Appointments;
import com.ondemandsalon.fragments.Provider_Clients;
import com.ondemandsalon.fragments.Provider_DashBoard;
import com.ondemandsalon.fragments.Ratings_reviews;
import com.ondemandsalon.fragments.Ratreviews_details;
import com.ondemandsalon.fragments.Search_All_Services;
import com.ondemandsalon.fragments.Select_Appointment_Time;
import com.ondemandsalon.fragments.Service_Details;
import com.ondemandsalon.fragments.Service_provider_profile;
import com.ondemandsalon.fragments.Settings;
import com.ondemandsalon.fragments.TodaysOffer;
import com.ondemandsalon.fragments.User;
import com.ondemandsalon.fragments.User_Appointments;
import com.ondemandsalon.fragments.User_Edit_Profile;
import com.ondemandsalon.fragments.User_Home;
import com.ondemandsalon.fragments.User_Profile;
import com.ondemandsalon.fragments.User_Search;
import com.ondemandsalon.fragments.ViewOffers;
import com.ondemandsalon.fragments.View_Packages;
import com.ondemandsalon.helper.OnResponseListener;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.utils.CustomTextviewLight;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomViewPager;
import com.ondemandsalon.utils.LoginDialog;

import java.util.ArrayList;
import java.util.List;

import static com.ondemandsalon.helper.CONSTANTS.ACCOUNT;
import static com.ondemandsalon.helper.CONSTANTS.ALL_SERVICES;
import static com.ondemandsalon.helper.CONSTANTS.APPOINTMENT;
import static com.ondemandsalon.helper.CONSTANTS.CLIENT;
import static com.ondemandsalon.helper.CONSTANTS.DASHBOARD;
import static com.ondemandsalon.helper.CONSTANTS.EMPLOYEE_LIST;
import static com.ondemandsalon.helper.CONSTANTS.MY_APPOINTMENTS;
import static com.ondemandsalon.helper.CONSTANTS.PROFILE;
import static com.ondemandsalon.helper.CONSTANTS.RATING_AND_REVIEWS;
import static com.ondemandsalon.helper.CONSTANTS.SEARCH_SERVICES;
import static com.ondemandsalon.helper.CONSTANTS.SERVICES;
import static com.ondemandsalon.helper.CONSTANTS.YOUR_LOCATION;

public class HomeNew extends AppCompatActivity implements View.OnClickListener {

    public static String HOME_TOOL = YOUR_LOCATION, SEARCH_TOOL = SEARCH_SERVICES, MY_APPO_TOOL = MY_APPOINTMENTS, PROFILE_TOOL = PROFILE,
            DASHBOARD_TOOL = DASHBOARD, APPOINTMENT_TOOL = APPOINTMENT, CLIENT_TOOL = CLIENT, ACCOUNT_TOOL = ACCOUNT,
            HOME_LEVEL_0 = YOUR_LOCATION, HOME_LEVEL_1 = HOME_TOOL, HOME_LEVEL_2 = HOME_TOOL,
            SEARCH_LEVEL_0 = SEARCH_SERVICES, SEARCH_LEVEL_1 = SEARCH_TOOL,
            PROFILE_LEVEL_0 = PROFILE, PROFILE_LEVEL_1 = PROFILE_TOOL,
            APPOINTMENT_LEVEL_0 = APPOINTMENT, APPOINTMENT_LEVEL_1 = APPOINTMENT_TOOL,
            CLIENT_LEVEL_0 = CLIENT, CLIENT_LEVEL_1 = CLIENT_TOOL,
            ACCOUNT_LEVEL_0 = ACCOUNT, ACCOUNT_LEVEL_1 = ACCOUNT_TOOL;

    public static CustomTextviewRegular toolTitle;
    public LinearLayout dashLin, appointmentLin, clientLin, accLin, homeLin, searchLin, appoLin, profileLin, loginLin, bottomRelUser, bottomRelProvider;
    RelativeLayout switchRel;
    ImageView dashImg, appointmentImg, clientImg, accImg, homeImg, searchImg, appoImg, profileImg;
    public static ImageView toolImg;
    CustomTextviewRegular dashTxt, appointmentTxt, clientTxt, accTxt, homeTxt, searchTxt, appoTxt, profileTxt;
    CustomTextviewLight switchTxt;
    CustomViewPager dashboardpager;
    ViewPagerAdapter pagerAdapter;
    Preferences preferences;
    Fragment client;
    LoginDialog dialog;
    int switchFlag = 0;

    ChangeVal val, val1, val2;

    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_new);

        preferences = new Preferences(this);
        dialog = new LoginDialog(this);

        toolTitle = findViewById(R.id.toolbar_title);
        switchRel = findViewById(R.id.switchRel);
        switchTxt = findViewById(R.id.switchTxt);
        toolImg = findViewById(R.id.toolImg);

        bottomRelUser = findViewById(R.id.bottomRelUser);

        homeLin = findViewById(R.id.homeLin);
        searchLin = findViewById(R.id.searchLin);
        appoLin = findViewById(R.id.appoLin);
        profileLin = findViewById(R.id.profileLin);
        loginLin = findViewById(R.id.loginLin);

        homeImg = findViewById(R.id.homeImg);
        searchImg = findViewById(R.id.searchImg);
        appoImg = findViewById(R.id.appoImg);
        profileImg = findViewById(R.id.profileImg);

        homeTxt = findViewById(R.id.homeTxt);
        searchTxt = findViewById(R.id.searchTxt);
        appoTxt = findViewById(R.id.appoTxt);
        profileTxt = findViewById(R.id.profileTxt);

        bottomRelProvider = findViewById(R.id.bottomRelprovider);

        dashLin = findViewById(R.id.dashLin);
        appointmentLin = findViewById(R.id.appointmentLin);
        clientLin = findViewById(R.id.clientLin);
        accLin = findViewById(R.id.accLin);

        dashImg = findViewById(R.id.dashboardImg);
        appointmentImg = findViewById(R.id.appointmentImg);
        clientImg = findViewById(R.id.clientImg);
        accImg = findViewById(R.id.accountImg);

        dashTxt = findViewById(R.id.dashboardTxt);
        appointmentTxt = findViewById(R.id.appointmentTxt);
        clientTxt = findViewById(R.id.clientTxt);
        accTxt = findViewById(R.id.accontTxt);

        dashboardpager = findViewById(R.id.homepager);

        initialize(preferences.getlastLogin());
//        initialize("provider");

        homeLin.setOnClickListener(this);
        searchLin.setOnClickListener(this);
        appoLin.setOnClickListener(this);
        profileLin.setOnClickListener(this);
        loginLin.setOnClickListener(this);

        dashLin.setOnClickListener(this);
        appointmentLin.setOnClickListener(this);
        clientLin.setOnClickListener(this);
        accLin.setOnClickListener(this);

        toolImg.setOnClickListener(this);

        switchRel.setOnClickListener(this);

    }

    private void initialize(String s) {

        if (s.equals("user")) {
            preferences.lastLogin("user");
            bottomRelProvider.setVisibility(View.GONE);
            bottomRelUser.setVisibility(View.VISIBLE);
            if (preferences.getUserLogin())
                disableLogin("LogedIn");
            else {
                setViewPager("User Login");
                enableLogin();
            }

            if (preferences.getProviderLogin()) {
                switchFlag = 1;
                switchTxt.setText("Business Profile");
            } else {
                switchFlag = 0;
                switchTxt.setText("Sell Your Services");
            }
        } else {
            preferences.lastLogin("provider");
            bottomRelProvider.setVisibility(View.VISIBLE);
            bottomRelUser.setVisibility(View.GONE);

            setIconAndColor("Provider");
            setViewPager("Provider");
            switchTxt.setText("Normal User");
            switchFlag = 2;
        }

    }

    private void enableLogin() {
        loginLin.setVisibility(View.VISIBLE);
        appoLin.setVisibility(View.GONE);
        profileLin.setVisibility(View.GONE);
        dashboardpager.setCurrentItem(0);
        homeImg.setImageResource(R.drawable.home_ic);
        homeTxt.setTextColor(getResources().getColor(R.color.blue));
        homeTxt.setText("Home");
    }

    public void disableLogin(String type) {
        loginLin.setVisibility(View.GONE);
        appoLin.setVisibility(View.VISIBLE);
        profileLin.setVisibility(View.VISIBLE);
        if (type.equals("LogedIn")) {
            setIconAndColor("User Login");
            setViewPager("User Login");
        }
    }

    private void setViewPager(String s) {

        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        if (s.equals("User Login")) {
            pagerAdapter.addFragment(new User_Home(this), "");
            pagerAdapter.addFragment(new User_Search(this), "");
            pagerAdapter.addFragment(new User_Appointments(), "");
            pagerAdapter.addFragment(new User_Profile(this), "");

            dashboardpager.setOffscreenPageLimit(4);

            homeImg.setImageResource(R.drawable.home_ic);
            homeTxt.setTextColor(getResources().getColor(R.color.blue));
            homeTxt.setText("Home");


            HOME_TOOL = YOUR_LOCATION;
            toolTitle.setText(HOME_TOOL);
            HOME_LEVEL_0 = HOME_TOOL;

        } else if (s.equals("Provider")) {
            client = new Provider_Clients(this);
            val1 = (ChangeVal) client;
            pagerAdapter.addFragment(new Provider_DashBoard(this), "");
            pagerAdapter.addFragment(new Provider_Appointments(this), "");
            pagerAdapter.addFragment(client, "");
            pagerAdapter.addFragment(new Provider_Account(this), "");

//            HomeScreen.setInstance(client);

            dashImg.setImageResource(R.drawable.dashboard_ic);
            dashTxt.setTextColor(getResources().getColor(R.color.blue));
            dashTxt.setText("Dashboard");

            DASHBOARD_TOOL = DASHBOARD;
            toolTitle.setText(DASHBOARD_TOOL);
        }

        dashboardpager.disableScroll(true);
        dashboardpager.setAdapter(pagerAdapter);
        dashboardpager.setCurrentItem(0);

    }

    private void setIconAndColor(String s) {

        switch (s) {
            case "User Login":

                homeImg.setImageResource(R.drawable.home_black_ic);
                searchImg.setImageResource(R.drawable.search);
                appoImg.setImageResource(R.drawable.appointments_ic_black);
                profileImg.setImageResource(R.drawable.accounts_ic_black);

                homeTxt.setTextColor(getResources().getColor(R.color.black));
                searchTxt.setTextColor(getResources().getColor(R.color.black));
                appoTxt.setTextColor(getResources().getColor(R.color.black));
                profileTxt.setTextColor(getResources().getColor(R.color.black));

                homeTxt.setText("Home");
                searchTxt.setText("Search");
                appoTxt.setText("My Appointments");
                profileTxt.setText("Profile");

                break;
            case "Provider":

                dashImg.setImageResource(R.drawable.dashboard_ic_black);
                appointmentImg.setImageResource(R.drawable.appointments_ic_black);
                clientImg.setImageResource(R.drawable.clients_ic_black);
                accImg.setImageResource(R.drawable.accounts_ic_black);

                dashTxt.setTextColor(getResources().getColor(R.color.black));
                appointmentTxt.setTextColor(getResources().getColor(R.color.black));
                clientTxt.setTextColor(getResources().getColor(R.color.black));
                accTxt.setTextColor(getResources().getColor(R.color.black));

                dashTxt.setText("Dashboard");
                appointmentTxt.setText("Appointments");
                clientTxt.setText("Clients");
                accTxt.setText("Account");

                break;
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.homeLin:
                changeIcAndColor(0, 0);
                break;
            case R.id.searchLin:
                changeIcAndColor(1, 0);
                break;
            case R.id.appoLin:
                changeIcAndColor(2, 0);
                break;
            case R.id.profileLin:
                changeIcAndColor(3, 0);
                break;
            case R.id.loginLin:
                dialog.openLogin("user", new OnResponseListener() {
                    @Override
                    public void onResponce(int res) {
                        switch (res) {
                            case 1:
                                disableLogin("User NewLogin");
                                break;
                            case 3:
                                startActivityForResult(new Intent(HomeNew.this, SignUp_Social.class).putExtra("pos", -1).putExtra("for", "rate"), 1);
                                break;
                        }
                    }
                });
                break;
            case R.id.dashLin:
                changeIcAndColor(0, 1);
                break;
            case R.id.appointmentLin:
                changeIcAndColor(1, 1);
                break;
            case R.id.clientLin:
                changeIcAndColor(2, 1);
                break;
            case R.id.accLin:
                changeIcAndColor(3, 1);
                break;
            case R.id.switchRel:
                switch (switchFlag) {
                    case 0:
                        dialog.openLogin("provider", new OnResponseListener() {
                            @Override
                            public void onResponce(int res) {
                                switch (res) {
                                    case 1:
                                        initialize("provider");
                                        break;
                                    case 4:
                                        startActivityForResult(new Intent(HomeNew.this, SignUp_Provider.class), 2);
                                        break;
                                }
                            }
                        });
                        break;
                    case 1:
                        initialize("provider");
                        break;
                    case 2:
                        initialize("user");
                        break;
                }
                break;
            case R.id.toolImg:
                switch (dashboardpager.getCurrentItem()) {
                    case 1:
                        val.change(0, "");
                        break;
                    case 2:
                        val1.change(0, "");
                        break;
                    case 3:
                        val2.change(0, "");
                        break;
                }
                break;
        }
    }

    private void changeIcAndColor(int i, int t) {

        switch (t) {
            case 0:
                setIconAndColor("User Login");
                switch (i) {
                    case 0:
                        dashboardpager.setCurrentItem(0);
                        homeImg.setImageResource(R.drawable.home_ic);
                        homeTxt.setTextColor(getResources().getColor(R.color.blue));
                        toolTitle.setText(HOME_TOOL);
                        break;
                    case 1:
                        dashboardpager.setCurrentItem(1);
                        searchImg.setImageResource(R.drawable.search_blue_ic);
                        searchTxt.setTextColor(getResources().getColor(R.color.blue));
                        toolTitle.setText(SEARCH_TOOL);
                        break;
                    case 2:
                        dashboardpager.setCurrentItem(2);
                        appoImg.setImageResource(R.drawable.appointments_ic);
                        appoTxt.setTextColor(getResources().getColor(R.color.blue));
                        toolTitle.setText(MY_APPO_TOOL);
                        break;
                    case 3:
                        dashboardpager.setCurrentItem(3);
                        profileImg.setImageResource(R.drawable.accounts_ic);
                        profileTxt.setTextColor(getResources().getColor(R.color.blue));
                        toolTitle.setText(PROFILE_TOOL);
                        break;
                }
                break;
            case 1:
                setIconAndColor("Provider");
                switch (i) {
                    case 0:
                        dashboardpager.setCurrentItem(0);
                        dashImg.setImageResource(R.drawable.dashboard_ic);
                        dashTxt.setTextColor(getResources().getColor(R.color.blue));
                        toolTitle.setText(DASHBOARD_TOOL);
                        toolImg.setVisibility(View.GONE);
                        break;
                    case 1:
                        dashboardpager.setCurrentItem(1);
                        appointmentImg.setImageResource(R.drawable.appointments_ic);
                        appointmentTxt.setTextColor(getResources().getColor(R.color.blue));
                        toolTitle.setText(APPOINTMENT_TOOL);
                        toolImg.setImageResource(R.drawable.edit_white);
                        if (APPOINTMENT_TOOL.equals("Appointment Details"))
                            toolImg.setVisibility(View.VISIBLE);
                        else
                            toolImg.setVisibility(View.GONE);
                        break;
                    case 2:
                        dashboardpager.setCurrentItem(2);
                        clientImg.setImageResource(R.drawable.clients_ic);
                        clientTxt.setTextColor(getResources().getColor(R.color.blue));
                        toolTitle.setText(CLIENT_TOOL);
                        toolImg.setImageResource(R.drawable.plus_white);
                        if (CLIENT_TOOL.equals(CLIENT))
                            toolImg.setVisibility(View.VISIBLE);
                        else
                            toolImg.setVisibility(View.GONE);
                        break;
                    case 3:
                        dashboardpager.setCurrentItem(3);
                        accImg.setImageResource(R.drawable.accounts_ic);
                        accTxt.setTextColor(getResources().getColor(R.color.blue));
                        toolImg.setImageResource(R.drawable.plus_white);
                        toolTitle.setText(ACCOUNT_TOOL);
                        if (ACCOUNT_TOOL.equals("Employee List"))
                            toolImg.setVisibility(View.VISIBLE);
                        else
                            toolImg.setVisibility(View.GONE);
                        break;
                }
                break;
        }

    }

    public void logout(String type) {

        if (type.equals("user")) {
            if (!preferences.getProviderLogin())
                preferences.setLogin(false);
            preferences.logoutUser();
            enableLogin();
        } else {
            if (!preferences.getUserLogin())
                preferences.setLogin(false);
            preferences.logoutProvider();
            initialize("user");
        }
    }

    public void changeFragment(Fragment f, int i, String tooltitle, String tag, String type) {

        ft = getSupportFragmentManager().beginTransaction();
        int id = 0;

        if (f instanceof Service_Details)
            val = (ChangeVal) f;
        else if (f instanceof Employee)
            val2 = (ChangeVal) f;

        switch (type) {
            case "user":
                switch (i) {
                    case 0:
                        id = R.id.homeFrame;
                        HOME_TOOL = tooltitle;
                        break;
                    case 1:
                        id = R.id.searchFrame;
                        SEARCH_TOOL = tooltitle;
                        break;
                    case 2:
                        id = R.id.myappoFrame;
                        MY_APPO_TOOL = tooltitle;
                        break;
                    case 3:
                        id = R.id.userprofileFrame;
                        PROFILE_TOOL = tooltitle;
                        break;
                }
                break;
            case "provider":
                switch (i) {
                    case 0:
                        id = R.id.dashFrame;
                        DASHBOARD_TOOL = tooltitle;
                        break;
                    case 1:
                        id = R.id.appointmentFrame;
                        APPOINTMENT_TOOL = tooltitle;
                        toolImg.setImageResource(R.drawable.edit_white);
                        if (tooltitle.equals("Appointment Details"))
                            toolImg.setVisibility(View.VISIBLE);
                        else
                            toolImg.setVisibility(View.GONE);
                        break;
                    case 2:
                        id = R.id.clientFrame;
                        CLIENT_TOOL = tooltitle;
                        toolImg.setVisibility(View.GONE);
                        break;
                    case 3:
                        id = R.id.accountFrame;
                        ACCOUNT_TOOL = tooltitle;
                        toolImg.setImageResource(R.drawable.plus_white);
                        if (tooltitle.equals("Employee List"))
                            toolImg.setVisibility(View.VISIBLE);
                        else
                            toolImg.setVisibility(View.GONE);
                        break;
                }
                break;
        }


        ft.replace(id, f);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(tag);
        ft.commit();
        toolTitle.setText(tooltitle);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            toolTitle.setText(getoolTitle(getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getFragments().size() - 1), getSupportFragmentManager().getBackStackEntryCount()));
            getSupportFragmentManager().popBackStack();
        } else
            finishAffinity();
    }

    private String getoolTitle(Fragment fragment, int backStackEntryCount) {


        if (preferences.getlastLogin().equals("user")) {
            switch (backStackEntryCount) {
                case 1:
                    if (fragment instanceof View_Packages || fragment instanceof NearBy || fragment instanceof TodaysOffer || fragment instanceof Search_All_Services) {
                        HOME_TOOL = YOUR_LOCATION;
                        return HOME_LEVEL_0;
                    } else if (fragment instanceof Service_Details) {
                        SEARCH_TOOL = SEARCH_SERVICES;
                        return SEARCH_LEVEL_0;
                    } else if (fragment instanceof User_Edit_Profile || fragment instanceof Change_Password || fragment instanceof Ratings_reviews || fragment instanceof Settings) {
                        PROFILE_TOOL = PROFILE;
                        return PROFILE_LEVEL_0;
                    }
                    break;
                case 2:
                    if (fragment instanceof Service_Details || fragment instanceof OfferDescription) {
                        HOME_TOOL = HOME_LEVEL_1;
                        return HOME_LEVEL_1;
                    } else if (fragment instanceof Select_Appointment_Time) {
                        SEARCH_TOOL = SEARCH_LEVEL_1;
                        return SEARCH_LEVEL_1;
                    } else if (fragment instanceof Ratreviews_details) {
                        PROFILE_TOOL = PROFILE_LEVEL_1;
                        return PROFILE_LEVEL_1;
                    }
                    break;
                case 3:
                    if (fragment instanceof Select_Appointment_Time) {
                        HOME_TOOL = HOME_LEVEL_2;
                        return HOME_LEVEL_2;
                    }
                    break;
            }
        } else {
            switch (backStackEntryCount) {
                case 1:
                    if (fragment instanceof Appointment_details) {
                        APPOINTMENT_TOOL = APPOINTMENT_LEVEL_0;
                        return APPOINTMENT_LEVEL_0;
                    } else if (fragment instanceof Add_clients) {
                        CLIENT_TOOL = CLIENT_LEVEL_0;
                        return CLIENT_LEVEL_0;
                    } else if (fragment instanceof Service_provider_profile || fragment instanceof Employee || fragment instanceof Ratings_reviews ||
                            fragment instanceof Account_settings || fragment instanceof Change_Password || fragment instanceof Add_Offer || fragment instanceof ViewOffers) {
                        ACCOUNT_TOOL = ACCOUNT_LEVEL_0;
                        return ACCOUNT_LEVEL_0;
                    }
                    break;
                case 2:
                    if (fragment instanceof Add_employee || fragment instanceof Ratreviews_details || fragment instanceof Add_Offer) {
                        ACCOUNT_TOOL = ACCOUNT_LEVEL_1;
                        return ACCOUNT_LEVEL_1;
                    }
                    break;
            }
        }
        return "";
    }

    public void gotoN(int position, String fro) {
        startActivityForResult(new Intent(HomeNew.this, SignUp_Social.class).putExtra("pos", position).putExtra("for", fro), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                preferences.setLogin(true);
                preferences.lastLogin("user");
                preferences.setUserLogin(true);
                disableLogin("User NewLogin");
                if (!data.getStringExtra("for").equals("rate")) {
                    val.change(data.getIntExtra("pos", 0), data.getStringExtra("for"));
                }
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                preferences.setLogin(true);
                preferences.lastLogin("provider");
                preferences.setProviderLogin(true);
                initialize("provider");
            }
        }
    }


    public interface ChangeVal {
        public void change(int pos, String fro);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
