package com.ondemandsalon.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;

public class Welcome_Screen extends AppCompatActivity {

    private static final String FIRST_LAUNCH = "first_launch";
    ViewPager welcomePager;
    RelativeLayout skipRel;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ImageView welcomeBg;
    Preferences pref;
    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    welcomeBg.setImageResource(R.drawable.welcome_back);
                    break;
                case 1:
                    welcomeBg.setImageResource(R.drawable.welcome_back_2);
                    break;
                case 2:
                    welcomeBg.setImageResource(R.drawable.welcome_back_1);
                    break;
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
    private MyViewPagerAdapter myViewPagerAdapter;
    private int[] layouts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_screen);

        preferences = getSharedPreferences("welcome", 0);
        editor = preferences.edit();
        pref = new Preferences(this);


        welcomePager = findViewById(R.id.welcomePager);
        skipRel = findViewById(R.id.skipRel);
        welcomeBg = findViewById(R.id.welcome_bg);

        boolean st = preferences.getBoolean(FIRST_LAUNCH, true);

        if (!st) {
            if (pref.getLogin()) {
                launchHomeScreen();
                finish();
            }
        }


        layouts = new int[]{
                R.layout.welcome1,
                R.layout.welcome2,
                R.layout.welcome3};

        // adding bottom dots
//        addBottomDots(0);

        // making notification bar transparent
//        changeStatusBarColor();


        myViewPagerAdapter = new MyViewPagerAdapter();
        welcomePager.setAdapter(myViewPagerAdapter);
        welcomePager.addOnPageChangeListener(viewPagerPageChangeListener);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(welcomePager, true);

        skipRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomeScreen();
            }
        });
    }

    private int getItem(int i) {
        return welcomePager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        editor.putBoolean(FIRST_LAUNCH, false);
        editor.commit();
        /*startActivity(new Intent(Welcome_Screen.this, SelectUser.class));
        finish();*/
//        startActivity(new Intent(this, HomeScreen.class));
        startActivity(new Intent(this, HomeNew.class));
        finish();
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
