package com.ondemandsalon.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.Adapter.Add_Services_Category_adapter;
import com.ondemandsalon.Adapter.Appointment_list_adapter;
import com.ondemandsalon.R;
import com.ondemandsalon.fragments.Service_Details;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Sub_cat;
import com.ondemandsalon.model.User_Appointment_data;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Service_Category extends AppCompatActivity implements Add_Services_Category_adapter.Maintain_categories {


    RecyclerView add_services_category_recycler;
    RelativeLayout continueRel;
    ArrayList<Sub_cat> categoryLst;
    ImageView navMenu;
    String frm;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;
    Context context;
    ArrayList<Sub_cat> newcatlist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_category);
        navMenu = findViewById(R.id.navMenu);
        preferences = new Preferences(this);
        connectionDetector = new ConnectionDetector(this);
        progDialog = new ProgDialog(this);
        context = this;

        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        frm = getIntent().getStringExtra("frm");
        add_services_category_recycler = findViewById(R.id.add_services_category_recycler);
        continueRel = findViewById(R.id.continueRel);

        getCategories();

        continueRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!frm.equals("back")) {
                    postCategories();
                } else {
                    onBackPressed();
                }
            }
        });
    }

    private void postCategories() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("provider_id", preferences.getProviderId());
                jsonObject.put("category_list", getCatList());

                json json = new json(API.POST_CATEGORY_LIST_PROVIDER, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.getString("status").equals("1")) {
                                startActivityForResult(new Intent(Service_Category.this, Services_and_Prices.class).putExtra("frm", "result"), 1);
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                        Toast.makeText(context, "Somthing wents wrong\nPlease try again", Toast.LENGTH_SHORT).show();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private JSONArray getCatList() {
        JSONArray category_list = new JSONArray();
        if (newcatlist != null && newcatlist.size() != 0) {
            for (int i = 0; i < newcatlist.size(); i++) {
                Sub_cat sub_cat = newcatlist.get(i);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("category_id", sub_cat.getSub_cat_id());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                category_list.put(jsonObject);
            }
            return category_list;
        }
        return category_list;
    }

    private void getCategories() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();

            json j = new json(API.GET_CATEGORY_LIST_PROVIDER, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progDialog.ProgressDialogStop();
                    try {
                        if (response.getString("status").equals("1")) {
                            JSONArray jsonArray = response.getJSONArray("category_list");
                            if (jsonArray.length() != 0) {
                                categoryLst = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String cat_id = jsonObject.getString("ser_id");
                                    String cat_name = jsonObject.getString("ser_name");

                                    categoryLst.add(new Sub_cat(cat_name, cat_id));
                                }
                                add_services_category_recycler.setAdapter(new Add_Services_Category_adapter(context, categoryLst));
                                add_services_category_recycler.setLayoutManager(new LinearLayoutManager(context));
                            }
                        } else {
                            Toast.makeText(Service_Category.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progDialog.ProgressDialogStop();
                    System.out.println(error.toString());
                    Toast.makeText(Service_Category.this, "Somthing wents wrong", Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(j);
        } else {
            connectionDetector.error_Dialog();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    @Override
    public void add_remove_category(String sub_cat_id, String sub_cat_name, String add) {
        if (add.equals("ADD")) {
            if (newcatlist != null) {
                newcatlist.add(new Sub_cat(sub_cat_name, sub_cat_id));
            }
        } else {
            if (newcatlist != null && newcatlist.size() != 0) {
                for (int i = 0; i < newcatlist.size(); i++) {
                    Sub_cat Sub_cat = newcatlist.get(i);
                    if (Sub_cat.getSub_cat_id().equals(sub_cat_id)) {
                        newcatlist.remove(i);
                        break;
                    }
                }
            }
        }
    }
}
