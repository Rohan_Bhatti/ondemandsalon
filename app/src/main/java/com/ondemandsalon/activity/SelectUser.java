package com.ondemandsalon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;

public class SelectUser extends AppCompatActivity {

    RelativeLayout userRel, serviceProv;

    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_user);

        preferences = new Preferences(this);


        if (preferences.getLogin()) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        userRel = findViewById(R.id.userRel);

        serviceProv = findViewById(R.id.serviceProvRel);

        serviceProv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SelectUser.this, LoginActivity.class));
            }
        });
    }
}
