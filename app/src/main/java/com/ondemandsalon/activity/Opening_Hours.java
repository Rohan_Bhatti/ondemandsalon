package com.ondemandsalon.activity;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ondemandsalon.R;
import com.ondemandsalon.helper.Preferences;
import com.ondemandsalon.model.Service_data;
import com.ondemandsalon.utils.ConnectionDetector;
import com.ondemandsalon.utils.CustomEdittextLight;
import com.ondemandsalon.utils.CustomEdittextRegular;
import com.ondemandsalon.utils.CustomTextviewRegular;
import com.ondemandsalon.utils.CustomTimePickerDialog;
import com.ondemandsalon.utils.ProgDialog;
import com.ondemandsalon.webservices.API;
import com.ondemandsalon.webservices.AppController;
import com.ondemandsalon.webservices.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Opening_Hours extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout saveRel;
    CustomTextviewRegular f1, t1, f2, t2, f3, t3, f4, t4, f5, t5, f6, t6, f7, t7;
    ImageView navMenu;
    String frm, frm_class;
    private int mHour, mMinute;
    CustomEdittextLight note_for_client;
    ConnectionDetector connectionDetector;
    ProgDialog progDialog;
    Preferences preferences;
    Context context;
    CheckBox d1, d2, d3, d4, d5, d6, d7;
    ImageView i1, i2, i3, i4, i5, i6, i7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opening_hours);
        preferences = new Preferences(this);
        connectionDetector = new ConnectionDetector(this);
        progDialog = new ProgDialog(this);
        context = this;
        navMenu = findViewById(R.id.navMenu);
        note_for_client = findViewById(R.id.note_for_client);
        frm = getIntent().getStringExtra("frm");
        frm_class = getIntent().getStringExtra("class");

        navMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        f1 = findViewById(R.id.F1);
        f2 = findViewById(R.id.F2);
        f3 = findViewById(R.id.F3);
        f4 = findViewById(R.id.F4);
        f5 = findViewById(R.id.F5);
        f6 = findViewById(R.id.F6);
        f7 = findViewById(R.id.F7);

        t1 = findViewById(R.id.T1);
        t2 = findViewById(R.id.T2);
        t3 = findViewById(R.id.T3);
        t4 = findViewById(R.id.T4);
        t5 = findViewById(R.id.T5);
        t6 = findViewById(R.id.T6);
        t7 = findViewById(R.id.T7);

        d1 = findViewById(R.id.checkd1);
        d2 = findViewById(R.id.checkd2);
        d3 = findViewById(R.id.checkd3);
        d4 = findViewById(R.id.checkd4);
        d5 = findViewById(R.id.checkd5);
        d6 = findViewById(R.id.checkd6);
        d7 = findViewById(R.id.checkd7);

        i1 = findViewById(R.id.imgd1);
        i2 = findViewById(R.id.imgd2);
        i3 = findViewById(R.id.imgd3);
        i4 = findViewById(R.id.imgd4);
        i5 = findViewById(R.id.imgd5);
        i6 = findViewById(R.id.imgd6);
        i7 = findViewById(R.id.imgd7);

        f1.setOnClickListener(this);
        f2.setOnClickListener(this);
        f3.setOnClickListener(this);
        f4.setOnClickListener(this);
        f5.setOnClickListener(this);
        f6.setOnClickListener(this);
        f7.setOnClickListener(this);

        t1.setOnClickListener(this);
        t2.setOnClickListener(this);
        t3.setOnClickListener(this);
        t4.setOnClickListener(this);
        t5.setOnClickListener(this);
        t6.setOnClickListener(this);
        t7.setOnClickListener(this);

        saveRel = findViewById(R.id.saveRel);


        if (frm_class.equals("Profile")) {
            getOpeningHours();
        } /*else if (frm_class.equals("Registration")) {

        }*/


        d1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                state_change(b, i1);
            }
        });

        d2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                state_change(b, i2);
            }
        });
        d3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                state_change(b, i3);
            }
        });
        d4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                state_change(b, i4);
            }
        });
        d5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                state_change(b, i5);
            }
        });
        d6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                state_change(b, i6);
            }
        });
        d7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                state_change(b, i7);
            }
        });


        saveRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!frm.equals("back")) {
                    postOpeningHours();
                } else {
                    onBackPressed();
                }
            }
        });
    }

    private void state_change(boolean b, ImageView i1) {
        if (b) {
            i1.setVisibility(View.VISIBLE);
        } else {
            i1.setVisibility(View.INVISIBLE);
        }
    }

    private void postOpeningHours() {
        if (connectionDetector.isNetworkAvailable()) {
            progDialog.ProgressDialogStart();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("hour_list", getCatList());
                jsonObject.accumulate("provider_id", preferences.getProviderId());
                jsonObject.accumulate("notes", note_for_client.getText().toString());

                json json = new json(API.POST_OPENING_HOURS, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progDialog.ProgressDialogStop();
                        try {
                            if (response.getString("status").equals("1")) {
                                startActivityForResult(new Intent(Opening_Hours.this, Service_Category.class).putExtra("frm", "result"), 1);
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progDialog.ProgressDialogStop();
                        Toast.makeText(context, "Somthing wents wrong\nPlease try again", Toast.LENGTH_SHORT).show();
                    }
                });
                AppController.getInstance().addToRequestQueue(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            connectionDetector.error_Dialog();
        }
    }

    private JSONArray getCatList() {
        ArrayList<Service_data> servicelst = new ArrayList<>();
        if (d1.isChecked()) {
            servicelst.add(new Service_data("Monday", f1.getText().toString(), t1.getText().toString()));
        }
        if (d2.isChecked()) {
            servicelst.add(new Service_data("Tuesday", f2.getText().toString(), t2.getText().toString()));
        }
        if (d3.isChecked()) {
            servicelst.add(new Service_data("Wednesday", f3.getText().toString(), t3.getText().toString()));
        }
        if (d4.isChecked()) {
            servicelst.add(new Service_data("Thursday", f4.getText().toString(), t4.getText().toString()));
        }
        if (d5.isChecked()) {
            servicelst.add(new Service_data("Friday", f5.getText().toString(), t5.getText().toString()));
        }
        if (d6.isChecked()) {
            servicelst.add(new Service_data("Saturday", f6.getText().toString(), t6.getText().toString()));
        }
        if (d7.isChecked()) {
            servicelst.add(new Service_data("Sunday", f7.getText().toString(), t7.getText().toString()));
        }

        JSONArray category_list = new JSONArray();
        if (servicelst.size() != 0) {
            for (int i = 0; i < servicelst.size(); i++) {
                Service_data service_data = servicelst.get(i);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("oh_day", service_data.getDay());
                    jsonObject.put("oh_start_time", service_data.getFrom());
                    jsonObject.put("oh_end_time", service_data.getTo());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                category_list.put(jsonObject);
            }
            return category_list;
        }
        return category_list;
    }

    private void getOpeningHours() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.F1:
                getTime(f1);
                break;
            case R.id.F2:
                getTime(f2);
                break;
            case R.id.F3:
                getTime(f3);
                break;
            case R.id.F4:
                getTime(f4);
                break;
            case R.id.F5:
                getTime(f5);
                break;
            case R.id.F6:
                getTime(f6);
                break;
            case R.id.F7:
                getTime(f7);
                break;


            case R.id.T1:
                getTime(t1);
                break;
            case R.id.T2:
                getTime(t2);
                break;
            case R.id.T3:
                getTime(t3);
                break;
            case R.id.T4:
                getTime(t4);
                break;
            case R.id.T5:
                getTime(t5);
                break;
            case R.id.T6:
                getTime(t6);
                break;
            case R.id.T7:
                getTime(t7);
                break;
        }

    }

    private void getTime(final TextView textView) {
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);


        CustomTimePickerDialog customTimePickerDialog = new CustomTimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                Time time = new Time(hourOfDay, minute, 0);
                @SuppressLint("SimpleDateFormat") Format format = new SimpleDateFormat("hh:mm a");
                textView.setText(format.format(time));
            }
        }, mHour, mMinute, false);
        customTimePickerDialog.show();

       /* TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                Time time = new Time(hourOfDay, minute, 0);
                @SuppressLint("SimpleDateFormat") Format format = new SimpleDateFormat("hh:mm a");
                textView.setText(format.format(time));
            }
        }, mHour, mMinute, false);
        timePickerDialog.show();*/
    }
}
